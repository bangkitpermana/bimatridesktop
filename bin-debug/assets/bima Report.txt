﻿bima.swf Movie Report
----------------------

Frame #    Frame Bytes    Total Bytes    Scene
-------    -----------    -----------    -----
      1         590136         590136    Scene 1 (AS 3.0 Classes Export Frame)

Scene      Shape Bytes    Text Bytes    ActionScript Bytes
-------    -----------    ----------    ------------------
Scene 1            112             0                   536

Symbol               Shape Bytes    Text Bytes    ActionScript Bytes
-----------------    -----------    ----------    ------------------
faint_mc                       0             0                     0
faintloop_mc                   0             0                     0
kiss_mc                        0             0                     0
kissloop_mc                    0             0                     0
laugh_mc                       0             0                     0
laughloop_mc                   0             0                     0
laughrotate_mc                 0             0                     0
smile_mc                       0             0                     0
smileloop_mc                   0             0                     0
tongue_mc                      0             0                     0
tongueloop_mc                  0             0                     0
wink_mc                        0             0                     0
winkloop_mc                    0             0                     0
worry_mc                       0             0                     0
worryloop_mc                   0             0                     0
Ball                          69             0                     0
Bima                           0             0                     0
circle                         0             0                     0
circle_shape                 108             0                     0
empty                          0             0                     0
loading_circle_mc              0            60                     0
Ray                            0             0                     0
ray_shape                     63             0                     0
Ray2                           0             0                     0
Tween 2                       67             0                     0
Window                        80             0                     0
Tween 1                        0             0                     0

Font Name                  Bytes         Characters
-----------------------    ----------    ----------
MyriadPro-Semibold Bold            30    

ActionScript Bytes    Location
------------------    --------
               536    Scene 1:Frame 1

Bitmap                     Compressed    Original      Compression
-----------------------    ----------    ----------    -----------
FAINT0001.png                    7759        193600    JPEG Quality=80
FAINT0002.png                    7699        193600    JPEG Quality=80
FAINT0003.png                    7491        193600    JPEG Quality=80
FAINT0004.png                    7372        193600    JPEG Quality=80
FAINT0005.png                    7299        193600    JPEG Quality=80
FAINTLOOP0001.png                7299        193600    JPEG Quality=80
FAINTLOOP0002.png                7221        193600    JPEG Quality=80
FAINTLOOP0003.png                7197        193600    JPEG Quality=80
FAINTLOOP0004.png                7221        193600    JPEG Quality=80
FAINTLOOP0005.png                7299        193600    JPEG Quality=80
KISS0001.png                     7866        193600    JPEG Quality=80
KISS0002.png                     7687        193600    JPEG Quality=80
KISS0003.png                     7483        193600    JPEG Quality=80
KISS0004.png                     7251        193600    JPEG Quality=80
KISS0005.png                     7091        193600    JPEG Quality=80
KISS0006.png                     7140        193600    JPEG Quality=80
KISS0007.png                     7056        193600    JPEG Quality=80
KISSLOOP0001.png                 7133        193600    JPEG Quality=80
KISSLOOP0002.png                 7117        193600    JPEG Quality=80
KISSLOOP0003.png                 7154        193600    JPEG Quality=80
KISSLOOP0004.png                 7128        193600    JPEG Quality=80
KISSLOOP0005.png                 7141        193600    JPEG Quality=80
LAUGH0001.png                    7733        193600    JPEG Quality=80
LAUGH0002.png                    7540        193600    JPEG Quality=80
LAUGH0003.png                    7582        193600    JPEG Quality=80
LAUGH0004.png                    7539        193600    JPEG Quality=80
LAUGH0005.png                    7733        193600    JPEG Quality=80
LAUGHLOOP0001.png                7670        193600    JPEG Quality=80
LAUGHLOOP0002.png                7729        193600    JPEG Quality=80
LAUGHLOOP0003.png                7721        193600    JPEG Quality=80
LAUGHLOOP0004.png                7729        193600    JPEG Quality=80
LAUGHLOOP0005.png                7670        193600    JPEG Quality=80
LAUGHROTATE0001.png              7733        193600    JPEG Quality=80
LAUGHROTATE0002.png              7853        193600    JPEG Quality=80
LAUGHROTATE0003.png              7661        193600    JPEG Quality=80
LAUGHROTATE0004.png              7670        193600    JPEG Quality=80
SMILEROTATE0001.png              7706        193600    JPEG Quality=80
SMILEROTATE0002.png              7841        193600    JPEG Quality=80
SMILEROTATE0003.png              7634        193600    JPEG Quality=80
SMILEROTATE0004.png              7663        193600    JPEG Quality=80
SMILEROTATELOOP0001.png          7663        193600    JPEG Quality=80
SMILEROTATELOOP0002.png          7703        193600    JPEG Quality=80
SMILEROTATELOOP0003.png          7686        193600    JPEG Quality=80
SMILEROTATELOOP0004.png          7703        193600    JPEG Quality=80
SMILEROTATELOOP0005.png          7663        193600    JPEG Quality=80
TONGUE0001.png                   7671        193600    JPEG Quality=80
TONGUE0002.png                   7573        193600    JPEG Quality=80
TONGUE0003.png                   7059        193600    JPEG Quality=80
TONGUE0004.png                   6799        193600    JPEG Quality=80
TONGUE0005.png                   6683        193600    JPEG Quality=80
TONGUE0006.png                   6812        193600    JPEG Quality=80
TONGUE0007.png                   6975        193600    JPEG Quality=80
TONGUELOOP0001.png               6975        193600    JPEG Quality=80
TONGUELOOP0002.png               6882        193600    JPEG Quality=80
TONGUELOOP0003.png               6959        193600    JPEG Quality=80
TONGUELOOP0004.png               6883        193600    JPEG Quality=80
TONGUELOOP0005.png               6975        193600    JPEG Quality=80
WINK0001.png                     7892        193600    JPEG Quality=80
WINK0002.png                     7696        193600    JPEG Quality=80
WINK0003.png                     7392        193600    JPEG Quality=80
WINK0004.png                     7086        193600    JPEG Quality=80
WINK0005.png                     6692        193600    JPEG Quality=80
WINK0006.png                     6658        193600    JPEG Quality=80
WINK0007.png                     6890        193600    JPEG Quality=80
WINKLOOP0001.png                 6890        193600    JPEG Quality=80
WINKLOOP0002.png                 6843        193600    JPEG Quality=80
WINKLOOP0003.png                 6836        193600    JPEG Quality=80
WINKLOOP0004.png                 6843        193600    JPEG Quality=80
WINKLOOP0005.png                 6890        193600    JPEG Quality=80
WORRY0001.png                    7785        193600    JPEG Quality=80
WORRY0002.png                    7641        193600    JPEG Quality=80
WORRY0003.png                    7712        193600    JPEG Quality=80
WORRY0004.png                    7691        193600    JPEG Quality=80
WORRY0005.png                    7676        193600    JPEG Quality=80
WORRYLOOP0001.png                7676        193600    JPEG Quality=80
WORRYLOOP0002.png                7617        193600    JPEG Quality=80
WORRYLOOP0003.png                7682        193600    JPEG Quality=80
WORRYLOOP0004.png                7617        193600    JPEG Quality=80
WORRYLOOP0005.png                7676        193600    JPEG Quality=80




