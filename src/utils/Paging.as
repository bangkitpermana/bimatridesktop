package utils
{
	import mx.collections.ArrayCollection;

	public class Paging
	{
		
		public static function createMenuLevel1Pages(data:ArrayCollection,maxItemPerPage:uint):PagingObject {
			var ob:PagingObject = new PagingObject();
			
			ob.pages = new ArrayCollection();
			ob.activePage = null;
			ob.maxPages = data.length/maxItemPerPage + (data.length%maxItemPerPage==0?0:1);
			var idx:int = 0;
			for(var i:int=0;i<ob.maxPages;i++) {
				var page:ArrayCollection = new ArrayCollection();
				for(var j:int=0;j<maxItemPerPage;j++) {
					if(idx==data.length) break;
					page.addItem(data[idx]);
					idx++;
				}
				ob.pages.addItem(page);
			}
			//if(ob.pages.length < 1) return;
			ob.activePageNo = 0;
			ob.activePage = ob.pages[ob.activePageNo];
			
			return ob;
		}
	}
}