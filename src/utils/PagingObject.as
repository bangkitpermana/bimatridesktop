package utils
{
	import mx.collections.ArrayCollection;

	public class PagingObject
	{
		
		[Bindable] public var pages:ArrayCollection;
		[Bindable] public var activePage:ArrayCollection;
		[Bindable] public var activePageNo:int;
		[Bindable] public var maxPages:int;
		
		public function nextPage():void {
			if(activePageNo < maxPages-1) {
				activePageNo++;
				activePage = pages[activePageNo];
			}
		}
		
		public function prevPage():void {
			if(activePageNo > 0) {
				activePageNo--;
				activePage = pages[activePageNo];
			}
		}
		
	}
}