package core
{
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectProxy;

	public class PackageMenu
	{
		
		private static var menu:ArrayCollection;
		
		public static var reloadButtonMenu:ArrayCollection;
		
		public static function generateGlobalMenu(rawData:Array , id:String):ArrayCollection {
			menu = new ArrayCollection();
			for(var i:int=0;i<rawData.length;i++){
				if(id == rawData[i].parentid && rawData[i].parentid != 0) {
					trace ("first a 1" + rawData[i].name);
					var pm:Object = new ObjectProxy();
					pm.name = rawData[i].name;
					pm.href = rawData[i].href;
					pm.parentid = rawData[i].parentid;
					pm.id = rawData[i].id;	
					pm.desc = rawData[i].desc;
					pm.group = rawData[i].group;
					pm.value = rawData[i].value;
					pm.image = rawData[i].image;
					pm.tariff = rawData[i].tariff;
					pm.validity = rawData[i].validity;
					pm.promo = rawData[i].promo;
					pm.code = rawData[i].pkg_code;
					pm.auto_renewal = rawData[i].auto_renewal;
					pm.pm = rawData[i].pm;
					pm.paymentchannel= rawData[i].paymentchannel;
					pm.paymentname= rawData[i].paymentname;
					pm.imageUrl = Service.getPackageImageUrl(pm.code);
					pm.paymenturl = rawData[i].paymenturl;
					
					//
					/*
					if(rawData[i].hasOwnProperty('upsell')) pm.upsell = rawData[i].upsell;
					var tempArr:Array = rawData[i].pm?rawData[i].pm:[];
					pm.pm = [];
					for(var j:int=0;j<tempArr.length;j++) {
						var pOb:Object = new ObjectProxy();
						pOb = new ObjectProxy();
						pOb.paymentchannel = tempArr[j].paymentchannel;
						pOb.paymentname = tempArr[j].paymentname;
						pOb.paymenturl = tempArr[j].paymenturl;
						pm.pm.push(pOb);
					}
					
					pm.imageUrl = Service.getPackageImageUrl(pm.code);
					
					pm.tariff = rawData[i].tariff;
					pm.validity = rawData[i].validity;
					pm.value = rawData[i].value;
					var parent:Object = getParent(pm.group);
					if(parent) {
						parent.menus.addItem(pm);
					}
				}
			}
			Manager.getInstance().reloadPageReady = true;
			Manager.getInstance().pullNotification();
			
			return menu;
		} */
					//
					
					 pm.menus = new ArrayCollection();
					menu.addItem(pm);
					if(String(pm.name).toLowerCase().indexOf("roam")>-1) {
						trace ("first b");
						Manager.getInstance().isRoamingMenu = true;
						Manager.getInstance().roamingGroupName = rawData[i].group;
					}
					
				}
			}
			return menu;
		}
		
		
		
		public static function generatePackageMenu(rawData:Array):ArrayCollection {
			menu = new ArrayCollection();
			trace ("this is generatePackageMenu");
			if(reloadButtonMenu == null) reloadButtonMenu = new ArrayCollection();

			//generate first level package menu
			for(var i:int=0;i<rawData.length;i++) {
				trace ("first a");
				if(isPackageMenu(rawData[i].href) && rawData[i].parentid != 0) {
					trace ("first a 1" + rawData[i].name);
					var pm:Object = new ObjectProxy();
					pm.name = rawData[i].name;
					pm.href = rawData[i].href;
					pm.parentid = rawData[i].parentid;
					pm.id = rawData[i].id;					
					pm.menus = new ArrayCollection();
					menu.addItem(pm);
					if(String(pm.name).toLowerCase().indexOf("roam")>-1) {
						trace ("first b");
						Manager.getInstance().isRoamingMenu = true;
						Manager.getInstance().roamingGroupName = rawData[i].group;
					}
					
				}
				/*
				if(isPackageMenu(rawData[i].parentid == ))
				{
					trace ("ini parent atas :"+rawData[i].href);
				} */
				
				if(rawData[i].href == '#payment') {
					trace ("first c");
					Manager.getInstance().billingPaymentMenu = new ObjectProxy();
					Manager.getInstance().billingPaymentMenu.group = rawData[i].group;
					Manager.getInstance().billingPaymentMenu.name = rawData[i].name;
//					Manager.getInstance().billingPaymentMenu.pm = rawData[i].pm;
					Manager.getInstance().billingPaymentMenu.pm = new Array();
					for(j = 0; j < rawData[i].pm.length;j++) {
						Manager.getInstance().billingPaymentMenu.pm.push(rawData[i].pm[j]);
					}
				}
			}
				
			//revision 18/4/2013 : check for external package(topups and/or bills)
			// ('_') <-- not an emo
				
			if(reloadButtonMenu.length == 0) {
				for(i=0;i<rawData.length;i++) {
					if(rawData[i].id.split('_').length == 2) {
	//					if(rawData[i].id == '3_3' || (rawData[i].id.split('_')[0] == 2))
						if(((rawData[i].group == 'Reload' || rawData[i].group == 'Isi Ulang') && rawData[i].tariff != null)) {
//							trace(rawData[i].name);
							var px:Object = new ObjectProxy();
							px.name = rawData[i].name;
							px.id = rawData[i].id;							
							px.desc = rawData[i].desc;
							px.group = rawData[i].group;
							px.tariff = rawData[i].tariff;
							px.validity = rawData[i].validity;
							px.promo = rawData[i].promo;
							if(rawData[i].hasOwnProperty('upsell')) px.upsell = rawData[i].upsell;
							if(rawData[i].hasOwnProperty('pkg_code')) px.code = rawData[i].pkg_code;
							px.value = rawData[i].name;
							if(rawData[i].hasOwnProperty('pkg_code')) {
								px.image = rawData[i].image;
								px.imageUrl = Service.getPackageImageUrl(px.code);
							}
							var tempArr:Array = rawData[i].pm?rawData[i].pm:[];
							px.pm = [];
	//						if((px.name as String).toLowerCase().indexOf('indie') < 0 && Manager.getInstance().isHybrid)
	//							px.pm.push({paymentchannel:"00H", paymentname:Manager.getInstance().lm.getLangStr(81)});
							for(var j:int=0;j<tempArr.length;j++) {
								var pOb:Object = new ObjectProxy();
								pOb.paymentchannel = tempArr[j].paymentchannel;
								pOb.paymentname = tempArr[j].paymentname;
								pOb.paymenturl = tempArr[j].paymenturl;
								px.pm.push(pOb);
							}
							if(px.tariff != "") reloadButtonMenu.addItem(px);
						}
					}
				}
			}
			
			trace("Ext Menu Length :" +reloadButtonMenu.length);
			//generate second level package menu
			for(i=0;i<rawData.length;i++) {
				if(isPackageMenuChild(rawData[i].href)) {
					var pc:Object = new ObjectProxy();
					pc.name = rawData[i].name;
					pc.href = rawData[i].href;
					pc.code = rawData[i].pkg_code;
					pc.desc = rawData[i].desc;
					pc.group = rawData[i].group;
					pc.image = rawData[i].image;
					pc.promo = rawData[i].promo;
					pc.autoRenewal = rawData[i].auto_renewal;
					if(rawData[i].hasOwnProperty('upsell')) pc.upsell = rawData[i].upsell;
					tempArr = rawData[i].pm?rawData[i].pm:[];
					pc.pm = [];
					for(j=0;j<tempArr.length;j++) {
						pOb = new ObjectProxy();
						pOb.paymentchannel = tempArr[j].paymentchannel;
						pOb.paymentname = tempArr[j].paymentname;
						pOb.paymenturl = tempArr[j].paymenturl;
						pc.pm.push(pOb);
					}
					
					pc.imageUrl = Service.getPackageImageUrl(pc.code);
				
					pc.tariff = rawData[i].tariff;
					pc.validity = rawData[i].validity;
					pc.value = rawData[i].value;
					var parent:Object = getParent(pc.group);
					if(parent) {
						parent.menus.addItem(pc);
					}
				}
			}
			Manager.getInstance().reloadPageReady = true;
			Manager.getInstance().pullNotification();
			
			return menu;
		}
		
//		private static function isPackageMenu(id:String):Boolean {
//			return id.split('_').length==3;
//		}
//		
//		private static function isPackageMenuChild(id:String):Boolean {
//			return id.split('_').length==4;
//		}
		
		private static function isPackageMenu(href:String):Boolean {
			return (href=="#package" || href=="#hybrid");
		}
		
		private static function isPackageMenuChild(href:String):Boolean {
			return href=="#packageitem";
		}
		
		public static function getRoamingPackage():ArrayCollection {
			if(Manager.getInstance().isRoamingMenu) {
				var temp:ArrayCollection = new ArrayCollection();
				var isRoaming:Boolean;
				for(var i:int=0;i<menu.length;i++) {
					isRoaming = String(menu[i].name).toLowerCase().indexOf("roam")>-1;
					if(isRoaming) {
						temp.addItem(menu[i]);
					}
				}
				for(i=0;i<temp.length;i++) {
					var idx:int = menu.getItemIndex(temp[i]);
					menu.removeItemAt(idx);
				}
				
				return temp;
			}
			
			return null;
		}
		
		private static function getParent(groupName:String):Object {
			for(var i:int=0;i<menu.length;i++) {
				if(groupName==menu[i].name) return menu[i];
			}
			return null;
		}
		
	}
	
}