package core
{ internal final class Node
{
	public var value:String;
	public var next:Node;
	
	public function Node(value:String):void
	{
		this.value = value;
	}
}
}