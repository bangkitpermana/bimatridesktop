package core
{
	public class LangObject
	{
		
		public var en:String;
		public var ind:String;
		
		public function LangObject(ind:String,en:String):void {
			this.ind = ind;
			this.en = en;
		}
	}
}