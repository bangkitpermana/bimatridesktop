package core
{
	import com.stevewebster.Base64;
	
	import flash.events.EventDispatcher;
	import flash.html.script.Package;
	import flash.net.SharedObject;

	public class User extends EventDispatcher
	{
		
		public var registered:Boolean;
		public var data:Object;
		[Bindable] public var msisdn:String;
		[Bindable] public var child_msisdn:String;
		public var token:String;
		[Bindable] public var email:String;
		
		[Bindable] public var type:String;
		[Bindable] public var nameplus:String;//add by bangkit
		[Bindable] public var status:String;//add by bangkit
		[Bindable] public var shareQuotaMsisdn:String;//add by bangkit for share quota
		[Bindable] public var note1:String;
		[Bindable] public var note2:String;
		//[Bindable] public var note2:String;
		
		
		[Bindable] public var dataLimit:uint;
		[Bindable] public var isDataLimitOn:Boolean;
		[Bindable] public var lang:String = 'en';
<<<<<<< HEAD
		[Bindable] public var version:String = '1.4.6';
=======
<<<<<<< HEAD
		[Bindable] public var version:String = '1.4.5';
=======
		[Bindable] public var version:String = '1.4.1';
>>>>>>> 116b1975f2796f3dfa0d7077414a18b9a0111886
>>>>>>> 837a975a0a4ecb8cf79dee4fe6f9249c5010d724
		
		private var m:Manager = Manager.getInstance();
		
		public function User():void {
			lang = 'en';
		}
		
		public function resetByLogout():void {
			registered = false;
			data = null;
			msisdn = '';
			token = '';
		}
		
		private var so:SharedObject;
		public function loadUserPreferences():void {
//			so = SharedObject.getLocal("bima");
//			//so.clear();
//			if(so.data.preferences==null) {
//				dataLimit = 25;
//				isDataLimitOn = true;
//				lang = 'en';
//				saveUserPreferences();
//			} else {
//				dataLimit = so.data.preferences.dataLimit;
//				isDataLimitOn = so.data.preferences.isDataLimitOn;
//				lang = so.data.preferences.lang;
//			}
			dataLimit = 0;
			isDataLimitOn = false;
			lang = 'id';
			dispatchEvent(new BimaEvent(BimaEvent.USER_PREFERENCES_LOADED));
		}
		
		public function saveUserPreferences():void {
//			var preferences:Object = new Object();
//			preferences.dataLimit = dataLimit;
//			preferences.isDataLimitOn = isDataLimitOn;
//			preferences.lang = lang;
//			so.data.preferences = preferences;
			
			//call service so user will get pull notif when below dataLimit
			Service.saveSubscriberSettings();
		}
		
		public function getUIdata(key:String):Object {
			try {
				return data.ui[key];
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getContent(key:String):Array {
			try {
				return data.ui[key].content;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function get3no():String {
			if(!data) return null;
			return Base64.decode(data.msisdn);
		}
		
		public function getBalance():String {
			if(!data) return 'Rp. 0,-';
			return data.ui.balance.balance;
		}
		
		public function getValidity():String {
			if(!data) return null;
			return data.ui.validity.validity;
		}
		
		public function getTelpOnNet():Object {
			try {
				return data.ui.voice_onnet;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getTelpOffNet():Object {
			try {
				return data.ui.voice_offnet;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getSmsOnNet():Object {
			try {
				return data.ui.sms_onnet;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getSmsOffNet():Object {
			try {
				return data.ui.sms_offnet;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getDataQuota():Object {
			try {
				return data.ui.data;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getNotification():Object {
			try {
				return data.ui.notification;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getUnreadMessage():int {
			try {
				return data.ui.notification.unread;
			} catch(e:Error) {
				return 0;
			}
			return 0;
		}
		
		public function getFreeSites():Array {
			try {
				return data.ui.free;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		public function getBlackberry():Object {
			try {
				return data.ui.blackberry;
			} catch(e:Error) {
				return null;
			}
			return null;
		}
		
		private function autoCheckProfile():void {
			
		}
		
	}
}