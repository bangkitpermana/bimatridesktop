package core
{
	
	import air.net.URLMonitor;
	
	import com.stevewebster.Base64;
	
	import components.MainWindow;
	import components.ProfileContent;
	
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filesystem.FileMode;
	import flash.filesystem.FileStream;
	import flash.html.HTMLLoader;
	import flash.net.NetworkInfo;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestMethod;
	import flash.net.URLVariables;
	import flash.utils.Timer;
	import flash.utils.getTimer;
	
	import utils.MD5;
	import utils.SHA1;
	
	public class Service {
		
		//host services
		public static const API_ENVIRONMENT:String = "/pro";
		public static const API_SERVER:String = "http://180.214.232.99"; //production
//		public static const API_ENVIRONMENT:String = "/odp"; // staging
//		public static const API_SERVER:String = "http://180.214.232.98"; // staging
//		public static const API_ENVIRONMENT:String = "/sim"; // simulator
//		public static const API_SERVER:String = "http://10.70.5.4";//"http://180.214.232.98"; // staging
		public static const MALLID:String = "526";//"1030";//pro=526;odp=1030 //fortesting stagging juga = 1074
		public static const CHAINMERCHANT_ODP:String = "106";//"1023";//pro=106;odp=1023
		
		public static const API_HOST:String = API_SERVER + API_ENVIRONMENT;
// before edit force update
//		public static const SECRET_KEY:String = '3ed183e8fed005cd'; //win: 77ceb455acf632c7 ; mac: 99ceb455acf632c9
	//	public static const SECRET_KEY:String = '7ae1e92993e244f2'; //iphone to tr autologin

		public static const SECRET_KEY:String = 'ca50e77ef1eedbd2';//fix for desktop after for force update
	
		
		
		//for postpaid test 628982048669
//		public static const SECRET_KEY:String = '1d52d7d836c9722c'; //win: 1d52d7d836c9722c ; mac: 7493491c807b7db2
		public static const API_USERNAME:String = 'odpaltermyth';
		public static const API_PASSWORD:String = '099ac4e3265e947d4e2f5b7e3d62609a';
		public static const SERVICE_CLAIM_BONUS:String = "/api/selfcare/promo/claim";
		
		//services
		public static const SIGNIN:String = "/sso/signin";
		public static const SIGNOUT:String = "/sso/signout";
		public static const SERVICE_GET_PROFILE:String = "/sso/getprofile";
		public static const SERVICE_GET_PACKAGE:String = "/api/selfcare/menu/get3";
		public static const SERVICE_DEVICE_SETTING:String = "/sso/subs/devicesetting";
		public static const SERVICE_GET_NEW_TOKEN:String = "";
		public static const SERVICE_GET_PROMO:String = "/api/selfcare/promo/gets";  //add by bangkit
		
		
		public static const SERVICE_GET_INBOX:String = "/api/selfcare/notification/inbox";
		public static const SERVICE_MARK_MESSAGE_AS_READ:String = "/api/selfcare/notification/read";
		public static const SERVICE_VOUCHER_RELOAD:String = "/api/selfcare/balance/reload";
		public static const SERVICE_VOUCHER_TKM:String = "/api/selfcare/tkc/verify";
		public static const SERVICE_CHECK_TKM:String = "/api/selfcare/tkc/check";
		public static const SERVICE_REDEEM_TKM:String = "/api/selfcare/tkc/redeem";
		public static const SERVICE_ACCUMULATE_TKM:String = "/api/selfcare/tkc/accumulate";
		public static const SERVICE_GET_PACKAGE_IMG:String = "/api/selfcare/img/package";
		public static const SERVICE_GET_FREESITE_IMG:String = "/api/selfcare/img/freesite";
		public static const SERVICE_BUY_PACKAGE:String = "/api/selfcare/package/buy";
		public static const SERVICE_STOP_PACKAGE:String = "/api/selfcare/package/stop";
		public static const SERVICE_BUY_PULSA:String = "/api/selfcare/balance/buypulsa";
		public static const SERVICE_GET_SPECIAL_PACKAGE:String = "/api/selfcare/package/recommendation";
		public static const SERVICE_GET_TIPS:String = "/api/selfcare/posts/index/";
		public static const SERVICE_GET_CONTACTS:String = "/api/selfcare/contactus/get";
		public static const SERVICE_GET_UPSELL:String = "/api/selfcare/package/upsell";
		public static const SERVICE_CALL_PLAN_VIEW:String = "/api/selfcare/callplan/view";
		public static const SERVICE_CALL_PLAN_CHANGE:String = "/api/selfcare/callplan/change";
		public static const SERVICE_CREDIT_LIMIT_INFO:String = "/api/selfcare/creditlimit/info";
		public static const SERVICE_CREDIT_LIMIT_CHANGE:String = "/api/selfcare/creditlimit/change";
		public static const SERVICE_BILLING_INFO:String = "/api/selfcare/billing/info";
		public static const SERVICE_BILLING_PAYMENT:String = "/api/selfcare/billing/payment";
		public static const SERVICE_BILLING_SUMMARY:String = "/api/selfcare/billing/summary";
		public static const SERVICE_UPDATE_EMAIL:String = "/api/selfcare/hybrid/email";
		public static const SERVICE_SEND_INVOICE:String = "/api/selfcare/hybrid/resendinvoice";
		public static const SERVICE_RECEIVE_BILL:String = "/api/selfcare/hybrid/receive_bill";
		public static const SERVICE_HYBRID_BILL:String = "/api/selfcare/hybrid/bill";
		// edit bangkit
		public static const SERVICE_UPDATE_EMAIL_POSTPAID:String = "/api/selfcare/postpaid/email";//add by bangkit
		public static const SERVICE_REMOVE_NOTIFICATIONED:String = "/api/selfcare/notification/delete";
<<<<<<< HEAD
		public static const VERSION_APP:String = "1.4.6";
=======
<<<<<<< HEAD
		public static const VERSION_APP:String = "1.4.5";
=======
		public static const VERSION_APP:String = "1.4.1";
>>>>>>> 116b1975f2796f3dfa0d7077414a18b9a0111886
>>>>>>> 837a975a0a4ecb8cf79dee4fe6f9249c5010d724
		
		//add share quota
		public static const SERVICE_ADD_CHILD_SHARE_QUOTA:String = "/api/selfcare/sq/addchild";
	
		//host payment
		public static const HOST_PAYMENT:String = "http://103.10.129.17";
		//payment service
		public static const TOKENIZATION_PAYMENT:String = "/AccountBilling/DoTokenizationPayment"; 
		public static const TOKENIZATION_VOID:String = "/AccountBilling/DoTokenizationVoid";
		public static const TOKENIZATION_CHECK_PAYMENT:String = "/AccountBilling/DoTokenizationCheckPayment";
		
		//payment constants
		public static const CHAINMERCHANT_SC:String = "1024";
		public static const CURRENCY:String = "360";
		public static const PURCHASECURRENCY:String = "360";
		public static const CREDITCARD_TESTER1:String = "5426400030108754";
		public static const CREDITCARD_TESTER2:String = "4512490000010591";
		public static const MANDIRI_CLICKPAY_TESTER:String = "4111111111111111";
		public static const DEV_SHARED_KEY:String = "3yj1FEG7tBm1"; //aslinya : "D0kU3";//test staging "3yj1FEG7tBm1"
		
		public static var m:Manager = Manager.getInstance();
		
		private static var file:File;
		
		public static var signInTime:Number=0;
		public static var getProfileTime:Number=0;
		public static var getMenuTime:Number=0;
		public static var getInboxTime:Number=0;
		public static var getFreeSiteTime:Number=0;
		public static var getTipsTime:Number=0;
		public static var settingTime:Number = 0;
		public static var renderTime:Number = 0;
		public static var getThePromoTimer:Number = 0;
		
		
		
		public static function logToFile(module:String, detail:String):void {	
			return;
			var stream:FileStream = new FileStream();
			if(!file) 
			{
				file = File.documentsDirectory.resolvePath("BimaTRI_log.txt");
//				file.createDirectory("BimaTRI");
				stream.open(file, FileMode.UPDATE);                                         
			}
			else
				stream.open(file, FileMode.APPEND);
			var str2Write:String ;
			var date:Date = new Date();
			str2Write = date.hours+":"+date.minutes+":"+date.seconds+" Performance breakdown Summary, Modul:" + module + "\r\n";
			str2Write += detail; 
			stream.writeUTFBytes(str2Write + "\r\n\r\n");
			stream.close();
			trace(str2Write);
		}
		
		public static function updateEmail(email:String, type:String):void {
		
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.email = email;
			dataOb.type_req = type;
			
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL);
		
		//	var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL_POSTPAID);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.UPDATE_EMAIL_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		//add by bangkit, api email for postpaid
		
		public static function updateEmail_postpaid(email:String, type:String):void {
			
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.email = email;
			dataOb.type_req = type;
			
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			//var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL);
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL_POSTPAID);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.UPDATE_EMAIL_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		//end
		//add by bangkit, api email for unreg email postpaid
		public static function unregEmail_postpaid(email:String, type:String, bill_type:String):void {
			
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.email = email;
			dataOb.type_req = type;
			dataOb.bill_type = bill_type;
			
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			//var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL);
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL_POSTPAID);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.UPDATE_EMAIL_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		//end
		
		public static function sendInvoiceViaEmail(email:String, invoiceIDs:String, type:String=null):void 
		{
			//use comma separated ids if needed
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.email = email;
			dataOb.invoice_id = invoiceIDs;
			if(type) dataOb.type_req = type;
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_SEND_INVOICE);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.SEND_INVOICE_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		//send mail postpaid add by bangkit
		public static function sendInvoiceViaEmailPostpaid(email:String,type:String, periode:String):void 
		{
			//use comma separated ids if needed
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.email = email;
			dataOb.periode = periode;
		//	dataOb.invoice_id = invoiceIDs;
			if(type) dataOb.type_req = type;
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL_POSTPAID);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.SEND_INVOICE_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		//end
		//=============shared quota
		public static function sendChildSharedQuota(add_child_msisdn:String):void {
			trace ("ini add_child_msisdn :" + add_child_msisdn);
			var dataOb:Object = new Object();
			var child_msisdn:String = Base64.encode(add_child_msisdn);
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.child_msisdn = child_msisdn;
			
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			//var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL);
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_ADD_CHILD_SHARE_QUOTA);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.ADD_CHILD_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		//==============end shared quota
		
		//promo/ bonus/ banner
		public static function GetThePromo():void 
		{
			var networkInterface:Object = NetworkInfo.networkInfo.findInterfaces();
			var networkInfo:Object= networkInterface[0];
			var physicalAddress : String = networkInfo.hardwareAddress.toString();
			m.macAddress = physicalAddress;
			
			//use comma separated ids if needed
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.imei = physicalAddress;
			dataOb.width = "350";
			dataOb.height = "72";
			//	dataOb.invoice_id = invoiceIDs;
			trace ("macc addressss" +  dataOb.imei);
	
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_PROMO);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				var timeStart:int = getTimer();
				try
				{
				var data:Object = JSON.parse(e.target.data);
				
				trace ("heyyy promo"+data.code);
				m.newPromo = data;
				//var images:String = data.data.href;
				
				
					trace ("image url"+data.data[0].images);
				
				
				getThePromoTimer = getTimer() - timeStart;
				
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_PROMO_RESULT,data));
				
				} 
				catch(error:Error) 
				{
					
				} 
				
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		//end
		//claim bonus
		
		public static function claim_bonus(promo_id:String):void {
			
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.imei = m.macAddress;
			dataOb.promo_id = promo_id;
			
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			//var url:URLRequest = new URLRequest(API_HOST+SERVICE_UPDATE_EMAIL);
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_CLAIM_BONUS);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.CLAIM_BONUS_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		//end
		//end
		
		public static function receiveBillRegistration(type:String):void {
			
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.type_req = type;
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_RECEIVE_BILL);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.RECEIVE_BILL_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getHybridBill(code:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.msisdn = m.user.msisdn;
			dataOb.lang = m.user.lang;
			dataOb.type_bill = code;
			
			var json:String = JSON.stringify(dataOb);
			
			var ud:URLVariables = new URLVariables();
			ud.data = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_HYBRID_BILL);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			
			var loader:URLLoader = new URLLoader(url);
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object = JSON.parse(e.target.data);
				m.dispatchEvent(new BimaEvent(BimaEvent.HYBRID_BILL_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		
		
		
		public static function signin(mobileNo:String,password:String):void {
			
			m.logger.addLog(2,"\n\nSIGNIN ATTEMP\n");
			trace ("this is sginin from service as");
			var msisdn:String = Base64.encode(mobileNo);
			var pswd:String = Base64.encode(password);
			trace(msisdn,pswd);
			m.user.msisdn = msisdn;
			
			var data:Object = new Object();
			data.msisdn = msisdn;
			data.secret_key = SECRET_KEY;
			data.password = pswd;
			data.lang = m.user.lang;
			data.version = VERSION_APP;
			
			
			var timer:Timer = new Timer(30000,1);
			//timer.start();
			var json:String = JSON.stringify(data);
			m.logger.addLog(2,"\nparameters = " + json + "\n");
			
			
			var ud:URLVariables = new URLVariables();
			ud.jsondata = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SIGNIN);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			m.logger.addLog(2,"\nurl="+API_HOST+SIGNIN+"\n");
		
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				timer.reset();
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					m.logger.addLog(2,"\nresult="+e.target.data+"\n");
					trace("SignIn JSON : " + e.target.data);
					data = JSON.parse(e.target.data);
					
				} catch(e:Error) { //server error
					data = {code:'666'}
					trace("SignIn Failed");
				}
				
				if(data.code=='200'|| data.code =='301' || data.code == '400' ) {
					trace ("cek 200 301");
					
					m.user.token = data.data.token;
					m.user.lang = data.data.language;
				//	trace("status adalah"+ data.data.profile[7].id);
					m.dispatchEvent(new Event("languageChanged"));
					GetThePromo();
				
				}
				
				
				signInTime = getTimer() - timeStart;
				//logToFile("","SignIn","signin",(getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.SIGNIN_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				m.logger.addLog(2,"\nservice I/O error="+JSON.stringify(e)+"\n");
				timer.reset();
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				trace("IORError SignIn" + e.text);
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
			timer.addEventListener(TimerEvent.TIMER_COMPLETE, function onTimeOut(e:Event):void {
				try {
					loader.close();
				} catch(e:Error) {
					trace(e);					
				}
				trace("LOGIN TIMEOUT");
				timer.removeEventListener(TimerEvent.TIMER_COMPLETE,onTimeOut);
				timer.reset();
				m.dispatchEvent(new BimaEvent(BimaEvent.SIGNIN_TIMEOUT_ERROR));
			});
			
		
		}
		
		public static function getProfile(mobileNo:String=null):void {
			trace ("this is getprofile from service.as");
			m.logger.addLog(2,"\n\nGET PROFILE ATTEMP\n");
			
			var data:Object = new Object();
			data.secret_key = SECRET_KEY;
			data.lang = m.user.lang;
			data.version = VERSION_APP;
			if(mobileNo) {
				var msisdn:String = Base64.encode(mobileNo); //will not exist later, just need secret key
				data.msisdn = msisdn;
				m.user.msisdn = msisdn;
			}
			
			var json:String = JSON.stringify(data);
			m.logger.addLog(2,"\nparameters = " + json + "\n");
			
			var ud:URLVariables = new URLVariables();
			ud.jsondata = json;
			m.logger.addLog(2,"\nparameters = " + json + "\n");
			m.logger.addLog(2,"\nurl = " + API_HOST+SERVICE_GET_PROFILE + "\n");
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_PROFILE);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
		
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object
				try {
					m.logger.addLog(2,"\nresult = " + e.target.data + "\n");
					trace(e.target.data);
					data = JSON.parse(e.target.data);
					
				} catch(e:Error) { //server error
					
					data = {code:'-1'}
				}
				//before if(data.code=='200' || data.code=='301'|| data.code=='400')
				if(data.code=='200') {
					trace ("disini");
					m.user.token = data.data.token;
					m.user.msisdn = data.data.msisdn;
					
					
				}
				GetThePromo();
				trace ("belum");
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_PROFILE_CALLBACK,data));
				trace ("udah");
				getProfileTime = getTimer() - timeStart;
				//logToFile("","BACK TO PROFILE","GetProfile",(getTimer() - timeStart));
			});
			
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				m.logger.addLog(2,"\nservice I/O error="+JSON.stringify(e)+"\n");
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		
		
		}
		
		public static function getNewToken():void {
			var data:Object = new Object();
			data.secret_key = SECRET_KEY;
			data.lang = m.user.lang;
			
			var json:String = JSON.stringify(data);
			
			var ud:URLVariables = new URLVariables();
			ud.jsondata = json;
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_NEW_TOKEN);
			url.method = URLRequestMethod.POST;
			url.data = ud;
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'-1'}
				}
				if(data.code=='200') {
					m.user.token = data.data.token;
				}
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_NEW_TOKEN_CALLBACK,data));
//				//logToFile("GetNewToken : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
//				//logToFile("","BACK TO PROFILE","GetProfile",(getTimer() - timeStart));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static var autoLoginRetry:int = -99;
		public static var timer:Timer;
		public static var monitor:URLMonitor;
		
		public static function autoLogin(mobileNo:String=null):void {
			m.logger.addLog(2,"\n\nAUTOLOGIN ATTEMPT\n");
			
			var data:Object = new Object();
			data.secret_key = SECRET_KEY;
			data.lang = m.user.lang;
			//  add parameter version
			data.version = VERSION_APP;
		
			if(mobileNo) {
				var msisdn:String = Base64.encode(mobileNo); //will not exist later, just need secret key
				data.msisdn = msisdn;
				m.user.msisdn = msisdn;
				
			}
			
			var json:String = JSON.stringify(data);
			m.logger.addLog(2,"\nparameters = " + json + "\n");
			var ud:URLVariables = new URLVariables();
			ud.jsondata = json;
			trace ("ini data : " + ud.jsondata);
			
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_PROFILE);
			m.logger.addLog(2,"\nurl = " + API_HOST+SERVICE_GET_PROFILE + "\n");
			url.method = URLRequestMethod.POST;
			url.data = ud;
			var loader:URLLoader = new URLLoader(url);
			if(timer == null) {
				timer = new Timer(60000,1);
				timer.addEventListener(TimerEvent.TIMER_COMPLETE, function onTimeOut(e:Event):void {
					timer.reset();
					loader.close();
					timer.removeEventListener(TimerEvent.TIMER_COMPLETE,onTimeOut);
//					m.dispatchEvent(new BimaEvent(BimaEvent.AUTOSIGN_TIMEOUT_ERROR));
					m.dispatchEvent(new BimaEvent(BimaEvent.SHOW_LOGIN_WINDOW));
					m.avatar.hideLoadingCircle();
					
					m.logger.addLog(2,"\nno respond from server, exceed 1 minute\n");
				});
				timer.start();
			}
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				timer.reset();
//				monitor.stop();
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object
				try {
					trace("AutoLogin" + e.target.data);
					m.logger.addLog(2,"\nresult=" + String(e.target.data) + "\n");
//					(data as String)
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					trace("AutoLogin Failed");
					data = {code:'666'}
				}
				// edited by bangkit, for major manual
				if(data.code=='200' || data.code=='400') {
					trace ("autologin in 400");
					m.user.token = data.data.token;
					m.user.msisdn = data.data.msisdn;
					m.user.lang = data.data.language;
					
					m.reloadPageReady = false;
					m.dispatchEvent(new Event("languageChanged"));
					m.packageMenuCreated = false;
					m.billOwn = null;
					m.requestPackagefresh = true;
				    m.appWindow.showProfilePage();
					autoLoginRetry = -99;
				} 
				//edit by bangkit update versi autologin minor
				
				
				/* if(data.code=='400') {
					
					m.user.token = data.data.token;
					m.user.msisdn = data.data.msisdn;
					m.user.lang = data.data.language;
					
					m.reloadPageReady = false;
					m.dispatchEvent(new Event("languageChanged"));
					m.packageMenuCreated = false;
					m.billOwn = null;
					m.requestPackagefresh = true; 
					m.appWindow.showProfilePageMinor();
					//autoLoginRetry = -99;
					
				}  */
				
				else if(data.code == '208') {
					if(autoLoginRetry == -99) {
						autoLoginRetry = int(data.data.retry_auto_login);
						trace("ral" + autoLoginRetry);
						trace ("ini ke autologin loh");
					}
				}
				//logToFile("","AUTOLOGIN","GetProfile",(getTimer() - timeStart));
				
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_PROFILE_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				m.logger.addLog(2,"\nservice I/O error="+JSON.stringify(e)+"\n");
				timer.reset();
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getPackage(lang:String=null):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.version = "0";
			if(lang) {
				dataOb.lang = lang;
			} else {
				dataOb.lang = m.user.lang;
			}
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_PACKAGE);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					trace("Pckg Failed");
					data = {code:'666'}
				}
				
				m.packageReady = true;
				getMenuTime = getTimer() - timeStart;
				//logToFile("","GET PACKAGES","menu/get3",(getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_PACKAGE_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
			
		}
		
		public static function getInbox():void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_INBOX);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					trace("Inbox Failed");
					data = {code:'666'}
				}
				//logToFile("","GET INBOX","inbox",(getTimer() - timeStart));
				getInboxTime = getTimer() - timeStart;
				m.processToWatch = "inbox";
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_INBOX_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getContact(lang:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "0";
			
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_CONTACTS);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onContact(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onContact);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					trace("Contact Failed");
					data = {code:'666'}
				}
				//logToFile("","GET CONTACT","contactus/get",(getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_CONTACT_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getCallPlanView():void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			//			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_CALL_PLAN_VIEW);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onCallPlanView(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onCallPlanView);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
//				//logToFile("GetCallPlan : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_CALL_PLAN_VIEW_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_CALL_PLAN_VIEW_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getUpsell(pkgId:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.pkgid = pkgId;
			//			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_UPSELL);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onGetUpsell(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onGetUpsell);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
//				//logToFile("GetUpsell : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_UPSELL_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_UPSELL_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getCreditLimitInfo():void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			//			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_CREDIT_LIMIT_INFO);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onCreditLimitInfo(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onCreditLimitInfo);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
//				//logToFile("GetCreditLimit : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_CREDIT_LIMIT_INFO_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_CREDIT_LIMIT_INFO_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getBillingInfo():void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			//			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_BILLING_INFO);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onBillingInfo(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onBillingInfo);
				var data:Object;
				try {
					
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
//				//logToFile("GetBillInfo : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_INFO_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_INFO_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function getBillingSummary():void {
			//revision 05062013: When hybrid, bypass data using billown
/*			if(m.isHybrid)
			{
				var billData:Object = new Object;
				data.treeReady = true;
//				data.label=m.billOwn.
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_SUMMARY_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_SUMMARY_RESULT,data));
				return;
			}*/
			
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_BILLING_SUMMARY);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onBillingSummary(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onBillingSummary);
				var data:Object;
				trace("GetBillSumm " + e.target.data);
				try {
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				//logToFile("","BILL SUMMARY", "payment/summary", (getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_SUMMARY_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_SUMMARY_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
// get promo bannner
		/*
		public static function getPromo():void {
			
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_BILLING_SUMMARY);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onBillingSummary(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onBillingSummary);
				var data:Object;
				trace("GetBillSumm " + e.target.data);
				try {
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				//logToFile("","BILL SUMMARY", "payment/summary", (getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_SUMMARY_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_BILLING_SUMMARY_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		*/
//end
		
		public static function callPlanChange(newPlan:String, currentCallPlan:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.new_callplan = newPlan;
			dataOb.current_callplan = currentCallPlan;
			trace(newPlan);
			//			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_CALL_PLAN_CHANGE);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onCallPlanChange(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onCallPlanChange);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
//				//logToFile("ChangeCallPlan : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.CALL_PLAN_CHANGE_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.CALL_PLAN_CHANGE_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		
		//revision 05042013 : Post Paid Payment
		public static function postPaidPayment(msisdn:String, amount:String, method:int):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.token = m.user.token;
			dataOb.version = "1";
			dataOb.msisdn = msisdn;
			dataOb.amount = amount;
			dataOb.method = method;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_BILLING_PAYMENT);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onPostPaidChange(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onPostPaidChange);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace("PostPaidPayment : " + e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
//				//logToFile("PostPaidPayment : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.BILLING_PAYMENT_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.BILLING_PAYMENT_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		//Revision 04032013: Credit Limit Change API
		public static function creditLimitChange(change:int, period:int, amount:String, days:String, reason:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			
//			if(change == 0) dataOb.change = "inc"; else dataOb.change = "dec";
			if(period == 0) dataOb.periode = "temp"; else dataOb.periode = "fix";
			dataOb.amount = amount;
			dataOb.days = days;
			dataOb.reason = reason;
			
			//			dataOb.lang = m.user.lang;
			//			dataOb.version = m.user.version;
			dataOb.version = "1";
			
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_CREDIT_LIMIT_CHANGE);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onCreditLimitChange(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onCreditLimitChange);
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					trace(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
//				//logToFile("ChangeCreditLimit : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.CREDIT_LIMIT_CHANGE_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.CREDIT_LIMIT_CHANGE_RESULT,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}

				public static function markMessageAsRead(id:int):void {
					var dataOb:Object = new Object();
					dataOb.username = API_USERNAME;
					dataOb.password = API_PASSWORD;
					dataOb.msisdn = m.user.msisdn;
					dataOb.token = m.user.token;
					dataOb.lang = m.user.lang;
					dataOb.notification_id = [String(id)];//id
					
					var data:String = JSON.stringify(dataOb);
					var url:URLRequest = new URLRequest(API_HOST+SERVICE_MARK_MESSAGE_AS_READ);
					var v:URLVariables = new URLVariables();
					v.data = data;
					url.data = v;
					url.method = URLRequestMethod.POST;
					
					var loader:URLLoader = new URLLoader(url);
					var timeStart:int = getTimer();
					loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
						loader.removeEventListener(Event.COMPLETE, onResult);
						
						var data:Object;
						try {
							data = JSON.parse(e.target.data);
						} catch(e:Error) { //server error
							data = {code:'666'}
						}	

								
//				//logToFile("MarkAsRead : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.SET_MESSAGE_MARK_AS_READ,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
				// add by bangkit : remove notification	
				public static function removeNotification(idRem:String=null):void {
					trace (" sampe nihh");
					var dataOb:Object = new Object();
					dataOb.username = API_USERNAME;
					dataOb.password = API_PASSWORD;
					dataOb.msisdn = m.user.msisdn;
					dataOb.token = m.user.token;
					dataOb.lang = m.user.lang;
					dataOb.id = idRem;//id
					//trace ("Ini id : " + id);
					//trace ("ini notification_id :" + notification_id);
					var data:String = JSON.stringify(dataOb);
					var url:URLRequest = new URLRequest(API_HOST+SERVICE_REMOVE_NOTIFICATIONED);
					var v:URLVariables = new URLVariables();
					v.data = data;
					url.data = v;
					url.method = URLRequestMethod.POST;
					
					var loader:URLLoader = new URLLoader(url);
					var timeStart:int = getTimer();
					loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
						loader.removeEventListener(Event.COMPLETE, onResult);
						
						var data:Object;
						//trace ("ini output : " + data as String);
						try {trace ("ini response : " + e.target.data);
							data = JSON.parse(e.target.data);
						
						} catch(e:Error) { //server error
							data = {code:'666'}
						}
						//logToFile("MarkAsRead : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
						m.dispatchEvent(new BimaEvent(BimaEvent.GET_REMOVE_NOTIFICATION,data));
					});
					loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
						loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
						
						m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
					});
				}
				
				
				// end remove notification
		public static function pullNotification():void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			dataOb.pull = 1;
			dataOb.limit = 1;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_INBOX);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					data = JSON.parse(e.target.data);
					var date:Date = new Date();
					
					trace(date.hours+":"+date.minutes+":"+date.seconds+" Performance breakdown Summary, Modul: GetInbox");
//					trace(data.version);
				} catch(e:Error) { //server error
					trace("PullNotif Failed");
					data = {code:'666'}
				}
								
				//logToFile("","NOTIFICATION","notication",(getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_PULL_NOTIFICATION_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				trace("PullNotif Failed " + e);
				
				//retry pull notification
				m.pullNotification();
			});
		}
		
		public static function reload(voucherId:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			//dataOb.lang = m.user.lang;
			dataOb.voucher_pin = voucherId;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_VOUCHER_RELOAD);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
//					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
								
//				//logToFile("VoucherReload : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.RELOAD_BY_VOUCHER_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.RELOAD_RESULT));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.RELOAD_RESULT));
			});
		}
		
		public static function tkcVerify(tkcId:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			dataOb.tkc_code = tkcId;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_VOUCHER_TKM);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
					//					data = JSON.parse("{\"code\":\"200\",\"message\":\"success\",\"data\":{\"desc\":\"Terima kasih telah mencoba koneksi internet baru dari TRI, pilih \\\"LANJUT\\\" dan mendapatkan AlwayON 12Bln atau \\\"STOP\\\"\",\"action\":[{\"label\":\"LANJUT\",\"url\":\"http:\/\/180.214.232.98\/odp\/api\/selfcare\/tkc\/lanjut\"},{\"label\":\"STOP\",\"url\":\"http:\/\/180.214.232.98\/odp\/api\/selfcare\/tkc\/stop\"}],\"type\":\"moveon\",\"isauto\":\"0\"}}");
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
//				//logToFile(" TKC Verify: " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.VERIFY_TKC_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.VERIFY_TKC_RESULT));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.VERIFY_TKC_RESULT));
			});
		}
		
		public static function tkcCheck():void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_CHECK_TKM);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
//				//logToFile(" TKC Check: " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.CHECK_TKC_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.CHECK_TKC_RESULT));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.CHECK_TKC_RESULT));
			});
		}
		
		public static function tkcRight(tkcId:String, type:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			dataOb.tkc_code = tkcId;
			dataOb.type = type;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(m.rightUrl);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
//					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
				trace(m.rightLabel + " : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.RIGHT_TKC_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.RIGHT_TKC_RESULT));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.RIGHT_TKC_RESULT));
			});
		}
		
		public static function tkcLeft(tkcId:String, type:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			dataOb.tkc_code = tkcId;
			dataOb.type = type;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(m.leftUrl);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			trace(m.leftUrl);
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
//					data = JSON.parse("{\"code\":\"200\",\"message\":\"success\",\"data\":{\"redeemcode\":\"DC9626283733\",\"transactionid\":\"123654\",\"desc\":\"Berikan kode DC9626283733 ke Outlet bertanda TRIms untuk membelanjakan Rp. 8000 . Kode valid sampai 14\/03\/2013\"}}");
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
				trace(m.leftLabel + " : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.LEFT_TKC_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.LEFT_TKC_RESULT));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.LEFT_TKC_RESULT));
			});
		}
		
		public static function getPackageImageUrl(id:String,size:String='75x75'):String {
		
			var url:String = API_HOST+SERVICE_GET_PACKAGE_IMG+'?id='+id+'&s='+size;
			//trace(url);
			return url;
		}
		
		public static function getFreeSiteImageUrl(id:String,size:String='50x50'):String {
			return API_HOST+SERVICE_GET_FREESITE_IMG+'?id='+id+'&s='+size;
		}
		
		public static function getTipsUrl(id:String):String {
			return API_HOST+SERVICE_GET_TIPS+id+"?lang="+m.user.lang;
		}
		
		public static function buyUsingPulsa(packageId:String):void {
			trace("Buying Pulsa Service");
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			dataOb.pkgid = packageId;
			//dataOb.amount = packageAmount;//optional
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_BUY_PULSA);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
				trace("BuyPulsa : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.BUY_PULSA_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.BUY_PULSA_RESULT));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				trace("Buy Pulsa Failed");
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.BUY_PULSA_RESULT));
			});
		}
		
		public static function buyPackage(packageId:String, isHybrid:Boolean=false, autorenewal:Boolean=false):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			dataOb.pkgid = packageId;
			trace ("buyPackage from service");
			if(isHybrid) {
				trace("Buy using kredit");
				dataOb.hybrid_balance_credit = 1;
			}
			//dataOb.amount = packageAmount;//optional
			if(autorenewal) {
				dataOb.auto_renewal = '1';
			}
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_BUY_PACKAGE);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
				trace("BuyPackage : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.BUY_PACKAGE_CALLBACK,data));
				m.dispatchEvent(new BimaEvent(BimaEvent.BUY_RESULT));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.BUY_RESULT));
			});
		}
		
		public static function getSpecialPackage():void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_GET_SPECIAL_PACKAGE);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					trace("SpclPckg Failed");
					data = {code:'666'}
				}
				
				//logToFile("","SPECIAL PACKAGE", "GetSpclPckg", (getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.GET_SPECIAL_PACKAGE_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function signout():void {
			var url:URLRequest = new URLRequest(API_HOST+SIGNOUT+'?secret_key='+SECRET_KEY+'&token='+m.user.token+'&lang='+m.user.lang);
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
				//logToFile("","SIGN OUT","SignOut",(getTimer() - timeStart));
				m.dispatchEvent(new BimaEvent(BimaEvent.SIGNOUT_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		public static function saveSubscriberSettings(version:String=null):void {
			var dataOb:Object = new Object();
			dataOb.secret_key = SECRET_KEY;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.new_lang = m.user.lang;
			dataOb.quota_threshold = String(m.user.dataLimit);
			if(version) dataOb.version = version;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_DEVICE_SETTING);
			var v:URLVariables = new URLVariables();
			v.jsondata = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					trace("DevSet Failed");
					data = {code:'666'}
				}
				
				m.processToWatch = "setting";
				settingTime = (getTimer() - timeStart);
				trace("DeviceSetting : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.DEVICE_SETTING_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		private static function getUrlVar(key:String,value:String, first:Boolean=false):String {
			return (first?'':'&') + key + '=' + value;
		}
		
		public static var htmlLoader:HTMLLoader = new HTMLLoader();
		public static var urlLoader:URLLoader = new URLLoader();
		
		public static function buyPackage2(pkgOb:Object, payment:Object):void {
			//trace ("this is URL" + url);
			htmlLoader.height = 600;
			var topup:String;
			//trace(pkgOb.group);
			if(String(pkgOb.group).toLowerCase()=='pulsa' || String(pkgOb.group).toLowerCase()=='balance' || String(pkgOb.group).toLowerCase()=='reload'  || String(pkgOb.group).toLowerCase()=='isi ulang') {
				topup = '01';
//			} else if(String(pkgOb.group).toLowerCase()=='package'){
//				topup = '03';
			} else {
				topup = '02';
			}
// Override trx_type as topup			
//--------------------------------------------------			
			if(pkgOb.hasOwnProperty('trx_type'))
			{
				trace("TOP UP = " + pkgOb.trx_type);
				topup = pkgOb.trx_type;
			}
			var url:URLRequest;
			var isBCA:Boolean = (payment.paymentname as String).toLowerCase().indexOf("bca") != -1 && payment.paymentchannel=='03';
			var isCIMB:Boolean = payment.paymentchannel=='04';
			var isCC:Boolean = payment.paymentchannel=='01';
			if(isBCA) 
			{ //bca
				url = new URLRequest(payment.paymenturl); // + '?' + generateBCAparams(topup,pkgOb,payment.paymentchannel));
				url.data = generateBCAparams(topup,pkgOb,payment.paymentchannel);
				 
			} 
			//edit bangkit, masih salah doku
			else if(isCIMB) 
			{ //CIMB
				url = new URLRequest(payment.paymenturl); // + '?' + generateBCAparams(topup,pkgOb,payment.paymentchannel));
				url.method = URLRequestMethod.POST;
				var v:URLVariables = new URLVariables();
				v.data = generateCIMBparams(topup,pkgOb,payment.paymentchannel);
				trace(v.data);
				url.data = v;
			} else 
			{ //mandiri
				url = new URLRequest(payment.paymenturl + '?' + generateDokuParams(topup,pkgOb,payment.paymentchannel));
			//	trace ("payment.paymenturl"+payment.paymenturl);
			//trace ("masuk mana?" + payment.paymentchannel);
			
			
			}
			if(isCC){
				//payment.paymentchannel=='02';
				//payment.paymentchannel = {code:'02'};
			
				var raw:Object = m.reponsInit1.data.param;
				var out:URLVariables = new URLVariables();
				for (var k:String in raw) {
					out[k] = raw[k];
				}
			/*	if (m.reponsInit1.data.payment_channel['code'] == '01')
				{ trace ("ini kode 01"); } */
				//m.reponsInit1.data.param.paymentchannel=='02';
				var thisurl:String = m.reponsInit1.data.url;
				//trace ("thispchannel"+ payment.paymentchannel);
				//payment.paymentchannel=='01';
				url = new URLRequest(thisurl);
				//url = new URLRequest("http://103.10.129.17/Suite/Receive");
			
				//trace ("heyy ini url pertama"+url);
				url.data = out;
				//trace ("heyy ini url"out);
				//m.isLoadingDoku = true;	
				//htmlLoader.height = 600;
				//htmlLoader.height = 600;
				//htmlLoader.load(url);
				//m.dispatchEvent(new BimaEvent(BimaEvent.DOKUCC_SUCCESS));
				
			} 
			//trace ("isbcaaaaa  1"+ isBCA);
			url.method = URLRequestMethod.POST;
			
//revision 19032013, BCA pipeline change
//-------------------------------------------------------------------------------------------------			
			if(isBCA || isCIMB) {
				//trace ("isbcaaaaa");
				urlLoader.addEventListener(Event.COMPLETE, function onURLResult(e:Event):void {
					urlLoader.removeEventListener(Event.COMPLETE,onURLResult);
					trace(e.target.data);
					var data:Object = JSON.parse(e.target.data);
					if(data.code == '200') {
						//trace("loading url BCA");
						
						if( !htmlLoader.hasEventListener(Event.COMPLETE) ) {
							htmlLoader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
								trace("loaded url BCA/CIMB");
								m.isLoadingDoku = false;
								m.dispatchEvent(new BimaEvent(BimaEvent.DOKU_PAYMENT_CALLBACK, htmlLoader));
							});
						}
						if( !htmlLoader.hasEventListener(Event.LOCATION_CHANGE) ) {
							htmlLoader.addEventListener(Event.LOCATION_CHANGE, function onChange(e:Event):void {
								m.dispatchEvent(new BimaEvent(BimaEvent.DOKU_LOCATION_CHANGE,Object(e).location));
								m.isLoadingDoku = true;
							});
						}
						
						url = new URLRequest(data.data.url);
						url.method = URLRequestMethod.POST;
						
						var raw:Object = data.data.param;
						var out:URLVariables = new URLVariables();
						for (var k:String in raw) {
							out[k] = raw[k];
						}
						url.data = out;
					
						
						htmlLoader.load(url);
					}
				});
			//	trace("is URL null "+url);
				urlLoader.load(url);
			} else {
				//just load using htmlLoader
				if( !htmlLoader.hasEventListener(Event.COMPLETE) ) {
					htmlLoader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
						//htmlLoader.removeEventListener(Event.COMPLETE,onResult);
					//	trace ("htmlloader" +htmlLoader.height);
					//	trace('complete load url');
					//	trace(e.target);
						m.isLoadingDoku = false;
						m.dispatchEvent(new BimaEvent(BimaEvent.DOKU_PAYMENT_CALLBACK, htmlLoader));
					
						
						
					});
				}
				if( !htmlLoader.hasEventListener(Event.LOCATION_CHANGE) ) {
					htmlLoader.addEventListener(Event.LOCATION_CHANGE, function onChange(e:Event):void {
						m.dispatchEvent(new BimaEvent(BimaEvent.DOKU_LOCATION_CHANGE,Object(e).location));
	//					trace(Object(e).location);
						m.isLoadingDoku = true;
					});
				}
				
				htmlLoader.height = 600 ;
				htmlLoader.load(url);
				
			}
			//-------------------------------------------------------------------------------------------------			
		}
		
		private static function generateDokuParams(topup:String, pkgOb:Object, paymentChannel:String):String {
			var transIdMerchant:String = topup + (1000+Math.round(Math.random()*1000)) + getCustomDateFormat(new Date());
			if(transIdMerchant.length > 14) {
				transIdMerchant = transIdMerchant.substr(0,14);
			}
			var sessionId:String = MD5.hash(transIdMerchant);
			if(sessionId.length > 48) {
				sessionId = sessionId.substr(0,47);
			}			
			var amount:String;
			if(pkgOb.tariff || pkgOb.item_tarrif_promo) { //package 
				amount = getAmount(pkgOb.tariff || pkgOb.item_tarrif_promo);
			} else if(pkgOb.price) { //special package
				amount = getAmount(pkgOb.price);
			}
			
			var normalizedPkgName:String = String(pkgOb.item_name || pkgOb.name ).replace(',','.');
			
			var v:URLVariables = new URLVariables();
			v.TOKEN = Base64.decode(m.user.msisdn);
			v.MALLID = MALLID;
			v.CHAINMERCHANT = CHAINMERCHANT_ODP;
			v.AMOUNT = amount;
			v.PURCHASEAMOUNT = amount;
			v.TRANSIDMERCHANT = transIdMerchant;
			v.CURRENCY = CURRENCY;
			v.PURCHASECURRENCY = PURCHASECURRENCY;
			v.SESSIONID = sessionId;
			try {
				v.EMAIL = m.contact.email[0];
			} catch(e:Error) {
				v.EMAIL = '';
			}
			v.REQUESTDATETIME = getCustomDateFormat(new Date());
//			v.WORDS = amount + CURRENCY + MALLID + DEV_SHARED_KEY + transIdMerchant;
			v.WORDS = SHA1.hash(amount + CURRENCY + MALLID + DEV_SHARED_KEY + transIdMerchant);
			v.BASKET = normalizedPkgName + ',' + amount + ',1,' + amount;
			v.PAYMENTCHANNEL = paymentChannel;
			trace("Pkg Code : " + pkgOb.item_code);
			trace ("pkg item_name :" + pkgOb.item_name);
			trace ("heeeeeeeeeyyyyyyyyyyy");
			trace ("v.token"+ v.TOKEN);
			trace ("pkgOb.item_code" + pkgOb.item_code);
			trace ("Base64.decode(m.user.msisdn)" + Base64.decode(m.user.msisdn));
			trace (" m.user.token" +  m.user.token);
			//trace ("pkgOb.code" + pkgOb.code);
			
			
			
			v.ADDITIONALDATA = 'c@' + (m.isShowingPromo?pkgOb.item_code:pkgOb.code) + Base64.decode(m.user.msisdn) + ';t@' + m.user.token;
			if(pkgOb.hasOwnProperty('family')) 
			{
				trace("Pkg Type : " + pkgOb.family);
				v.ADDITIONALDATA += ';v@' + pkgOb.family;
			}
			
			trace(decodeURI(v.toString()));
//			trace(encodeURI(v.toString()));
//			trace(v.toString());
			return v.toString();
			///////
			
			
			/////
		}
		
		
		// edit doku, bangkit
		public static function generateDokuCreditCard(pkgOb:Object, payment:Object) {
			//topup,pkgOb,payment.paymentchannel
			trace ("ini generatte");
			trace (pkgOb.toString());
			
			var amount:String;
			if(pkgOb.tariff || pkgOb.item_tarrif_promo) { //package 
				amount = getAmount(pkgOb.tariff || pkgOb.item_tarrif_promo);
			} else if(pkgOb.price) { //special package
				amount = getAmount(pkgOb.price);
			}
			var username:String = API_USERNAME;
			var passwrd:String = API_PASSWORD;
			var lang:String = m.user.lang;
			//pkgOb.hasOwnProperty('trx_type');
			
			//trace(pkgOb.code+"TOP UP = " + pkgOb.trx_type);
		
			//	var data5:String = com.adobe.serialization.json.JSON.encode(pkgOb);
		//	trace ("datas :" +datax);
			var paymentMethod:String;
		//	payment.paymentchannel = '01';
			if(String(pkgOb.group).toLowerCase()=='pulsa' 
				|| String(pkgOb.group).toLowerCase()=='balance' 
				|| String(pkgOb.group).toLowerCase()=='reload'  
				|| String(pkgOb.group).toLowerCase()=='isi ulang') {
				trace ("masuk pulsa dll");
				paymentMethod = '01';
			}else { trace ("masuk else");
				paymentMethod = '02';
			}
			
			if(pkgOb.isBillingPayment){
				paymentMethod = pkgOb.trx_type;
				
			}
	
			
			
			var normalizedPkgName:String = String(pkgOb.item_name || pkgOb.name).replace(',','.');
			
		
			
			var v:URLVariables = new URLVariables();
			v.token = m.user.token;
			v.amount = amount;
			v.packageCode = pkgOb.code || pkgOb.item_code ;
			v.username = username;
			v.packagename = normalizedPkgName;
			v.msisdn = m.user.msisdn;
			v.password = passwrd;
			v.lang = lang;
			v.paymentMethod = paymentMethod;
			//var data:String = JSON.stringify(v);
			
			
			var data:String = JSON.stringify(v);
			var url:URLRequest;
			//url = new URLRequest("http://180.214.232.98/odp/api/selfcare/payment/initial1" + '?'+  decodeURI(v.toString())); // + '?' + generateBCAparams(topup,pkgOb,payment.paymentchannel));
			url = new URLRequest(payment.paymenturl + '?'+  decodeURI(v.toString()));
			var x:URLVariables = new URLVariables();
			x.data = data;
			url.data = x;
			url.method = URLRequestMethod.POST;

			
			trace ("trace payment.paymenturl" + payment.paymenturl);
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace("ini apa?" + e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
					
					m.dispatchEvent(new BimaEvent (BimaEvent.ALERT_ERROR_CC));
					return;
				}
				m.reponsInit1 = data;
				//m.tokenDokucc = m.user.msisdn;
				
				
				//trace("BuyPackage : " + (getTimer() - timeStart) + " ms, Size : " + (e.target.data as String).length + " bytes");
				m.dispatchEvent(new BimaEvent(BimaEvent.DOKUCC_SUCCESS));
				//m.dispatchEvent(new BimaEvent(BimaEvent.BUY_RESULT));*
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				m.dispatchEvent(new BimaEvent(BimaEvent.BUY_RESULT));
			});
			
			htmlLoader.load(url);
			
			
		/*	trace(decodeURI(v.toString()));
			var url:URLRequest;
			url = new URLRequest(payment.paymenturl + '?'+  decodeURI(v.toString())); // + '?' + generateBCAparams(topup,pkgOb,payment.paymentchannel));
			//url.data= v.toString;
			url.method = URLRequestMethod.POST;
			
			
			htmlLoader.load(url); */
			//return v.toString();
			///////
			/*
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_BUY_PACKAGE);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v; */
			/////
		}
		
		
		//=========end=====
		/*
		//start param dokucc
		private static function generateDokuCCparam(topup:String,pkgOb:Object, paymentChannel:String):URLVariables {
			var amount:String;
			if(pkgOb.tariff) { //package 
				amount = getAmount(pkgOb.tariff,true);
			} else if(pkgOb.price) { //special package
				amount = getAmount(pkgOb.price,true);
			}
			
			var normalizedPkgName:String = String(pkgOb.name).replace(',','.');
			
			var v:URLVariables = new URLVariables();
			v.msisdn = Base64.decode(m.user.msisdn);
			v.paymentChannel = paymentChannel;
			v.paymentMethod = topup;
			v.totalAmount = amount;
			v.transactionDate = getCustomDateFormat(new Date(),true);
			v.packageName = normalizedPkgName;
			v.packageCode = pkgOb.code;
			v.token = m.user.token;
			if(pkgOb.hasOwnProperty('family'))
			{
				trace("Pkg Type : " + pkgOb.family);
				v.paymentLevel = pkgOb.family;
			}
			
			//strace(v.toString())
			return v;
		}
		//end param dokucc
		*/
	
		
		private static function getCustomDateFormat(date:Date,isBCA:Boolean=false):String {
			var dateStr:String;
			if(!isBCA) {
				dateStr = String(date.fullYear) + (date.month+1<10?'0'+String(date.month+1):String(date.month+1)) +
					(date.date<10?'0'+String(date.date):String(date.date)) 	 + 
					(date.hours<10?'0'+String(date.hours):String(date.hours)) + 
					(date.minutes<10?'0'+String(date.minutes):String(date.minutes)) + 
					(date.seconds<10?'0'+String(date.seconds):String(date.seconds));
			} else {
				dateStr = (date.date<10?'0'+String(date.date):String(date.date)) + '/' +
					(date.month+1<10?'0'+String(date.month+1):String(date.month+1)) + '/' +
					String(date.fullYear) + ' ' +
					(date.hours<10?'0'+String(date.hours):String(date.hours)) + ':' +
					(date.minutes<10?'0'+String(date.minutes):String(date.minutes)) + ':' +
					(date.seconds<10?'0'+String(date.seconds):String(date.seconds));
			}
			return dateStr;
		}
		
		private static function getAmount(tariff:String,isBCA:Boolean=false):String { //tariff: Rp 10.000 or IDR 10,000
			var str:String = tariff;
			var isContainsComma:Boolean = tariff.indexOf(',') > -1;
			var isContainsDot:Boolean = tariff.indexOf('.') > -1;
//			var r:RegExp;
			if(isContainsComma) {
//				r = /,/;
//				str.replace(r,'');
				str = str.split(',').join('');
			}
			if(isContainsDot) {
//				r = /./;
//				str.replace(r,'');
				str = str.split('.').join('');
			}
			
			if(isBCA) {
				return str.split(' ')[1];
			}
			
			return String(str.split(' ')[1] + '.00');
		}
		
		private static function generateBCAparams(topup:String,pkgOb:Object, paymentChannel:String):URLVariables {
			var amount:String;
			if(pkgOb.tariff || pkgOb.item_tarrif_promo) { //package 
				amount = getAmount(pkgOb.tariff || pkgOb.item_tarrif_promo,true);
			} else if(pkgOb.price) { //special package
				amount = getAmount(pkgOb.price,true);
			}
			
			var normalizedPkgName:String = String(pkgOb.item_name || pkgOb.name).replace(',','.');
			
			var v:URLVariables = new URLVariables();
			v.msisdn = Base64.decode(m.user.msisdn);
			v.paymentChannel = paymentChannel;
			v.paymentMethod = topup;
			v.totalAmount = amount;
			v.transactionDate = getCustomDateFormat(new Date(),true);
			v.packageName = normalizedPkgName;
			v.packageCode = pkgOb.code || pkgOb.item_code;
			v.token = m.user.token;
			if(pkgOb.hasOwnProperty('family'))
			{
				trace("Pkg Type : " + pkgOb.family);
				v.paymentLevel = pkgOb.family;
			}
			
			//strace(v.toString())
			return v;
		}
		
/*		data={		
		"username" : "odp",			
		"password" : "80b51612bb261ffff0997a882f069307",			
		"msisdn"   : "4qeSIXHcy5cODOK0O-XmiQ==",			
		"token"    : "492252563218655"			
		"paymentLevel":"family",			
		"packageCode":"03",			
		"packageName":"POSTPAID",			
		"totalAmount":"37668",			
		"paymentMethod":"03",
		"paymentChannel":"03",			
		"redirectForward":"1",
		
		}*/
		private static function generateCIMBparams(topup:String,pkgOb:Object, paymentChannel:String):String {
			var amount:String;
			if(pkgOb.tariff || pkgOb.item_tarrif_promo) { //package 
				amount = getAmount(pkgOb.tariff || pkgOb.item_tarrif_promo,true);
			} else if(pkgOb.price) { //special package
				amount = getAmount(pkgOb.price,true);
			}
			
			var normalizedPkgName:String = String(pkgOb.item_name || pkgOb.name).replace(',','.');
			
			var v:Object = new Object();
			v.username = API_USERNAME;
			v.password = API_PASSWORD;
			v.msisdn = m.user.msisdn;
			v.token = m.user.token;
			if(pkgOb.hasOwnProperty('family')) {
				trace("Pkg Type : " + pkgOb.family);
				v.paymentLevel = pkgOb.family;
			}
			v.packageCode = pkgOb.code || pkgOb.item_code;
			v.packageName = normalizedPkgName;
			v.paymentChannel = paymentChannel;
			v.paymentMethod = topup;
			v.totalAmount = amount;
			v.userName = m.CIMBName;
			v.userEmail = m.CIMBEMail;
//			v.transactionDate = getCustomDateFormat(new Date(),true);
			v.redirectForward = "1";
			//strace(v.toString())
			return JSON.stringify(v);
		}
		
		public static function stopPackage(id:String):void {
			var dataOb:Object = new Object();
			dataOb.username = API_USERNAME;
			dataOb.password = API_PASSWORD;
			dataOb.msisdn = m.user.msisdn;
			dataOb.token = m.user.token;
			dataOb.lang = m.user.lang;
			dataOb.pkgid = id;
			
			var data:String = JSON.stringify(dataOb);
			var url:URLRequest = new URLRequest(API_HOST+SERVICE_STOP_PACKAGE);
			var v:URLVariables = new URLVariables();
			v.data = data;
			url.data = v;
			url.method = URLRequestMethod.POST;
			
			var loader:URLLoader = new URLLoader(url);
			var timeStart:int = getTimer();
			loader.addEventListener(Event.COMPLETE, function onResult(e:Event):void {
				loader.removeEventListener(Event.COMPLETE, onResult);
				
				var data:Object;
				try {
					trace(e.target.data);
					data = JSON.parse(e.target.data);
				} catch(e:Error) { //server error
					data = {code:'666'}
				}
				
				m.dispatchEvent(new BimaEvent(BimaEvent.STOP_PACKAGE_CALLBACK,data));
			});
			loader.addEventListener(IOErrorEvent.IO_ERROR, function onIOError(e:IOErrorEvent):void {
				loader.removeEventListener(IOErrorEvent.IO_ERROR, onIOError);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
			});
		}
		
		
	}
}