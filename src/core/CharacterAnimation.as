package core
{
	
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	import mx.collections.ArrayCollection;
	import mx.core.UIComponent;
	import mx.events.EffectEvent;
	
	import spark.effects.Move;
	import spark.effects.animation.RepeatBehavior;
	
	public class CharacterAnimation extends EventDispatcher
	{
		
		private var m:Manager = Manager.getInstance();
		
		public var SPEED_MUL:Number = 1;
		
		private var mc:MovieClip;
		
		public function CharacterAnimation(mc:MovieClip):void {
			this.mc = mc;
		}
		
		private var animData:Object;
		private var loop:Boolean;
		private var animFrames:uint;
		public var animLabel:String;
		public function play(lbl:String,loop:Boolean=false):void {
			var isBimaOnNotifPos:Boolean = m.avatar.con.x==m.avatar.notifPos.x;
			if(isBimaOnNotifPos) {
				if(lbl!='fly') return;
			}
			mc.removeEventListener(Event.ENTER_FRAME, frameHandler);
			this.loop = loop;
			this.animLabel = lbl;
			if(isAnimationExist(lbl)) {
				animData = getAnimData(lbl);
				animFrames = animData.endFrame - animData.startFrame;
				df = 1;//int(animFrames/(animFrames*SPEED_MUL));
				if(animData) {
//					if(mc.hasEventListener(Event.ENTER_FRAME)) {
//						mc.removeEventListener(Event.ENTER_FRAME, frameHandler);
//					}
//					mc.gotoAndStop(animData.startFrame);
					mc.gotoAndStop(int(animData.startFrame));// + Math.random()*(Math.abs(animData.endFrame-animData.startFrame))));
					MovieClip(mc.getChildAt(0)).gotoAndStop(1);//
					mc.addEventListener(Event.ENTER_FRAME, frameHandler);
				}
			}
		}
		
		public function playAndLoop(lbl:String):void {
			var isBimaOnNotifPos:Boolean = m.avatar.con.x==m.avatar.notifPos.x;
			if(isBimaOnNotifPos) {
				if(lbl!='fly') return;
			}
			mc.removeEventListener(Event.ENTER_FRAME, frameHandler);
			this.animLabel = lbl;
			this.loop = false;
			if(isAnimationExist(lbl)) {
				animData = getAnimData(lbl);
				animFrames = animData.endFrame - animData.startFrame;
				df = 1;
				if(animData) {
					mc.gotoAndStop(int(animData.startFrame));
					MovieClip(mc.getChildAt(0)).gotoAndStop(1);
					addEventListener(BimaEvent.ANIMATION_END, function onAnimEnd(e:BimaEvent):void {
						removeEventListener(BimaEvent.ANIMATION_END, onAnimEnd);
						var loopLabel:String = String(e.params) + 'loop';
						play(loopLabel,true);
					});
					mc.addEventListener(Event.ENTER_FRAME, frameHandler);
				}
			}
		}
		
		public function stop(lbl:String):void {
			var isBimaOnNotifPos:Boolean = m.avatar.con.x==m.avatar.notifPos.x;
			if(isBimaOnNotifPos) {
				if(lbl!='fly') return;
			}
			mc.removeEventListener(Event.ENTER_FRAME, frameHandler);
			this.animLabel = lbl;
			this.loop = false;
			if(isAnimationExist(lbl)) {
				animData = getAnimData(lbl);
				animFrames = animData.endFrame - animData.startFrame;
				df = 1;
				if(animData) {
					mc.gotoAndStop(int(animData.startFrame));
					MovieClip(mc.getChildAt(0)).gotoAndStop(1);
				}
			}
		}
		
		private var df:int;//delta frame
		private var nf:int;//next frame
		private function frameHandler(event:Event):void{
			if(mc.currentFrame==animData.endFrame) {
				if(loop) {
					mc.gotoAndStop(animData.startFrame);
					MovieClip(mc.getChildAt(0)).gotoAndStop(1);//
				} else {
					mc.removeEventListener(Event.ENTER_FRAME, frameHandler);
					MovieClip(mc.getChildAt(0)).stop();//
					//trace(MovieClip(mc.getChildAt(0)).currentFrame);
					//Manager.getInstance().cannotInteruptAvatar = false;
					this.dispatchEvent(new BimaEvent(BimaEvent.ANIMATION_END,animLabel));
				}
			} else {
				nf = mc.currentFrame + 1;//df;
//				if(nf > animData.endFrame) {
//					mc.gotoAndStop(animData.endFrame);
//					MovieClip(mc.getChildAt(0)).gotoAndStop(MovieClip(mc.getChildAt(0)).totalFrames);//
//				} else {
					//mc.gotoAndStop(nf);
					mc.nextFrame();
					MovieClip(mc.getChildAt(0)).nextFrame();//
					//trace(MovieClip(mc.getChildAt(0)).currentFrame);
//				}
			}
		}
		
		public function isAnimationExist(lbl:String):Boolean {
			for(var i:int=0;i<mc.currentLabels.length;i++) {
				if(mc.currentLabels[i].name==lbl) {
					return true;
				}
			}
			return false;
		}
		
		private function getAnimData(lbl:String):Object {
			var ob:Object;
			for(var i:int=0;i<mc.currentLabels.length;i++) {
				if(mc.currentLabels[i].name==lbl) {
					ob = new Object();
					ob.name = lbl;
					ob.startFrame = mc.currentLabels[i].frame;
					var isLastAnim:Boolean = i==mc.currentLabels.length-1;
					if(isLastAnim) {
						ob.endFrame = mc.totalFrames;
					} else {
						ob.endFrame = mc.currentLabels[i+1].frame - 1;
					}
					break;
				}
			}
			return ob;
		}
		
	}
}
