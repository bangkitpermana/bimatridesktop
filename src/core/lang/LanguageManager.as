package core.lang {
	
	import core.Manager;
	
	import flash.events.Event;
	import flash.events.EventDispatcher;
	
	public class LanguageManager extends EventDispatcher {
		
		private static var instance:LanguageManager = new LanguageManager();
		
		public static const LANGUAGE_CHANGED:String = "languageChanged";
		
		private var langController:LanguageController;
		private var langData:LanguageData;
		
		[Bindable] public var langObj:Object; //languages object
		private var defaultLanguage:String = "en";
		[Bindable] public var selectedLanguage:String;
		
		private var _sl:String = "en";
		[Bindable]
		public function set sl(value:String):void {
			_sl = value;
			dispatchEvent(new Event("languageChanged"));
		}
		public function get sl():String {
			return _sl;
		}
		
		public function LanguageManager():void {
			if(instance) {
				throw new Error("It is a Singleton and can only be accessed through Singleton.getInstance()");
			}
		}
		
		public static function getInstance():LanguageManager {
			return instance;
		}
		
		//==========================================================================================================================
		
		public function init(langData:LanguageData):void {
			this.langData = langData;
			this.langObj = LanguageXMLtoObject.convert(this.langData.languagesXml);
			this.defaultLanguage = this.langData.defaultLanguage;
			this.selectedLanguage = this.defaultLanguage;
			
			langController = new LanguageController();
			langController.init(this.langObj.availableLanguages);
		}
		
		[Bindable(event="languageChanged")]
		public function getLangStr(id:int):String {
			if(this.langObj.data[id]) {
				//Manager.getInstance().app.logger.addLog(2,"\n\n\ngetLangStr=" + id + ' ' + sl);
				if(sl==''||sl==null) sl = 'en';
				if(sl!='en') {
					if(sl!='id') sl = 'id';
				}
				var str:String = String(this.langObj.data[id][sl]).split('\\n').join('\n');
				return str;
			} else {
				throw new Error("Data with provided id not defined!");
				return null;
			}
		}
		
		public function setLangStr(id:int, str2Insert:String):Boolean {
			if(this.langObj.data[id]) {
				//Manager.getInstance().app.logger.addLog(2,"\n\n\ngetLangStr=" + id + ' ' + sl);
				if(sl==''||sl==null) sl = 'en';
				if(sl!='en') {
					if(sl!='id') sl = 'id';
				}
				var str:String = String(this.langObj.data[id][sl]).split('\\n').join('\n');
				this.langObj.data[id][sl] = str2Insert; 
				return true;
			} else {
				throw new Error("Data with provided id not defined!");
				return false;
			}
		}
		
	}
}