package core.lang {
	
	public class BimaLanguageData extends LanguageData {
		
		private var bimaLanguagesXML:XML =
			<Languages>
			
			
			//menu bar
				<data id='0'>
					<id>Profil</id>
					<en>Profile</en>
				</data>
				<data id='1'>
					<id>Paket</id>
					<en>Package</en>
				</data>
				<data id='2'>
					<id>Tips</id>
					<en>Tips</en>
				</data>
				<data id='3'>
					<id>Kontak</id>
					<en>Contact</en>
				</data>
				<data id='4'>
					<id>Logout</id>
					<en>Logout</en>
				</data>
				<data id='5'>
					<id>Pengaturan</id>
					<en>Setting</en>
				</data>
				<data id='6'>
					<id>Anda yakin ingin logout?</id>
					<en>Are you sure want to logout?</en>
				</data>
				<data id='7'>
					<id>Memuat</id>
					<en>Loading</en>
				</data>
				<data id='8'>
					<id>Isi Ulang</id>
					<en>Reload</en>
				</data>
				<data id='9'>
					<id>Tagihan</id>
					<en>Bill</en>
				</data>
				<data id='91'>
					<id>Indie+</id>
					<en>Indie+</en>
				</data>
			
			
			//setting window
				<data id='10'>
					<id>Bahasa</id>
					<en>Language</en>
				</data>
				<data id='11'>
					<id>Indonesia</id>
					<en>Indonesian</en>
				</data>
				<data id='12'>
					<id>Inggris</id>
					<en>English</en>
				</data>
				<data id='13'>
					<id>Notifikasi Batas Data</id>
					<en>Notification Data Treshold</en>
				</data>
				<data id='14'>
					<id>Simpan</id>
					<en>Save</en>
				</data>
				<data id='15'>
					<id>Apakah kamu ingin menyimpan pengaturan?</id>
					<en>Do you want to save the settings?</en>
				</data>
				<data id='16'>
					<id>Ya</id>
					<en>Yes</en>
				</data>
				<data id='17'>
					<id>Tidak</id>
					<en>No</en>
				</data>
			
				
			//register window
				<data id='20'>
					<id>no. hp</id>
					<en>mobile no.</en>
				</data>
				<data id='21'>
					<id>kata kunci</id>
					<en>password</en>
				</data>
				<data id='22'>
					<id>Masuk</id>
					<en>Login</en>
				</data>
			
			
			//contact window
				<data id='30'>
					<id>Hubungi Kami</id>
					<en>Contact Us</en>
				</data>
				<data id='31'>
					<id>Kami siap untuk membantu Anda.\nSilahkan hubungi kami bila Anda membutuhkan.\nKami siap melayani via telepon dengan cepat dan mudah.\n\nHubungi 3Agent kami di 3Store atau ke nomor-nomor dibawah ini:</id>
					<en>We are ready to assist you.\nPlease contact us.We are ready to assist via phone.\n\nContact our 3Agents at 3Store or to the numbers below:</en>
				</data>
				<data id='32'>
					<id>Hubungi</id>
					<en>Contact.</en>
				</data>
			
			
			//tips window
				<data id='40'>
					<id>Tips.</id>
					<en>Tips.</en>
				</data>
			
			
			//profile menus
				<data id='50'>
					<id>Sisa Pulsa</id>
					<en>Remaining Balance</en>
				</data>
				<data id='51'>
					<id>Status</id>
					<en>Status</en>
				</data>
				<data id='52'>
					<id>BlackBerry</id>
					<en>BlackBerry</en>
				</data>
				<data id='53'>
					<id>Kuota Data</id>
					<en>Quota Data</en>
				</data>
				<data id='54'>
					<id>Kuota Data Malam</id>
					<en>Quota Data Night</en>
				</data>
				<data id='541'>
					<id>Kuota Data IMEI</id>
					<en>Quota Data IMEI</en>
				</data>
				<data id='55'>
					<id>Akses Gratis</id>
					<en>Free Access</en>
				</data>
				<data id='56'>
					<id>Nomor Tri Kamu</id>
					<en>Your Tri Number</en>
				</data>
				<data id='57'>
					<id>Notifikasi</id>
					<en>Notification</en>
				</data>
				<data id='571'>
					<id>Email</id>
					<en>Email</en>
				</data>
				<data id='58'>
					<id>Promo</id>
					<en>Promo</en>
				</data>
				<data id='59'>
					<id>Kuota SMS Off Net</id>
					<en>SMS Quota Off Net</en>
				</data>
				<data id='60'>
					<id>Kuota SMS On Net</id>
					<en>SMS Quota On Net</en>
				</data>
				<data id='61'>
					<id>Masa Berlaku</id>
					<en>Validity</en>
				</data>
				<data id='62'>
					<id>Kuota Telepon Off Net</id>
					<en>Voice Quota Off Net</en>
				</data>
				<data id='63'>
					<id>Kuota Telepon On Net</id>
					<en>Voice Quota On Net</en>
				</data>
				<data id='64'>
					<id>TRIms</id>
					<en>TRIms</en>
				</data>
				<data id='65'>
					<id>Sisa Kredit</id>
					<en>Remaining Credit</en>
				</data>
				<data id='652'>
					<id>Sisa Credit Limit</id>
					<en>Remaining Credit Limit</en>
				</data>
				<data id='651'>
					<id>Sisa Kantong Kredit</id>
					<en>Remaining Credit Balance</en>
				</data>
				<data id='66'>
					<id>Tanggal Jatuh Tempo</id>
					<en>Payment Due Date</en>
				</data>
				<data id='67'>
					<id>Kredit</id>
					<en>Credit</en>
				</data>
				<data id='68'>
					<id>Tagihan</id>
					<en>Bill</en>
				</data>
				<data id='69'>
					<id>Tanggal Jatuh Tempo</id>
					<en>Payment Due Date</en>
				</data>
			
			
			//package carousel menus
				<data id='70'>
					<id>Paket</id>
					<en>Packages</en>
				</data>
				<data id='71'>
					<id>Paket Spesial</id>
					<en>Special Package</en>
				</data>
				<data id='72'>
					<id>Pay As You Go</id>
					<en>Pay As You Go</en>
				</data>
				<data id='73'>
					<id>Isi Ulang</id>
					<en>Reload</en>
				</data>
				<data id='74'>
					<id>Nomor</id>
					<en>Number</en>
				</data>
				<data id='75'>
					<id>Pembayaran Tagihan</id>
					<en>Bill Payment</en>
				</data>
				<data id='76'>
					<id>Ubah Call Plan</id>
					<en>Call Plan Changes</en>
				</data>
				<data id='77'>
					<id>Ubah Credit Limit</id>
					<en>Credit Limit Changes</en>
				</data>
				<data id='78'>
					<id>Pembayaran</id>
					<en>Payment</en>
				</data>
			
			
			//pay methods 80 - 99
				<data id='80'>
					<id>Pulsa</id>
					<en>Balance</en>
				</data>
				<data id='81'>
					<id>Kantong Kredit</id>
					<en>Credit Balance</en>
				</data>
			
			
			//package window 100 - 119
				<data id='100'>
					<id>Paket</id>
					<en>Package</en>
				</data>
				<data id='101'>
					<id>Nama Paket</id>
					<en>Package Name</en>
				</data>
				<data id='102'>
					<id>Deskripsi Paket</id>
					<en>Package Description</en>
				</data>
				<data id='103'>
					<id>Kuota</id>
					<en>Quota</en>
				</data>
				<data id='104'>
					<id>Masa Aktif</id>
					<en>Active Period</en>
				</data>
				<data id='105'>
					<id>Harga</id>
					<en>Price</en>
				</data>
				<data id='106'>
					<id>Beli</id>
					<en>Buy</en>
				</data>
				<data id='107'>
					<id>Konfirmasi</id>
					<en>Confirmation</en>
				</data>
				<data id='108'>
					<id>Kamu akan melakukan pembelian</id>
					<en>You will purchase</en>
				</data>
				<data id='109'>
					<id>Melalui</id>
					<en>By</en>
				</data>
				<data id='110'>
					<id>Konfirm</id>
					<en>Confirm</en>
				</data>
				<data id='111'>
					<id>Permintaan kamu sedang di proses</id>
					<en>Your request is being processed</en>
				</data>
				<data id='112'>
					<id>Permintaan kamu sedang diproses</id>
					<en>Your request is being processed</en>
				</data>
				<data id='113'>
					<id>Ok</id>
					<en>Ok</en>
				</data>
				<data id='114'>
					<id>Paket Spesial</id>
					<en>Special Package</en>
				</data>
				<data id='115'>
					<id>Metoda Pembayaran</id>
					<en>Payment Methods</en>
				</data>
				<data id='116'>
					<id>Kita ada paket spesial buat kamu. Ini penawarannya, pemakaian 30 hari lalu kamu:</id>
					<en>We have special package for you. Here's the deal, your last 30 days usage is:</en>
				</data>
				<data id='117'>
					<id>Klik Beli untuk melanjutkan</id>
					<en>Click Buy to continue</en>
				</data>
				<data id='118'>
					<id>Call Plan Aktif</id>
					<en>Current Call Plan</en>
				</data>
				<data id='119'>
					<id>Deskripsi</id>
					<en>Description</en>
				</data>
				<data id='1191'>
					<id>Ubah ke</id>
					<en>Change to</en>
				</data>
				<data id='11911'>
					<id>Deskripsi</id>
					<en>Description</en>
				</data>
				<data id='1192'>
					<id>Konfirm</id>
					<en>Confirm</en>
				</data>
				<data id='1193'>
					<id>Cancel</id>
					<en>Cancel</en>
				</data>
				<data id='1194'>
					<id>Kredit Kamu</id>
					<en>Credit Limit</en>
				</data>
				<data id='1195'>
					<id>Ubah</id>
					<en>Change</en>
				</data>
				<data id='1196'>
					<id>Periode</id>
					<en>Period</en>
				</data>
				<data id='1197'>
					<id>Jumlah</id>
					<en>Amount</en>
				</data>
				<data id='11971'>
					<id>Tagihan</id>
					<en>Bill</en>
				</data>
				<data id='11972'>
					<id>Jumlah</id>
					<en>Amount</en>
				</data>
				<data id='11973'>
					<id>Metode Pembayaran</id>
					<en>Payment Method</en>
				</data>
				<data id='1198'>
					<id>Jumlah hari</id>
					<en>Number of Days</en>
				</data>
				<data id='1199'>
					<id>Alasan</id>
					<en>Reason</en>
				</data>
				<data id='1200'>
					<id>Bulan</id>
					<en>Month</en>
				</data>
				<data id='1201'>
					<id>Bulan Ini</id>
					<en>Running Month</en>
				</data>
				<data id='1202'>
					<id>Jumlah(Rp)</id>
					<en>Amount(IDR)</en>
				</data>
				<data id='1203'>
					<id>Rp.</id>
					<en>IDR</en>
				</data>
				<data id='12041'>
					<id>Kredit kamu akan dirubah</id> 
					<en>Your credit will be changed</en> 
				</data>
				<data id='12042'>
					<id>Dari</id> 
					<en>From</en> 
				</data>
				<data id='12043'>
					<id>Limit Sebelumnya</id> 
					<en>Previous Limit</en> 
				</data>
				<data id='12044'>
					<id>Ke</id> 
					<en>To</en> 
				</data>
				<data id='12045'>
					<id>Limit Baru</id> 
					<en>New Limit</en> 
				</data>
				<data id='12046'>
					<id>Jumlah Hari</id> 
					<en>Number of Days</en> 
				</data>
				<data id='120461'>
					<id>hari</id> 
					<en>days</en> 
				</data>
				<data id='12047'>
					<id>Alasan</id> 
					<en>Reason</en> 
				</data>
				<data id='12048'>
					<id>Apakah anda mau memperbarui paket secara otomatis?</id> 
					<en>Do you want to Auto Renewal the Package?</en> 
				</data>
				<data id='12049'>
					<id>Hentikan</id> 
					<en>Stop</en> 
				</data>
				<data id='12050'>
					<id>Apakah anda mau menonaktifkan</id> 
					<en>Do you want to stop activate</en> 
				</data>
				
			
			//intro window 120 - 129
				<data id='120'>
					<id>Halo.</id>
					<en>Hallo.</en>
				</data>
				<data id='121'>
					<id>Selamat datang di 3Care. Silahkan login untuk melihat detail kartu Tri kamu sekarang.</id>
					<en>Welcome to 3Care. Please login now to see your Tri card details.</en>
				</data>
				<data id='122'>
					<id>Login diperlukan karena aplikasi tidak menggunakan koneksi internet Tri.\n\nJika kamu tidak memiliki user id, silahkan daftar di</id>
					<en>Login is required because you're not on Tri's network.\n\nIf you don't have user id, please create at</en>
				</data>
				<data id='123'>
					<id>Saat ini layanan internet tidak tersedia. Cobalah beberapa saat lagi.</id>
					<en>Currently there's no sufficient internet connection. Please try again later.</en>
				</data>
				<data id='124'>
					<id>Maaf, saat ini sistem sedang dalam perbaikan. Cobalah beberapa saat lagi.</id>
					<en>Sorry, currently system is under maintenance. Please try again later</en>
				</data>
				<data id='125'>
					<id>Lupa password?</id>
					<en>Forgot password?</en>
				</data>
				<data id='126'>
					<id>Belum punya ID Tri?</id>
					<en>Doesn't have Tri ID?</en>
				</data>
				<data id='127'>
					<id>Nomor Tri Kamu</id>
					<en>Your Tri Number</en>
				</data>
				<data id='128'>
					<id>Password</id>
					<en>Password</en>
				</data>
				<data id='129'>
					<id>Nomor Tri atau Email.</id>
					<en>Tri Number or Email.</en>
				</data>
				<data id='1291'>
					<id>Nama</id>
					<en>Name</en>
				</data>
				<data id='1292'>
					<id>Email</id>
					<en>Email</en>
				</data>
				<data id='1293'>
					<id>Data Pelanggan</id>
					<en>Customer Data</en>
				</data>
				<data id='1294'>
					<id>Nomor HP</id>
					<en>Mobile Number</en>
				</data>
				<data id='1295'>
					<id>Format email salah</id>
					<en>Incorrect email format</en>
				</data>
			
			
			//reload window 130 - 139
				<data id='130'>
					<id>Isi Ulang</id>
					<en>Reload</en>
				</data>
				<data id='131'>
					<id>Masukan kode voucher</id>
					<en>Enter the voucher code</en>
				</data>
				<data id='132'>
					<id>Isi Ulang</id>
					<en>Reload</en>
				</data>
				<data id='133'>
					<id>Terimakasih</id>
					<en>Thanks</en>
				</data>
				<data id='134'>
					<id>Anda akan segera mendapat notifikasi isi ulang.</id>
					<en>You will receive reload notification soon.</en>
				</data>
				<data id='135'>
					<id>Kode voucher harus 16 digit</id>
					<en>Voucher code must be 16 digits length</en>
				</data>
				<data id='136'>
					<id>Pilih dan Beli Pulsa</id>
					<en>Pick and Buy Credit</en>
				</data>
				<data id='137'>
					<id>Pilih dan Beli Kredit</id>
					<en>Pick and Buy Credit</en>
				</data>
			
			
			//general menu 140 - 159
				<data id='140'>
					<id>kembali</id>
					<en>back</en>
				</data>
				<data id='141'>
					<id>Detail</id>
					<en>Details</en>
				</data>
				<data id='1411'>
					<id>Ringkasan Isi Ulang</id>
					<en>Top Up Summary</en>
				</data>
				<data id='1412'>
					<id>Ringkasan Pemakaian Kantong Kredit</id>
					<en>Credit Line Usage Summary</en>
				</data>
				<data id='142'>
					<id>Ke Aplikasi</id>
					<en>Go To App</en>
				</data>
				<data id='143'>
					<id>Tutup</id>
					<en>Dismiss</en>
				</data>
				<data id='144'>
					<id>Apakah kamu yakin untuk keluar?</id>
					<en>Are you sure want to quit?</en>
				</data>
				<data id='145'>
					<id>Ringkasan Tagihan</id>
					<en>Bill Summary</en>
				</data>
				<data id='1450'>
					<id>Halaman Tagihan untuk saat ini belum tersedia. Terima kasih.</id>
					<en>Currently this page is not available. Thank you.</en>
				</data>
				<data id='14501'>
					<id>Ke Pembayaran</id> 
					<en>To Payment Page</en> 
				</data>
				<data id='14502'>
					<id>Kirim ke Email</id> 
					<en>Send to Email</en> 
				</data>
				<data id='14503'>
					<id>Tagihan kamu saat ini belum tersedia. Terima kasih.</id>
					<en>Your bill is not available at the moment. Thank you.</en>
				</data>
				<data id='1451'>
					<id>Tagihan Terakhir</id>
					<en>Last Bill</en>
				</data>
				<data id='1452'>
					<id>Tagihan Sekarang</id>
					<en>Current Bill</en>
				</data>
				<data id='1453'>
					<id>Jumlah Tagihan</id>
					<en>Total Bill</en>
				</data>
				<data id='146'>
					<id>Bayar</id>
					<en>Pay</en>
				</data>
				<data id='147'>
					<id>Bayar</id>
					<en>Pay Now</en>
				</data>
				<data id='148'>
					<id>Bayar</id>
					<en>Pay All</en>
				</data>
			
				<data id='149'>
					<id>Pembayaran minimal Rp. </id>
					<en>Minimum Payment is IDR. </en>
				</data>
			
				<data id='150'>
					<id>  dan maksimal Rp. </id>
					<en>  and Maximum IDR. </en>
				</data>
				
				<data id='1491'>
					<id>Maaf, pembayaran hanya bisa dilakukan untuk tagihan sejumlah minimal Rp. </id>
					<en>Payment can only be done with minimum bill at IDR. </en>
				</data>
				
				<data id='1501'>
					<id>  . Terima Kasih. </id>
					<en>  . Thank You.</en>
				</data>
				
				<data id='1492'>
					<id>Maaf, pembayaran tidak bisa melebihi jumlah tertagih. Terima kasih.</id>
					<en>Sorry, bill cannot be paid exceed the total amount. Thank you.</en>
				</data>
				
				<data id='151'>
					<id>Your email address</id>
					<en>Your email address</en>
				</data>
				<data id='152'>
					<id>Email to me</id>
					<en>Email to me</en>
				</data>
			
						
			//month names 160 - 179
				<data id='160'>
					<id>Januari</id>
					<en>January</en>
				</data>
				<data id='161'>
					<id>Febuari</id>
					<en>February</en>
				</data>
				<data id='162'>
					<id>Maret</id>
					<en>March</en>
				</data>
				<data id='163'>
					<id>April</id>
					<en>April</en>
				</data>
				<data id='164'>
					<id>Mei</id>
					<en>May</en>
				</data>
				<data id='165'>
					<id>Juni</id>
					<en>June</en>
				</data>
				<data id='166'>
					<id>Juli</id>
					<en>July</en>
				</data>
				<data id='167'>
					<id>Agustus</id>
					<en>August</en>
				</data>
				<data id='168'>
					<id>September</id>
					<en>September</en>
				</data>
				<data id='169'>
					<id>Oktober</id>
					<en>October</en>
				</data>
				<data id='170'>
					<id>November</id>
					<en>November</en>
				</data>
				<data id='171'>
					<id>Desember</id>
					<en>December</en>
				</data>
			
			
			//Email things
				<data id='180'>
					<id>Register</id>
					<en>Register</en>
				</data>
				<data id='181'>
					<id>Update</id>
					<en>Update</en>
				</data>
				<data id='182'>
					<id>Note: By Clicking “Register/Update” to subscribe/update email, you are agree to receive email subscription every month</id>
					<en>Note: By Clicking “Register/Update” to subscribe/update email, you are agree to receive email subscription every month</en>
				</data>
				<data id='183'>
					<id>Alamat email tidak valid!</id>
					<en>Invalid email!</en>
				</data>
				<data id='184'>
					<id>Subscribe</id>
					<en>Subscribe</en>
				</data>
				<data id='185'>
					<id>Unsubscribe</id>
					<en>Unsubscribe</en>
				</data>
				<data id='186'>
					<id>Lanjut</id>
					<en>Continue</en>
				</data>
				<data id='187'>
					<id>Ubah</id>
					<en>Update</en>
				</data>
				<data id='188'>
					<id>Mail</id>
					<en>Mail</en>
				</data>
				<data id='189'>
					<id>You are about to update your email address. Changes can be made one time only per-bill cycle, and will be applied in the next billing period</id>
					<en>Mail</en>
				</data>
				<data id='211'>
					<id>Tambah</id>
					<en>Add</en>
				</data>
				<data id='212'>
					<id>Hapus</id>
					<en>Delete</en>
				</data>
				<data id='213'>
					<id>Masukkan nomor yang akan didaftarkan di Kuota Rame2x :</id>
					<en>Input a number to be registered in Kuota Rame2x :</en>
				</data>
				<data id='214'>
					<id>Apa kamu mau menambahkan</id>
					<en>Do you want to add</en>
				</data>
				<data id='215'>
					<id>sebagai anggota kamu?</id>
					<en>as your member? </en>
				</data>
			
			
			
			</Languages>;
		
		private var bimaDefaultLanguage:String = "en";
		
		public function BimaLanguageData():void {
			this.languagesXml = bimaLanguagesXML;
			this.defaultLanguage = bimaDefaultLanguage;
			super();
		}
		
	}
}