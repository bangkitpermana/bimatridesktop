package core
{
	
	import air.net.ServiceMonitor;
	import air.net.URLMonitor;
	import air.update.ApplicationUpdaterUI;
	import air.update.events.DownloadErrorEvent;
	import air.update.events.StatusFileUpdateErrorEvent;
	import air.update.events.StatusUpdateErrorEvent;
	import air.update.events.UpdateEvent;
	
	import com.stevewebster.Base64;
	
	import components.AlertDialog;
	import components.AppGroup;
	import components.Logger;
	import components.MainWindow;
	import components.Notification;
	import components.PackageContent;
	
	import core.data.ProfileMenu;
	import core.lang.BimaLanguageData;
	import core.lang.LanguageManager;
	
	import flash.desktop.DockIcon;
	import flash.desktop.NativeApplication;
	import flash.desktop.NotificationType;
	import flash.desktop.SystemTrayIcon;
	import flash.display.Bitmap;
	import flash.display.DisplayObject;
	import flash.display.NativeMenu;
	import flash.display.NativeMenuItem;
	import flash.display.NativeWindow;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.EventDispatcher;
	import flash.events.InvokeEvent;
	import flash.events.MouseEvent;
	import flash.events.StatusEvent;
	import flash.events.TimerEvent;
	import flash.filesystem.File;
	import flash.filters.ColorMatrixFilter;
	import flash.geom.PerspectiveProjection;
	import flash.geom.Point;
	import flash.net.NetworkInterface;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.profiler.showRedrawRegions;
	import flash.system.Capabilities;
	import flash.ui.Mouse;
	import flash.ui.MouseCursor;
	import flash.utils.Timer;
	
	import mx.collections.ArrayCollection;
	import mx.collections.ArrayList;
	import mx.containers.utilityClasses.PostScaleAdapter;
	import mx.formatters.DateFormatter;
	import mx.managers.PopUpManager;
	import mx.managers.SystemManager;
	import mx.utils.ObjectProxy;
	
	import org.casalib.util.DateUtil;
	
	import spark.collections.Sort;
	import spark.collections.SortField;
	import spark.core.ISharedDisplayObject;
	
	public class Manager extends EventDispatcher
	{
		
		private static var instance:Manager = new Manager();
		
		public var processToWatch:String;
		public var processToLog:String;
		
		public function Manager():void {
			if(instance) {
				throw new Error("It is a Singleton and can only be accessed through Singleton.getInstance()");
			}
			lm = LanguageManager.getInstance();
			lm.init(new BimaLanguageData());
			addEventListener("languageChanged", function onLanguageChanged(e:Event):void {
				trace("Ganti Bahasa");
				lm.sl = user.lang;
				//1.packet menu
				//				getSpecialPackage();
				
				//2.pay methods
				//				initPayMethods();
			});
		}
		
		public static function getInstance():Manager {
			return instance;
		}
		
		///////////////////////////////////////////////////////////////////////////////////////////////
		
		[Bindable]
		[Embed(source="assets/bima.swf",symbol="WinkLoop")]
		public var BimaNotifClass:Class;
		
		[Embed(source="assets/icons/icon16.png")]
		public var IconDock16Class:Class;
		
		[Embed(source="assets/icons/icon72.png")]
		public var IconDock72Class:Class;
		
		[Embed(source="assets/icons/icon128.png")]
		public var IconDock128Class:Class;
		
		[Bindable]
		[Embed(source="assets/images/three.png")]
		public var DefaultPackageImageClass:Class;
		
		[Embed(source="assets/bima.swf",symbol="LoadingCircle")]
		public var LoadingCircleClass:Class;
		
		///////////////////////////////////////////////////////////////////////////////////////////////
		
		[Bindable] public var lm:LanguageManager;
		
		public var appGroup:AppGroup;
		[Bindable] public var app:NativeWindow;
		public var appWindow:MainWindow;
		[Bindable] public var notification:Notification;
		public var appName:String;
		public var appVersion:String;
		public var screenRes:Point;
		public var isOnWindows:Boolean;
		public var isFromMain:Boolean = false;
		//add by bangkit
		public var ad:Boolean = false;
		[Bindable]  public var reponsInit1:Object;
		[Bindable]  public var btnBillValue:String;
		[Bindable]  public var btnBillInfo:String;
		
		public var tokenDokucc:String;  
		public var macAddress:String;
		public var billingPaymentMenu:Object;
		[Bindable] public var reloadPageReady:Boolean = false;
		public var typeOkBtn:String;
		public var disclosureGroupes:Boolean = false;
		[Bindable] public var CIMBEMail:String;
		[Bindable] public var CIMBName:String;
		[Bindable] public var tkcCode:String;
		[Bindable] public var tkcType:String;
		[Bindable] public var windowsTaskBarHeight:uint;
		[Bindable] public var windowsTaskBarY:int;
		[Bindable] public var TRIms:Boolean = false;
		[Bindable] public var TopUp:Boolean = false;
		[Bindable] public var isBill:Boolean = false;
		[Bindable] public var isShareQuotaBill:Boolean = false; // for share quota
		[Bindable] public var isPostPaid:Boolean = false;
		[Bindable] public var isHybrid:Boolean = false;
		[Bindable] public var isReload:Boolean = false;// add by bangkit for seleksi reload ga pake pulsa
		
		[Bindable] public var isTypeBillAvailable:Boolean = false;// hybrid only
		[Bindable] public var isIndieExecutive:Boolean = false;
		[Bindable] public var typeBills:ArrayCollection;
		[Bindable] public var selectedHybridData:Object;
		
		
		public var isOnMac:Boolean;
		public var currentDate:String;
		
		[Bindable] public var payMethods:ArrayCollection;
		
		[Bindable] public var packageReady:Boolean = false;
		
		private var _isProcessing:Boolean;
		[Bindable] public var isProcessing:Boolean;
		/*		[Bindable(event="businessChanged")]
		public function get isProcessing():Boolean{return _isProcessing;} 
		public function set isProcessing(proc:Boolean):void
		{
		_isProcessing = proc;
		if(_isProcessing) avatar.showLoadingCircle(null);
		else avatar.hideLoadingCircle();
		}*/ 
		[Bindable] public var buySucceedMsg:String;
		public var callplanconfirmtext:String;
		[Bindable] public var leftLabel:String;
		[Bindable] public var rightLabel:String;
		[Bindable] public var leftUrl:String;
		[Bindable] public var rightUrl:String;
		
		[Bindable] public var isShowingPromo:Boolean = false;
		//Hack!! request Package refresh after buying something from promo;
		[Bindable] public var requestPackagefresh:Boolean = false;
		
		[Bindable] public var isBillingAvailable:Boolean = false;
		[Bindable] public var isLoadingDoku:Boolean;
		//new promo
		public var responNewPromo:Object;
		
		
		[Bindable] public var videos:ArrayCollection;
		
		[Bindable] public var contactInfo:String;
		[Bindable] public var contactHeader:String;
		
		[Bindable] public var autologinSuccess:Boolean;
		public var _isLoggedIn:Boolean;
		public function set isLoggedIn(value:Boolean):void {
			_isLoggedIn = value;
			if(value) notification.hide();
		}
		[Bindable]
		public function get isLoggedIn():Boolean {
			return _isLoggedIn;
		}
		
		[Bindable] public var user:User;
		
		public var avatar:BimaAvatar;
		[Bindable] public var cannotInteruptAvatar:Boolean;
		[Bindable] public var isAvatarShowingWindow:Boolean;
		public var lastAnimLabelB4Notif:String;
		
		public var appPackageButtonClicked:Boolean;
		
		public function initUser():void {
			user = new User();
			user.addEventListener(BimaEvent.USER_PREFERENCES_LOADED, function onLoaded(e:BimaEvent):void {
				user.removeEventListener(BimaEvent.USER_PREFERENCES_LOADED, onLoaded);
				
				lm.selectedLanguage = lm.sl = user.lang;
			});
			user.loadUserPreferences();
			
			screenRes = new Point(Capabilities.screenResolutionX,Capabilities.screenResolutionY);
		}
		
		public var logger:Logger;
		public function init(appGroup:AppGroup,app:NativeWindow,appWindow:MainWindow,notification:Notification,logger:Logger=null):void {
			this.appGroup = appGroup;
			this.app = app;
			this.appWindow = appWindow;
			this.notification = notification;
			this.logger = logger;
			//appWindow.systemManager = new SystemManager();
			//			this.appWindow = new MainWindow();
			//			this.notification = new Notification();
			//			app.stage.addChild(appWindow);
			//			app.stage.addChild(notification);
			//set taskbar icon
			
			setApplicationNameAndVersion();
			checkNewUpdate();
			
			setTaskBarMenu(createFullIconMenu());
			
			addEventListener(BimaEvent.DOCK_APP, function dockAppNow(e:BimaEvent):void {
				dock();
			});
			
			appWindow.init(app);
			//center the appWindow
			appWindow.x = (screenRes.x-appWindow.width)/2;
			appWindow.y = (screenRes.y-appWindow.height)/2;
			
			//draw avatar
			drawBimaAvatar();
			
			//start at login
			if(!flash.system.Capabilities.isDebugger) {
				NativeApplication.nativeApplication.startAtLogin = true;
			}
			
			undock();
		}
		
		public function initPayMethods(pkgOb:Object=null):void {
			var paymentMethod:ArrayCollection;// = new ArrayCollection(paymentMethods as Array);
			if(String(pkgOb.group).toLowerCase()=='pulsa' || String(pkgOb.group).toLowerCase()=='balance' || String(pkgOb.group).toLowerCase()=='reload' || String(pkgOb.group).toLowerCase()=='isi ulang') {
				paymentMethod = new ArrayCollection(pkgOb.pm);
				payMethods = paymentMethod;
				trace("payment pulsa reload");
				isReload = true;
				return; //add by bangkit untuk berhentiin reload payment di halaman isi pulsa
			
			} 
			trace ("#HREF nyaa :" +  pkgOb.href);
			//add for bonus
			if (String(pkgOb.href).toLowerCase()=='#package' || String(pkgOb.href).toLowerCase()=='#reload')
				
			{
				/*
				paymentMethod = new ArrayCollection(pkgOb.item_pm);
				payMethods = paymentMethod; */
				trace("payment packageee");
				
				payMethods = new ArrayCollection();
				payMethods.addItem(new PayMethod('00',lm.getLangStr(80)));
				if(pkgOb.item_pm != null){
				paymentMethod = new ArrayCollection(pkgOb.item_pm);	
				for(var i:int=0;i<pkgOb.item_pm.length;i++) {
					payMethods.addItem(pkgOb.item_pm[i]);
				}
				}
			}
			else { //other than balance
				payMethods = new ArrayCollection();
				
				trace("payment elseee");
				trace ("ini pkgOb.group" +pkgOb.group);
				// Modified 12/06/2013 - Hide hardcoded Kantong Kredit
				//
				// if(String(pkgOb.group).toLowerCase()!="indie" && isHybrid) payMethods.addItem(new PayMethod('00H',lm.getLangStr(81)));
				
				if(String(pkgOb.group).toLowerCase()!="indie")payMethods.addItem(new PayMethod('00',lm.getLangStr(80)));
				try
				{
					for(var y:int=0;y<pkgOb.pm.length;y++) {
						payMethods.addItem(pkgOb.pm[y]);
					}
				} 
				catch(error:Error) 
				{
					
				}
			
			}
		}
		
		public function drawBimaAvatar():void {
			if(!avatar) avatar = new BimaAvatar(app);
		}
		
		//		public var colorCodes:Array = [0x019ee5,0xffff00,0xff3333];
		public var colorCodes:Array = [0x019ee5,0xdddd00,0xff3333];
		public var notifyTxt:String;
		
		[Bindable] public var freeSites:ArrayCollection;
		[Bindable] public var shareQuotaData:ArrayCollection;
		[Bindable] public var promo:Object;
		[Bindable] public var newPromo:Object;
		[Bindable] public var billOwn:Object;
		//[Bindable] public var shareQuotaOwn:ArrayCollection = new ArrayCollection();
		[Bindable] public var bills:ArrayCollection = new ArrayCollection();
		[Bindable] public var billMSISDN:ArrayList = new ArrayList();
		[Bindable] public var billSummaries:ArrayCollection = new ArrayCollection();
		[Bindable] public var billSummaryMSISDN:ArrayList = new ArrayList();
		[Bindable] public var billSummaryStatus:ArrayList = new ArrayList();
		[Bindable] public var isPayment:Boolean = false;
		
		[Bindable] public var billList:ArrayCollection;
		[Bindable] public var quotaList:ArrayCollection;
		//revision 25032013 : Add 'bill' keyword to profile menu
		private var ui_keys:Array = ['balance','bill','blackberry','data','data_night','imei','free','msisdn',
			'notification','promo','sms_offnet','sms_onnet','validity','voice_offnet','voice_onnet','querybonus','user_profile'];
		[Bindable] public var profileMenu:ArrayCollection;
		[Bindable] public var initialDataQuota:int;
		
		//		HACK : Recreate Package Menu ONCE when package downloaded 		
		public var packageMenuCreated:Boolean = false;
		
		private var profile_keys:Array = ['promo','profile','balance','validity','credit_limit','due_date','data','imei',
			'data_night','blackberry','voice_onnet','voice_offnet','sms_onnet','sms_offnet','roaming_hajj_9d','roaming_hajj_30d',
			'roaming_bb_hajj_9d','roaming_bb_hajj_30d','freesite','notification','trims','share_quota'];
		private function generateProfileMenuFromAPI():void {
			if(!user.data) return;
			
			if(user.data.hasOwnProperty('msisdn_type')) {
				if(user.data.msisdn_type == 0) {
					isPostPaid = true;
					//					user.data.ui["msisdn"] = {content:null};
				}
			}
			if(user.data.hasOwnProperty('hybrid_type') && user.data.msisdn_type == "1") {
				trace ("hasownproperty hybrid_type");
				isHybrid = true;
				
				//cek reg unreg
				if(user.data.hasOwnProperty('type_bill')) {
					if(user.data['type_bill'] is Array) {
						if(user.data['type_bill'].length > 0) {
							typeBills = new ArrayCollection( user.data['type_bill'] );
							isTypeBillAvailable = true;
							isIndieExecutive = typeBills.length > 1;
						}
					}
				}
			}
			
			profileMenu = new ArrayCollection();
			
			for(var i:int=0;i<user.data.profile.length;i++) {
				var id:String = user.data.profile[i].id;
				var name:Object = user.data.profile[i].name;
				var percentage:Number = -1;
				var isContent:Boolean = user.data.profile[i].content.length > 0;
				var isNotification:Boolean = user.data.profile[i].id=="notification";
				var isFreeAccess:Boolean = user.data.profile[i].id=="freesite";
				var isPromo:Boolean = user.data.profile[i].id=="promo";
				var isShareQuota:Boolean = user.data.profile[i].id=="share_quota"; //for share quota
				
				var isBill:Boolean;
				var isEmail:Boolean = user.data.profile[i].id=="emailhybrid"||user.data.profile[i].id=="emailpostpaid"; //edit by bangkit for e bill
				var isBtnPayNow:Boolean = false;
				var content:ArrayCollection = isContent? new ArrayCollection(user.data.profile[i].content) : null;
				var isFreeSite:Boolean = user.data.profile[i].id=="freesite";
				
				
				
				if(isPromo) continue;//handled elsewhere
				if(isFreeSite) {
					freeSites = new ArrayCollection(user.data.profile[i].detail);
				}
				// share quota
				/*
				if(isShareQuota) {
					shareQuotaData = new ArrayCollection(user.data.profile[i].detail);
				}
				*/
				
				//====end
				var value:String = user.data.profile[i].value;
				
				var status:String = user.data.profile[i].detail == null ? "" : user.data.profile[i].detail.status;
				
				
				if(id=="profile") {
					value = Base64.decode(user.msisdn);
				}
				var btn_bill:String = user.data.profile[i].detail == null ? "" : user.data.profile[i].detail.btn_bill;
				if(id=="emailpostpaid") {
					var note1:String = user.data.profile[i].detail == null ? "" : user.data.profile[i].detail.note[0];
					var note2:String = user.data.profile[i].detail == null ? "" : user.data.profile[i].detail.note[1];
					
					user.email = value;
					user.status = status;
					user.note1= note1;
					
					user.note2=note2;
					
				} 
				if(id=="emailhybrid") {
					//var note1a:String = user.data.profile[i].detail.note[0];
					//var note2a:String = user.data.profile[i].detail.note[1];
					
					
					user.email = value;
					
					//user.status = status;
					//user.note1= note1a;
					
					//user.note2=note2a;
					
				} 
					
					
					
				else if(id=="data") {
					initialDataQuota = int(user.data.profile[i].detail == null ? 0 : user.data.profile[i].detail.quota_initial);
				}
				//				trace(id,name,value);
				var stoppableArray:ArrayCollection = new ArrayCollection(user.data.profile[i].list_pkg_stop);
				
				addProfileMenu();
			}
		//	trace ("its msisdn"+ user.data.others['share_quota'].child.msisdn[0]);
			try
			{
				for(var y:int=0;y<user.data.others['share_quota'].child.length;y++) {
					//	var isMsisdnShareQuota:ArrayList = user.data.others['share_quota'].child.msisdn[y];
					//trace ("its msisdn"+ user.data.others['share_quota'].child[y].msisdn);
					//user.data.others['share_quota'].child = quotaList;
					
					trace ("ohh noooo" + quotaList);
				}
			} 
			catch(error:Error) 
			{
				
			}
			
			
			function addProfileMenu():void {
				var pm:ProfileMenu = new ProfileMenu(id,name,value,stoppableArray,percentage,isContent,isNotification,isFreeAccess,isBill,isBtnPayNow,isEmail,isShareQuota);
				
				if(content) {
					//pm.expendable = true;
					var bindableContent:ArrayCollection = new ArrayCollection();
					for(var j:int=0;j<content.length;j++) {
						var ob:Object = new ObjectProxy();
						//						trace(content[j].value);
						ob.key = content[j].key;
						ob.value = content[j].value;
						bindableContent.addItem(ob);
					}
					pm.content = bindableContent;
				}
				if(id=="0") {
					profileMenu.addItemAt(pm,0);
				} else {
					profileMenu.addItem(pm);
				}
			}
			
			var billDataAvailable:Boolean = user.data.others['bill'] != null;
			if(billDataAvailable) {
				this.isBill = true;
				billOwn = user.data.others['bill'].own;
				
				//there's no 'value' property in due date so create one from 'date'
				if(billOwn.hasOwnProperty('payment')) {
					if(billOwn.payment.due_date.date != null) value = billOwn.payment.due_date.value = (billOwn.payment.due_date.date as String).split(" ")[0];
					billOwn.payment.current_status = new Object();
					
					//override label using key from API
					billOwn.payment.current_status.key = 'Status';
					billOwn.payment.current_status.value = billOwn.payment.status;
					
					//convert color to percentage
					if(billOwn.payment.due_date.color == 0) percentage = 50;
					else if(billOwn.payment.due_date.color == 1) percentage = 100;
					else percentage = 0;
				}
				if(billOwn == null) {
					user.data.others['bill'] = null;
				}
			}
			
			//revision 12042013 : Add Status before remaining credit limit and payment due date after remaining credit limit					
			if(billOwn) {
				id = "0";
				name = 51;
				isBill = false;
				isContent = false;
				lm.setLangStr(51,billOwn.status);
				isNotification = false;
				isBtnPayNow = billOwn.btn_pay==1?true:false;
				value = null;
				content = null;
				percentage = -1;
				addProfileMenu();
			}
			//trace ("hereeeeeeeeeeeeeeeeee :" +user.data.others['share_quota'].amount);
			var shareQuotaAvailable:Boolean = user.data.others['share_quota'] != null;
			if(shareQuotaAvailable) {
				this.isShareQuotaBill = true;
				
			//	var msisdn:String = Base64.encode(user.msisdn);
				//var shareQuotaOwn:ArrayList = user.data.others['share_quota'].child[0].remaining_quota;
				//trace ("quota own : "+shareQuotaOwn);
			//	trace ("hereeeeeeeeeeeeeeeeee :" +user.data.others['share_quota'].child[0].msisdn);
				
			//	for(var k:int=0;k<isMsisdnShareQuota.length;k++) {
					trace ("heyyy hooo");
				//}
					//shareQuotaOwn.child[k].msisdn == quotaList	
				//		trace ("msisdn ==="+quotaList);
				//	trace ("msisddddnnn ");
				//}
				
			}
			
		}
		
		
		public function updateEmail(str:String):void {
			for(var i:int=0;i<profileMenu.length;i++) {
				if(profileMenu[i].id=="emailhybrid") {
					profileMenu[i].value = user.email = str;
					break;
				}
				if(profileMenu[i].id=="emailpostpaid") {
					profileMenu[i].value = user.email = str;
					break;
				}
				
			}
		}
		
		private function isQuota(str:String):Boolean {
			return str.toLocaleLowerCase().indexOf("uota") > -1;
		}
		
		private function isValidity(str:String):Boolean {
			return str.toLocaleLowerCase().indexOf("validity") > -1 || str.toLocaleLowerCase().indexOf("masa") > -1;
		}
		
		[Bindable] public var packageMenu:ArrayCollection
		public var isRoamingMenu:Boolean;
		public var roamingGroupName:String;
		public function generatePackageMenu():void {
			//0xe8007e
			trace ("Try package trace");
			packageMenu = new ArrayCollection();
			PackageContent.prevIdTemp = new Stack();
			//test trace 
			
			for(var i:int=0;i<rawData.length;i++) {
				if((rawData[i].group == "Paket" || rawData[i].group == "Package") && rawData[i].parentid.length == 1 && rawData[i].parentid != 0) 
				{
					trace ("urutan name : "+ rawData[i].name);
					trace ("urutan parentid i char : "+ rawData[i].parentid);
					trace ("urutan parentid i char : "+ rawData[i].parentid);
					
					var pkgMenu:Object = new ObjectProxy();
					pkgMenu.name = rawData[i].name;
					pkgMenu.href = rawData[i].href;
					pkgMenu.group = rawData[i].group;
					pkgMenu.id = rawData[i].id;
					pkgMenu.menus = new ArrayCollection();				
					packageMenu.addItem(pkgMenu);
				}
			} 
			
			//
			
			
			
			//Revision 16/04/2013 : Reload And Payment moved to top bar
			//			var reloadMenu:Object = new ObjectProxy({id:73, name:lm.getLangStr(73)});
			//			packageMenu.addItem(reloadMenu);
			
			//Revision 04042013 : Payment menu
			//			var payment:Object = new ObjectProxy({id:78, name:lm.getLangStr(78)});
			//			packageMenu.addItem(payment);
			//========================================tutup sementara=======================================================
		/*
			var pkgMenu:Object = new ObjectProxy({id:70, name:lm.getLangStr(70)});
			packageMenu.addItem(pkgMenu);
			
			if(_payAsYouGo.id!=null&&_payAsYouGo.en!=null) {
				var payAsYouGoMenu:Object = new ObjectProxy({id:72, name:lm.getLangStr(72)});
				packageMenu.addItem(payAsYouGoMenu);
			}
			
			var specialPkgMenu:Object = new ObjectProxy({id:71, name:lm.getLangStr(71)});
			for(var i:int = 0;i<packages.length;i++) {
				if(packages[i].href == "#hybrid") {
					var hybrid:Object = new ObjectProxy({id:701,name:'INDIE'});
					packageMenu.addItem(hybrid);
					break;
				}
			}
			
			packageMenu.addItem(specialPkgMenu);
			
			if(isRoamingMenu) {
				var roamingPkgMenu:Object = new ObjectProxy({name:roamingGroupName});
				packageMenu.addItem(roamingPkgMenu);
				if(!roamingPackages) roamingPackages = PackageMenu.getRoamingPackage();
			}	*/
			//=======================================================================================================
			//revision 26/04/2013 menu trims pindah ke top bar
			//TO DO: Menu trims belum dipakai	
			//Update: Menu trims ngikutin menu dari server;
			
			/*			if(TRIms)
			{
			var trimsMenu:Object = new ObjectProxy({id:74, name:"TRIms"});
			packageMenu.addItem(trimsMenu);
			}*/
			
			//Revision 20032013 : Call Plan Change menu
			//===========================================tutup sementara===================================
		/*
			if(isPostPaid || true) {
				for(i = 0;i<rawData.length;i++) {
					if(rawData[i].href == "#payment") {
						var payment:Object = new ObjectProxy({id:78,name:lm.getLangStr(78)});
						packageMenu.addItem(payment);
						break;
					}
				}
				
				for(i = 0;i<rawData.length;i++) {
					if(rawData[i].href == "#call_plan") {
						var callPlan:Object = new ObjectProxy({id:76, name:lm.getLangStr(76)});
						packageMenu.addItem(callPlan);
						break;
					}
				}
				
				
				//Revision 20032013 : Credit Limit Change menu
				for(i = 0;i<rawData.length;i++) {
					if(rawData[i].href == "#credit_limit") {
						var creditLimit:Object = new ObjectProxy({id:77, name:lm.getLangStr(77)});
						packageMenu.addItem(creditLimit);
						break;
					}
				}
				
			} */
			//=======================end===========================
			
		}
		
		[Bindable] public var isLogingIn:Boolean;
		
		public var dialogShown:Boolean = false;
		
		
		//------ add function for sign, bangkit
		public function NoreturnToforceUpdate(e:BimaEvent):void {
			if(Service.monitor != null) Service.monitor.stop();
			user.data = e.params.data;
			//					trace("User data " + user.data.ui);
			if(user.data) {
				user.registered = true;
				try {
					if(user.data.profile[0].id=="promo") { //promo
						promo = new ObjectProxy(user.data.profile[0].detail);
					}
				} catch(e:Error) {
					
					promo = null;
				}
			}
			
		}
		//-------end-----------
		
		
		public function userSignin(mobileNo:String,password:String):void {
			
			requestPackagefresh = false;
			isLogingIn = true;
			if(!user.registered) avatar.showLoadingCircle(lm.getLangStr(7));
			dispatchEvent(new BimaEvent(BimaEvent.AVATAR_ON_LOGGING_IN_MODE));
			dialogShown = false;
			addEventListener(BimaEvent.SIGNIN_TIMEOUT_ERROR, function onTimeOutError(e:BimaEvent):void {
				removeEventListener(BimaEvent.SIGNIN_TIMEOUT_ERROR, onTimeOutError)
				
				if(dialogShown) return;
				dialogShown = true;
				
				isLogingIn = false;
				if(!isLoggedIn) {
					trace ("!isloggingin");
					var ad:AlertDialog = AlertDialog.showErrorAlert(appGroup, lm.getLangStr(124));
					avatar.ca.play('faint');
					ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
						ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
						avatar.ca.play('laugh2');
						dialogShown = false;
					});
				}
				avatar.hideLoadingCircle();
			});
			
			addEventListener(BimaEvent.IO_ERROR, function onIOError(e:BimaEvent):void {
				removeEventListener(BimaEvent.IO_ERROR, onIOError);
				if(dialogShown) return;
				dialogShown = true;
				
				isLogingIn = false;
				if(!isLoggedIn) {
					avatar.ca.play('faint');
					
					var ad:AlertDialog = AlertDialog.showErrorAlert(appGroup, lm.getLangStr(124));
					ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
						ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
						avatar.ca.play("laugh2");
						dialogShown = false;
					});
				}
				avatar.hideLoadingCircle();
			});
			addEventListener(BimaEvent.SIGNIN_CALLBACK, function onCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.SIGNIN_CALLBACK, onCallback);
				
				
				if(e.params.code=='200' || e.params.code == '301') {
					//---------------edit bangkit manual minor update -------------------------
					
					if(e.params.code == '301') {
						
						
						//e.params.code != '666';
						trace ("ini atas"+ e.params.code);
						if(e.params.hasOwnProperty("message")) {
							if(e.params.message == null) e.params.message =  lm.getLangStr(124);
						} else e.params.message =  lm.getLangStr(124);
						
						var linkDownload:String = e.params.data.application.url_download;
						var adg:AlertDialog = AlertDialog.showErrorAlertMinorUpdate(appWindow,e.params.message);
						
						
						
						
						adg.continueUpdate.addEventListener(MouseEvent.CLICK, function onClick(xe:MouseEvent):void {
							
							
							
							trace ("button continue");
							NoreturnToforceUpdate(e);
							
							if(!isLoggedIn) dispatchEvent(new BimaEvent(BimaEvent.USER_LOGGED_IN));
							isLoggedIn = true;
							isLogingIn = false;
							
							inboxUnread = getNotificationUnread();//user.data..ui.notification.unread;
							generateProfileMenuFromAPI();
							getPackages();
							//removeEventListener(BimaEvent.SIGNIN_CALLBACK, onCallback);
							
							
						});
						adg.UpdateVer.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
							
							
							var url:String = linkDownload;
							//trace (linkDownload);
							
							var request:URLRequest = new URLRequest(url);
							try {
								navigateToURL(request, '_blank');
							} catch (e:Error) {
								trace("Error occurred!");
							}
							adg.UpdateVer.removeEventListener(MouseEvent.CLICK, onClick);
							exit();
							
						}); 
						return;
					}
					if (e.params.code =='200')
					{ NoreturnToforceUpdate(e);
						if(!isLoggedIn) dispatchEvent(new BimaEvent(BimaEvent.USER_LOGGED_IN));
						isLoggedIn = true;
						isLogingIn = false;
						
						inboxUnread = getNotificationUnread();//user.data..ui.notification.unread;
						generateProfileMenuFromAPI();
						getPackages();}
					//---------------------------end--------------------------------------------
					trace ("this is 200");
					
				} 
					
					
					
				else {
					isLogingIn = false;
					
					avatar.ca.play('faint');
					
					if(dialogShown) return;
					dialogShown = true;
					
					
					
					if(e.params.code == '666') {
						trace ("this is 666");
						if(e.params.hasOwnProperty("message")) {
							if(e.params.message == null) e.params.message =  lm.getLangStr(124);
						} else e.params.message =  lm.getLangStr(124);
					}
					var ad:AlertDialog = AlertDialog.showErrorAlert(appWindow,e.params.message);
					ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
						trace ("from error manager");
						ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
						avatar.ca.play("laugh2");
						dialogShown = false;
					});
					
					avatar.hideLoadingCircle();
				}
			});
			
			Service.signin(mobileNo,password);
			
			
			
			trace("this the bottom of usersignin");
		}
		
		private function getNotificationUnread():int {
			if (user.data)
				for(var i:int=0;i<user.data.profile.length;i++) {
					if(user.data.profile[i].id=="notification") {
						return int(user.data.profile[i].value);
					}
				}
			return 0;
		}
		
		
		//revision 01052013 : Make sure that only ONE autoLogin thread
		[Bindable] public var isAutoLogingIn:Boolean = false;
		// autologin awal
		public function autologin():void {
			requestPackagefresh = false; // test menu/get3
			if(isAutoLogingIn) return;
			
			isAutoLogingIn = true;
			if(!user.registered) avatar.showLoadingCircle(lm.getLangStr(7));
			dispatchEvent(new BimaEvent(BimaEvent.AVATAR_ON_LOGGING_IN_MODE));
			
			addEventListener(BimaEvent.IO_ERROR, function onIOError(e:BimaEvent):void {
				removeEventListener(BimaEvent.IO_ERROR, onIOError);
				//no internet connection
				dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
				avatar.hideLoadingCircle();
				isAutoLogingIn = false;
			}); 
			addEventListener(BimaEvent.GET_PROFILE_CALLBACK, function onCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.GET_PROFILE_CALLBACK, onCallback);
				//				trace(e.params);
				isAutoLogingIn = false;
				
				if(e.params.code=='200' || e.params.code == '400') {
					//---------------edit bangkit auto minor update -------------------------
					
					if(e.params.code == '400') {
						
						
						//e.params.code != '666';
						trace ("ini atasan function callback di autologin");
						if(e.params.hasOwnProperty("message")) {
							if(e.params.message == null) e.params.message =  lm.getLangStr(124);
						} else e.params.message =  lm.getLangStr(124);
						
						var linkDownload:String = e.params.data.application.url_download;
						var adg:AlertDialog = AlertDialog.showErrorAlertMinorUpdate(appWindow,e.params.message);
						
						
						
						
						adg.continueUpdate.addEventListener(MouseEvent.CLICK, function onClick(xe:MouseEvent):void {
							
							
							
							
							NoreturnToforceUpdate(e);
							autologinSuccess = true;
							if(!isLoggedIn) dispatchEvent(new BimaEvent(BimaEvent.USER_LOGGED_IN));
							isLoggedIn = true;
							
							inboxUnread = getNotificationUnread();
							//	inboxUnread = user.data.ui.notification.unread;
							generateProfileMenuFromAPI();
							trace ("button continue 1");
							getPackages();
							trace ("button continue 2");
							//removeEventListener(BimaEvent.SIGNIN_CALLBACK, onCallback);
							
							
						});
						adg.UpdateVer.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
							
							
							var url:String = linkDownload;
							//trace (linkDownload);
							
							var request:URLRequest = new URLRequest(url);
							try {
								navigateToURL(request, '_blank');
							} catch (e:Error) {
								trace("Error occurred!");
							}
							adg.UpdateVer.removeEventListener(MouseEvent.CLICK, onClick);
							exit();
							
						}); 
						return;
					}
					if (e.params.code =='200')
					{ NoreturnToforceUpdate(e);
						autologinSuccess = true;
						if(!isLoggedIn) dispatchEvent(new BimaEvent(BimaEvent.USER_LOGGED_IN));
						isLoggedIn = true;
						
						
						//inboxUnread = user.data.ui.notification.unread; //sebelumnya
						inboxUnread = getNotificationUnread();
						
						generateProfileMenuFromAPI();
						
						getPackages();
						
						
					}
					//---------------------------end--------------------------------------------
					trace ("this is 200");
					
				}
				
				
				
				//tutup sementara
				/*	if(e.params.code=='200') {
				user.data = e.params.data;
				if(user.data) {
				user.registered = true;
				try {
				if(user.data.profile[0].id=="promo") { //promo
				promo = new ObjectProxy(user.data.profile[0].detail);
				}
				} catch(e:Error) {
				promo = null;
				}
				}
				
				autologinSuccess = true;
				if(!isLoggedIn) dispatchEvent(new BimaEvent(BimaEvent.USER_LOGGED_IN));
				isLoggedIn = true;
				
				inboxUnread = user.data.ui.notification.unread;
				generateProfileMenuFromAPI();
				
				getPackages();
				} */
				//============add major update manual bangkit=============
				if(e.params.code == '300') { 
					
					//e.params.code != '666';
					trace ("ini atas"+ e.params.code);
					if(e.params.hasOwnProperty("message")) {
						if(e.params.message == null) e.params.message =  lm.getLangStr(124);
					} else e.params.message =  lm.getLangStr(124);
					
					var linkDownload:String = e.params.data.application.url_download;
					var adg:AlertDialog = AlertDialog.showErrorAlertMajorUpdate(appWindow,e.params.message);
					
					
					adg.btnUpdateMajor.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
						
						
						var url:String = linkDownload;
						//trace (linkDownload);
						
						var request:URLRequest = new URLRequest(url);
						try {
							navigateToURL(request, '_blank');
						} catch (e:Error) {
							trace("Error occurred!");
						}
						adg.btnUpdateMajor.removeEventListener(MouseEvent.CLICK, onClick);
						exit();
						
					}); 
					return;
					
					
				}
					//======================end===================
				else if(e.params.code == '666') {
					//no response
					if(e.params.hasOwnProperty("message"))
					{
						if(e.params.message == null) e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
					}else e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
					
					var ad:AlertDialog = AlertDialog.showErrorAlert(appGroup, e.params.message);
					avatar.ca.play('faint');
					
					trace("GetProfile Failed");
					
					ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
						ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
						exit();
					});
					avatar.hideLoadingCircle();
				} else if(e.params.code == '208' && Service.autoLoginRetry > 0 ) {
					trace("Retrying Login" + Service.autoLoginRetry);
					Service.autoLoginRetry--;
					if(Service.autoLoginRetry >= 0) autologin();
				} else {
					dispatchEvent(new BimaEvent(BimaEvent.SHOW_LOGIN_WINDOW));
					avatar.hideLoadingCircle();
					trace("To Login");
				}
			});
			Service.autoLogin();
		}
		
		
		
		
		/*
		
		// autologin edit by bangkit , for test auto minor update
		public function autologin():void {
		if(isAutoLogingIn) return;
		
		isAutoLogingIn = true;
		if(!user.registered) avatar.showLoadingCircle(lm.getLangStr(7));
		dispatchEvent(new BimaEvent(BimaEvent.AVATAR_ON_LOGGING_IN_MODE));
		
		addEventListener(BimaEvent.IO_ERROR, function onIOError(e:BimaEvent):void {
		removeEventListener(BimaEvent.IO_ERROR, onIOError);
		//no internet connection
		dispatchEvent(new BimaEvent(BimaEvent.IO_ERROR));
		avatar.hideLoadingCircle();
		isAutoLogingIn = false;
		});
		addEventListener(BimaEvent.GET_PROFILE_CALLBACK, function onCallback(e:BimaEvent):void {
		removeEventListener(BimaEvent.GET_PROFILE_CALLBACK, onCallback);
		//				trace(e.params);
		trace ("call back in autologin");
		isAutoLogingIn = false;
		//edited by bangkit auto minor, 200
		if(e.params.code=='200' || e.params.code=='400') {
		//--------------try bangkit, minor auto updatee-------------
		
		
		//------------------------end----------------
		user.data = e.params.data;
		if(user.data) {
		user.registered = true;
		try {
		if(user.data.profile[0].id=="promo") { //promo
		promo = new ObjectProxy(user.data.profile[0].detail);
		}
		} catch(e:Error) {
		promo = null;
		}
		}
		
		autologinSuccess = true;
		if(!isLoggedIn) dispatchEvent(new BimaEvent(BimaEvent.USER_LOGGED_IN));
		isLoggedIn = true;
		trace("before");
		inboxUnread = user.data.ui.notification.unread;
		trace("after");
		generateProfileMenuFromAPI();
		
		getPackages();
		
		} 
		//============add major update manual bangkit=============
		if(e.params.code == '300') { 
		
		//e.params.code != '666';
		trace ("ini atas"+ e.params.code);
		if(e.params.hasOwnProperty("message")) {
		if(e.params.message == null) e.params.message =  lm.getLangStr(124);
		} else e.params.message =  lm.getLangStr(124);
		
		var linkDownload:String = e.params.data.application.url_download;
		var adg:AlertDialog = AlertDialog.showErrorAlertMajorUpdate(appWindow,e.params.message);
		
		
		adg.btnUpdateMajor.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
		
		
		var url:String = linkDownload;
		//trace (linkDownload);
		
		var request:URLRequest = new URLRequest(url);
		try {
		navigateToURL(request, '_blank');
		} catch (e:Error) {
		trace("Error occurred!");
		}
		adg.btnUpdateMajor.removeEventListener(MouseEvent.CLICK, onClick);
		exit();
		
		}); 
		return;
		
		
		}
		//======================end===================
		else if(e.params.code == '666') {
		//no response
		if(e.params.hasOwnProperty("message"))
		{
		if(e.params.message == null) e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
		}else e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
		
		var ad:AlertDialog = AlertDialog.showErrorAlert(appGroup, e.params.message);
		avatar.ca.play('faint');
		
		trace("GetProfile Failed");
		
		ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
		ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
		exit();
		});
		avatar.hideLoadingCircle();
		} else if(e.params.code == '208' && Service.autoLoginRetry > 0 ) {
		trace("Retrying Login" + Service.autoLoginRetry);
		Service.autoLoginRetry--;
		if(Service.autoLoginRetry >= 0) autologin();
		} else {
		dispatchEvent(new BimaEvent(BimaEvent.SHOW_LOGIN_WINDOW));
		avatar.hideLoadingCircle();
		trace("To Login");
		}
		});
		Service.autoLogin();
		}  */
		
		public function getProfile(callback:Function = null):void {
			trace ("this is get pro from m.getpro");
			/// edited by bangkit, just try for manual login minor update
			//	trace ("this is get profile");
			/*	isProcessing = true;
			avatar.showLoadingCircle(lm.getLangStr(7));
			addEventListener(BimaEvent.IO_ERROR, function onIOError(e:BimaEvent):void {
			removeEventListener(BimaEvent.IO_ERROR, onIOError);
			//no internet connectionve
			isProcessing = false;
			avatar.hideLoadingCircle();
			trace("No Internet Connection"); 
			}); 
			*/
			addEventListener(BimaEvent.GET_PROFILE_CALLBACK, function onCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.GET_PROFILE_CALLBACK, onCallback);
				if(e.params.code=='200' || e.params.code=='400') {
					//---------------edit bangkit manual minor update -------------------------
					
					if(e.params.code == '400') {
						
						NoreturnToforceUpdate(e);
						processToWatch = "backtoprofile";
						
						generateProfileMenuFromAPI();
						
						if(callback != null) callback();
					}
					//bangkit must 200
					if (e.params.code =='200')
					{
						
						NoreturnToforceUpdate(e);
						processToWatch = "backtoprofile";
						
						generateProfileMenuFromAPI();
						
						if(callback != null) callback();}
					//---------------------------end--------------------------------------------
					
				} else {
					//Revision 09072013: Handler when something wrong in getting profile
					
					avatar.ca.play("faint");
					if(e.params.hasOwnProperty("message"))
					{
						if(e.params.message == null) e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
					}else e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
					
					trace("Profile Failed");
					
					var ad:AlertDialog = AlertDialog.showErrorAlert(appWindow,e.params.message);
					ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
						ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
						exit();
					});
				}
				//				is
				isProcessing = false;
				avatar.hideLoadingCircle();
			});
			
			inboxUnread = getNotificationUnread();
			Service.getProfile(Base64.decode(user.msisdn)); //remove parameter later
			trace ("bottom of Service.getProfile");
			if(requestPackagefresh) {
				promo = null;
				//					m.hidePromoOnce = false;
			}
		}
		
		
		//edit by bangkit, faint for force update autologin minor update
		//-----------------------------------------add by  bangkit for minor update-------------------------------
		/*	public function getProfileMinorUpdate(callback:Function = null):void {
		isProcessing = true;
		avatar.showLoadingCircle(lm.getLangStr(7));
		addEventListener(BimaEvent.IO_ERROR, function onIOError(e:BimaEvent):void {
		removeEventListener(BimaEvent.IO_ERROR, onIOError);
		//no internet connectionve
		isProcessing = false;
		avatar.hideLoadingCircle();
		trace("No Internet Connection");
		});
		
		addEventListener(BimaEvent.GET_PROFILE_CALLBACK, function onCallback(e:BimaEvent):void {
		removeEventListener(BimaEvent.GET_PROFILE_CALLBACK, onCallback);
		var linkDownload:String = e.params.data.application.url_download;
		if(e.params.code=='200') {
		user.data = e.params.data;
		if(user.data) {
		user.registered = true;
		try {
		if(user.data.profile[0].id=="promo") { //promo
		promo = new ObjectProxy(user.data.profile[0].detail);
		}
		} catch(e:Error) {
		promo = null;
		}
		}
		
		processToWatch = "backtoprofile";
		
		generateProfileMenuFromAPI();
		
		if(callback != null) callback();
		} else {
		//Revision 09072013: Handler when something wrong in getting profile
		//edit by bangkit, faint for force update autologin minor update
		avatar.ca.play("faint");
		if(e.params.hasOwnProperty("message"))
		{
		if(e.params.message == null) e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
		}else e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
		
		trace("Profile Failed ebaruuu");
		
		var ad:AlertDialog = AlertDialog.showErrorAlertMinorUpdate(appWindow,e.params.message);
		
		ad.continueUpdate.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
		
		ad.continueUpdate.removeEventListener(MouseEvent.CLICK, onClick);
		
		//exit();
		});
		ad.UpdateVer.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
		
		
		var url:String = linkDownload;
		//trace (linkDownload);
		
		var request:URLRequest = new URLRequest(url);
		try {
		navigateToURL(request, '_blank');
		} catch (e:Error) {
		trace("Error occurred!");
		}
		ad.UpdateVer.removeEventListener(MouseEvent.CLICK, onClick);
		exit();
		
		});
		}
		
		//				is
		isProcessing = false;
		avatar.hideLoadingCircle();
		});
		Service.getProfile(Base64.decode(user.msisdn)); //remove parameter later
		if(requestPackagefresh) {
		promo = null;
		//					m.hidePromoOnce = false;
		}
		}
		*/
		//--------------------------------------------end--------------------------------------------------------
		
		
		
		
		[Bindable] public var contact:Object;
		
		private var rawData:Array;
		private var _paymentMethods:Object = new ObjectProxy({id:null,en:null});;
		public function set paymentMethods(value:Object):void {
			_paymentMethods = value;
		}
		[Bindable(event="languageChanged")]
		public function get paymentMethods():Object {
			return _paymentMethods[user.lang];
		}
		
		/*		private var _packages:Object = new ObjectProxy({id:null,en:null});
		[Bindable(event="languageChanged")]
		public function get packages():Object {
		return _packages[user.lang];
		}*/
		
		[Bindable] public var packages:Object;
		[Bindable] public var roamingPackages:Object;
		
		private var _tips:Object = new ObjectProxy({id:null,en:null});
		public function set tips(value:Object):void {
			_tips = value;
		}
		[Bindable(event="languageChanged")]
		public function get tips():Object {
			return _tips[user.lang];
		}
		
		private var _payAsYouGo:Object = new ObjectProxy({id:null,en:null});
		public function set payAsYouGo(value:Object):void {
			_payAsYouGo = value;
		}
		[Bindable(event="languageChanged")]
		public function get payAsYouGo():Object {
			return _payAsYouGo[user.lang];
		}
		
		public function getPackages(callback:Function = null):void {
			//get package for both language version
			addEventListener(BimaEvent.IO_ERROR, function onIOError(e:BimaEvent):void {
				trace ("this is getPackages from service as");
				removeEventListener(BimaEvent.IO_ERROR, onIOError);
				trace("IO_ERROR on GetPackage \n" + e.params);
				//				var ad:AlertDialog = AlertDialog.showErrorAlert(appWindow,lm.getLangStr(124));
				//				ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
				//					ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
				//					exit();
				//				});
			});
			addEventListener(BimaEvent.GET_PACKAGE_CALLBACK, function onGotPackageCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.GET_PACKAGE_CALLBACK, onGotPackageCallback);
				if(e.params.code == '666') {
					avatar.ca.play("faint");
					if(e.params.hasOwnProperty("message")) {
						if(e.params.message == null) e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
					} else e.params.message =  lm.getLangStr(124);//"Currently system is under maintenance. Please try again later.";
					
					trace("GetPackage Failed");
					
					var ad:AlertDialog = AlertDialog.showErrorAlert(appWindow,e.params.message);
					ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
						ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
						exit();
					});
				} else {
					/*					if(e.params.data.hasOwnProperty("version")) 
					{
					trace(e.params.data.version);
					user.version = e.params.data.version;
					}*/
					
					
					//25022013 : OPT
					//get the other language after the first language loaded
					//					if(user.lang == 'en') getPackage("id");
					//					else getPackage("en");
					//					getPackage(user.lang);
					//if(callback!=null) callback(); //tutup sementara
					processToWatch = "login";
				}
			});
			//25022013 : OPT
			//get current language first
			trace("User Language " + user.lang);
			getPackage(user.lang);
			getContacts(user.lang);
			//get the special one
			getSpecialPackage();
			
		}
		
		private function getContacts(lang:String = null):void {
			addEventListener(BimaEvent.GET_CONTACT_CALLBACK, function onGotContactCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.GET_CONTACT_CALLBACK, onGotContactCallback);
				//try edited, bangkit. must 200
				if(e.params.code=='200') {
					if(e.params.data) {
						trace(e.params.data);
						//						lm.langObj.data[30][lang] = e.params.data.content.title;
						//						lm.langObj.data[31][lang] = e.params.data.content.info;
						contactHeader = e.params.data.content.title;
						contactInfo = e.params.data.content.info;
					}
					
				}
			});
			
			Service.getContact(lang);
		}
		
		
		var dataCoba:Object ;
		public function getCoba(id:String):ArrayCollection {
			return PackageMenu.generateGlobalMenu(dataCoba.menu,id);
			
		}
		
		
		private function getPackage(lang:String=null):void {
			trace ("get package!");
			_tips[lang] = new ArrayCollection();			
			contact = new ObjectProxy();
			addEventListener(BimaEvent.GET_PACKAGE_CALLBACK, function onGotPackageCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.GET_PACKAGE_CALLBACK, onGotPackageCallback);
				
				try {
					//this must 200, bangkit
					if(e.params.code=='200') { //success
						trace ("what package is this?");
						
						var data:Object = e.params.data;
						dataCoba = data;
						/*						try {
						trace("Check again promo");
						if(data.ui[ui_keys[7]]) {
						promo = new ObjectProxy(data.ui[ui_keys[7]]); //promo is on 7th index on ui_keys
						}
						} catch(e:Error) {
						trace("No promo");
						promo = null;
						}*/
						if(data.contact) {
							contact.phone = new ArrayCollection(data.contact.phone);
							contact.email = new ArrayCollection(data.contact.email);
							contact.web = new ArrayCollection(data.contact.web);
						}
						//						if(data.payment_method) {
						//							_paymentMethods[lang] = data.payment_method;
						//						}
						if(data.menu) {
							var isTip:Boolean;
							var isPayAsYouGo:Boolean;
							for(var i:int=0;i<data.menu.length;i++) {
								isPayAsYouGo = String(data.menu[i].name).toLowerCase()=='pay as you go';
								if(isPayAsYouGo) {
									_payAsYouGo[lang] = new ObjectProxy(data.menu[i]);
								}
								
								if(String(data.menu[i].name).toLowerCase()=='topup')
									TopUp = true;
								
								if(String(data.menu[i].name).toLowerCase()=='trims')
									TRIms = true;
								
								isTip = String(data.menu[i].group).toLowerCase().indexOf('tips') > -1 && data.menu[i].desc!='';
								if(isTip) { 
									//									if(!_tips[lang]) _tips[lang] = new ArrayCollection();			
									var tipsAlreadyThere:Boolean = false;
									for(var j:int=0;j<_tips[lang].length;j++) {
										trace(_tips[lang][j].name,data.menu[i],_tips[lang][j].name==data.menu[i]);
										if(_tips[lang][j].name==data.menu[i]) {
											trace("already there");
											tipsAlreadyThere = true;
											break;
										}
									}
									if(!tipsAlreadyThere) _tips[lang].addItem(data.menu[i]);
								}
							}
							
							rawData = data.menu;
							
							trace (rawData.length+"ini packages : xx " +rawData as String);
							
							
							packages = PackageMenu.generatePackageMenu(data.menu);
							trace ("ini packages :" +packages[1].name);
							roamingPackages = null;
							
							//		HACK : Recreate Package Menu ONCE when package downloaded
							//------------------------------------------------------------------
							if(!packageMenuCreated) {
								packageMenuCreated = true;
								generatePackageMenu();
							}
							//------------------------------------------------------------------
						}
					} else {
						//retry on failed current language menu/get API call
						//AlertDialog.showErrorAlert(appGroup,"Pengambilan data paket tidak berhasil: "+e.params.message);
					}
				} catch(e:Error) {
					trace(e);
					//retry on failed current language menu/get API call
				}
				
				//25022013 : BUG FIX : tips not shown after login
				dispatchEvent(new Event("languageChanged"));
			});
			Service.getPackage(lang);
		}
		
		public function getPackageDetails(id:String):Object {
			for(var i:int=0;i<packages.length;i++) {
				for(var j:int=0;j<packages[i].menus.length;j++) {
					if(packages[i].menus[j].code==id) {
						return packages[i].menus[j];
					}
				} 
			}
			return null;
		}
		
		[Bindable] public var specialPackage:Object;
		[Bindable] public var noSpPkgMsg:String;
		private function getSpecialPackage():void {
			specialPackage = null;
			addEventListener(BimaEvent.GET_SPECIAL_PACKAGE_CALLBACK, function onGotSpPackageCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.GET_SPECIAL_PACKAGE_CALLBACK, onGotSpPackageCallback);
				//edited by bangkit must 200, try
				if(e.params.code=='200') {
					trace("this is getspecialpackage :" + e.params.data);
					specialPackage = new ObjectProxy(e.params.data);
					var myPattern:RegExp = / /;
					
					//Revision 03052013: BUGFIX, undefined 'c' when using payment gateway, missing property 'code'
					specialPackage.code = specialPackage.pkg_code = String(specialPackage.pkg_code).replace(myPattern,'+');
					specialPackage.imageUrl = Service.getPackageImageUrl(specialPackage.pkg_code);
					specialPackage.autoRenewal = specialPackage.auto_renewal;
					//trace(specialPackage.pkg_code);
					specialPackage.chartData = new ArrayCollection();
					for(var i:int=0;i<specialPackage.before_content.length;i++) {
						var ob:Object = new ObjectProxy();
						ob.key = String(specialPackage.before_content[i].key + ': ' + specialPackage.before_content[i].value + '%');
						ob.value = specialPackage.before_content[i].value;
						specialPackage.chartData.addItem(ob);
					}
					/*				} else if(e.params.code == '203' || e.params.code == '204' || e.params.code == '208'){
					var ad:AlertDialog = AlertDialog.showErrorAlert(appWindow,e.params.message);
					ad.okBtn.addEventListener(MouseEvent.CLICK, function onClick(e:MouseEvent):void {
					ad.okBtn.removeEventListener(MouseEvent.CLICK, onClick);
					avatar.ca.play("laugh2");
					dialogShown = false;
					addEventListener(BimaEvent.GET_NEW_TOKEN_CALLBACK, function onNewToken(e:BimaEvent):void
					{
					removeEventListener(BimaEvent.GET_NEW_TOKEN_CALLBACK, onNewToken);
					getSpecialPackage();
					});
					});*/
				} else{
					noSpPkgMsg = e.params.message;
				}
			});
			Service.getSpecialPackage();
		}
		
		
		
		[Bindable] public var inbox:ArrayCollection;
		[Bindable] public var inboxUnread:int;
		public function getInbox():void {
			addEventListener(BimaEvent.IO_ERROR, function onIOError(e:BimaEvent):void {
				removeEventListener(BimaEvent.IO_ERROR, onIOError);
			});
			addEventListener(BimaEvent.GET_INBOX_CALLBACK, function onCallback(e:BimaEvent):void {
				removeEventListener(BimaEvent.GET_INBOX_CALLBACK, onCallback);
				//bangkit try, must 200
				if(e.params.code=='200') {
					//inbox retrieved
					generateInbox(e.params.data);
					inboxUnread = getInboxUnread();
				}
			});
			Service.getInbox();
		}
		
		private function generateInbox(inboxArr:Array):void {
			if(!inboxArr) return;
			var now:Date = new Date();
			
			inbox = new ArrayCollection();
			for(var i:int=0;i<inboxArr.length;i++) {
				var ob:Object = new ObjectProxy();
				ob.id = inboxArr[i].id;
				ob.status = inboxArr[i].status;
				ob.to = inboxArr[i].to;
				ob.message = inboxArr[i].message;
				
				ob.datetime = inboxArr[i].datetime;
				ob.date = getDate(ob.datetime);
				var msgTime:Date = new Date(ob.date.year,ob.date.month-1,ob.date.day,ob.date.hours,ob.date.minutes,ob.date.seconds);
				ob.delta = getDelta(now,msgTime);
				ob.isFold = true;
				inbox.addItem(ob);
			} 
		}
		
		private function getDate(datetime:String):Object {
			var dateStr:String = datetime.split(' ')[0];
			var br:Array = dateStr.split('-');
			
			var clockStr:String = datetime.split(' ')[1];
			var cr:Array = clockStr.split(':');
			
			var ob:Object = new ObjectProxy();
			ob.day = br[2];
			ob.month = br[1];
			ob.monthNameId = 159 + int(ob.month); //for 159 refer to BimaLanguageData.as
			ob.year = br[0];
			
			ob.hours = cr[0];
			ob.minutes = cr[1];
			ob.seconds = cr[2];
			
			return ob;
		}
		
		private function getDelta(now:Date,msgTime:Date):Object {
			var ob:Object = new Object();
			
			var days:uint = Math.abs(now.valueOf() - msgTime.valueOf())/86400000; //1 day = 86400 seconds
			var hours:uint = (Math.abs(now.valueOf() - msgTime.valueOf())%86400000)/3600000; //1 hour = 3600 seconds
			var minutes:uint = ((Math.abs(now.valueOf() - msgTime.valueOf())%86400000)%3600)/60000; //1 minute = 60 seconds
			ob.days = days;
			ob.hours = hours;
			ob.minutes = minutes;
			//trace(days,hours,minutes);
			//			var txt:String = String(day) + ' ';
			//			if(user.lang=='id') {
			//				txt += 'hari yang lalu';
			//			} else {
			//				if(day < 1) {
			//					txt += 'day ago';
			//				} else {
			//					txt += 'days ago';
			//				}
			//			}
			return ob;
		}
		
		public function getInboxUnread():int {
			if(!inbox) return 0;
			var c:int = 0;
			var cc:int = 0;
			for(var i:int=0;i<inbox.length;i++) {
				if(inbox[i].status==0) c++;
				else cc++;
			}
			//trace('unread: ',user.getUnreadMessage(),inbox.length,c,cc);
			return c;
		}
		
		public var currDate:Date = new Date();
		
		[Bindable] public var pullNotifOb:Object;
		private var tpn:Timer = new Timer(60000,1);//Test Please change back to 15000
		public function pullNotification():void {
			trace("Pooling started");
			if(!tpn.hasEventListener(TimerEvent.TIMER_COMPLETE)) {
				tpn.addEventListener(TimerEvent.TIMER_COMPLETE, function onComplete(e:TimerEvent):void {
					tpn.removeEventListener(TimerEvent.TIMER_COMPLETE, onComplete);
					if(isLoggedIn) {
						addEventListener(BimaEvent.GET_PULL_NOTIFICATION_CALLBACK, function onCallback(e:BimaEvent):void {
							removeEventListener(BimaEvent.GET_PULL_NOTIFICATION_CALLBACK, onCallback);
							//edited bangkit, try, must 200
							if(e.params.code==200) { //success
								//show pull notif
								try {
									
									//revisision 24/04/2013: Check if notification is from today or yesterday only
									trace(DateFormatter.parseDateString(e.params.data[0].datetime).time);
									trace(currDate.time);
									trace(Math.abs(currDate.time - DateFormatter.parseDateString(e.params.data[0].datetime).time));
									
									if(Math.abs(currDate.time - DateFormatter.parseDateString(e.params.data[0].datetime).time) < 1000 * 60 * 60 * 24) {
										pullNotifOb = new ObjectProxy();
										pullNotifOb.to = e.params.data[0].to;
										pullNotifOb.message = e.params.data[0].message;
										pullNotifOb.datetime = e.params.data[0].datetime;
										pullNotifOb.status = e.params.data[0].status;
										pullNotifOb.source = e.params.data[0].source;
										pullNotifOb.type = e.params.data[0].type;							
										
										if(!notification.isShowing) notification.show();
									} else {
										trace("Message is too old");
									}
								} catch(err:Error) {
									trace(err);
								}
							} else if(e.params.code == 1004) {
								//revision 25/04/2013 redirect to profile page when token is expired;
								trace("token is expired");
								appWindow.showProfilePage();
							} else { //pull again periodically
								//							pullNotifOb = new ObjectProxy();
								//							pullNotifOb.message = "Sisa kuota paket kamu adalah 800KB. Isi kuota untuk menikmati layanan internet TRI dengan kecepatan penuh"
								//							pullNotifOb.source = "pcrf";
								//							app.notification.show();
							}
							pullNotification();
						});
						Service.pullNotification();
					}
				});
			}
			tpn.start();
		}
		
		public static function hexToRGB(hex:Number):Object {
			var rgbObj:Object = {
				red: ((hex & 0xFF0000) >> 16),
				green: ((hex & 0x00FF00) >> 8),
				blue: ((hex & 0x0000FF))
			};
			
			return rgbObj;
		}
		
		public static function getColorFilter(c:uint):flash.filters.ColorMatrixFilter {
			var rgb:Object = hexToRGB(c);
			var matrix:Array = new Array();
			var clr:Object = new Object();
			clr.R = rgb.red/255*1;
			clr.G = rgb.green/255*1;
			clr.B = rgb.blue/255*1;
			matrix=matrix.concat([clr.R,0,0,0,0]);// red
			matrix=matrix.concat([0,clr.G,0,0,0]);// green
			matrix=matrix.concat([0,0,clr.B,0,0]);// blue
			matrix=matrix.concat([0,0,0,1,0]);// alpha
			return new flash.filters.ColorMatrixFilter(matrix);
		}
		
		public function logoutCleaning():void {
			user.registered = false;
			isLoggedIn = false;
			isLogingIn = false;
			isPayment = false;
			reloadPageReady = false;
			packages = null;
			roamingPackages = null;
			
			if(PackageMenu.reloadButtonMenu) PackageMenu.reloadButtonMenu.removeAll();
			
			user.resetByLogout();
			
			isHybrid = false;
			isPostPaid = false;
			isShowingPromo = false;
			freeSites = null;
			promo = null;
			profileMenu = null;
			packageMenu = null;
			
			billOwn = null;
			TRIms = false;
			TopUp = false;
			isBill = false;
			contact = null;
			/*			if(billSummaries) billSummaries.removeAll();
			if(billSummaryMSISDN) billSummaryMSISDN.removeAll();
			if(billSummaryStatus) billSummaryStatus.removeAll();
			if(billMSISDN) billMSISDN.removeAll();
			if(bills) bills.removeAll();*/
			//			packages = new ObjectProxy({id:null,en:null});
			packageReady = false;
			packageMenuCreated = false;
			tips = new ObjectProxy({id:null,en:null});
			payAsYouGo = new ObjectProxy({id:null,en:null});
			specialPackage = null;
			inbox = null;
			billingPaymentMenu = null;
			//hide pull notif alert if shown
			if(notification.visible) {
				notification.visible = false;
			}
		}
		
		public function activateApp(event:Event=null):void {
			app.activate();
			//app.stage.nativeWindow.activate();
		}
		
		public function notify():void {
			if(NativeApplication.supportsDockIcon){
				var dock:DockIcon = NativeApplication.nativeApplication.icon as DockIcon;
				dock.bounce(NotificationType.CRITICAL);
			} else if (NativeApplication.supportsSystemTrayIcon){
				app.stage.nativeWindow.notifyUser(NotificationType.CRITICAL);
			}
		}
		
		public function dock(event:Event=null):void {
			appWindow.visible = false;
			if(notification.visible) {
				notification.visible = true;
			} else {
				//move bima to notification pos
				notification.minimize();
			}
		}
		
		public function undock(event:Event=null):void {
			activateApp();
			appWindow.visible = true;
			appWindow.stage.nativeWindow.visible = true;
			if(!avatar.ca) return;
			var isOnDockAndNotShowingNotification:Boolean = avatar.ca.animLabel=='fly' && !notification.visible;
			if(isOnDockAndNotShowingNotification) {
				notification.flyToMainApp();
			} else {
				appWindow.stage.nativeWindow.visible = true;
			}
			app.alwaysInFront = true; app.alwaysInFront = false;
		}
		
		public function exit():void {
			NativeApplication.nativeApplication.exit();
		}
		
		private function setApplicationNameAndVersion():void {
			var appXML:XML = NativeApplication.nativeApplication.applicationDescriptor;
			var ns:Namespace = appXML.namespace();
			appName = appXML.ns::name;
			appVersion = appXML.ns::versionNumber;
			//trace(m.appVersion,m.appName);
		}
		
		private function checkNewUpdate():void {
			var appUpdater:ApplicationUpdaterUI = new ApplicationUpdaterUI();
			appUpdater.configurationFile = new File("app:/config/update-config.xml");
			appUpdater.isCheckForUpdateVisible = false;
			
			appUpdater.addEventListener(ErrorEvent.ERROR, function onError(e:ErrorEvent):void {
				appUpdater.removeEventListener(ErrorEvent.ERROR, onError);
				//trace(e.toString());
				//Alert.show(e.toString());
			});
			appUpdater.addEventListener(UpdateEvent.INITIALIZED, function onInitialized(e:UpdateEvent):void {
				appUpdater.removeEventListener(UpdateEvent.INITIALIZED, onInitialized);
				appUpdater.checkNow();
			});
			appUpdater.addEventListener(DownloadErrorEvent.DOWNLOAD_ERROR, function onDownloadError(e:DownloadErrorEvent):void {
				appUpdater.removeEventListener(DownloadErrorEvent.DOWNLOAD_ERROR, onDownloadError);
				//AlertDialog.showErrorAlert(appWindow,e.toString());
			});
			appUpdater.addEventListener(StatusFileUpdateErrorEvent.FILE_UPDATE_ERROR, function onFileUpdateError(e:StatusFileUpdateErrorEvent):void {
				appUpdater.removeEventListener(StatusFileUpdateErrorEvent.FILE_UPDATE_ERROR, onFileUpdateError);
				//AlertDialog.showErrorAlert(appWindow,e.toString());
			});
			appUpdater.addEventListener(StatusUpdateErrorEvent.UPDATE_ERROR, function onUpdateError(e:StatusUpdateErrorEvent):void {
				appUpdater.removeEventListener(StatusUpdateErrorEvent.UPDATE_ERROR, onUpdateError);
				//AlertDialog.showErrorAlert(appWindow,e.toString());
			});
			appUpdater.initialize();
		}
		
		private function setTaskBarMenu(menu:NativeMenu):void {
			var bmp16:Bitmap = new IconDock16Class();
			var bmp72:Bitmap = new IconDock72Class();
			var bmp128:Bitmap = new IconDock128Class();
			NativeApplication.nativeApplication.icon.bitmaps = [bmp16.bitmapData,bmp72.bitmapData,bmp128.bitmapData];
			if(NativeApplication.supportsDockIcon) {
				isOnMac = true;
				var dockIcon:DockIcon = NativeApplication.nativeApplication.icon as DockIcon;
				NativeApplication.nativeApplication.addEventListener(InvokeEvent.INVOKE, undock);
				dockIcon.menu = menu;
			} else if (NativeApplication.supportsSystemTrayIcon){
				isOnWindows = true;
				var sysTrayIcon:SystemTrayIcon = NativeApplication.nativeApplication.icon as SystemTrayIcon;
				sysTrayIcon.tooltip = "BimaTRI";
				sysTrayIcon.addEventListener(MouseEvent.CLICK,undock);
				sysTrayIcon.menu = menu;
			}
			bmp16.smoothing = bmp72.smoothing = bmp128.smoothing = true;
		}
		
		private function createIconMenu():NativeMenu {
			var exitCommand:NativeMenuItem = new NativeMenuItem("Exit");
			
			var iconMenu:NativeMenu = new NativeMenu();
			iconMenu.addItem(exitCommand);
			exitCommand.addEventListener(Event.SELECT, function onExitCommandSelect(e:Event):void {
				exit();
			});
			
			return iconMenu;
		}
		
		private function createFullIconMenu():NativeMenu {
			var showCommand:NativeMenuItem = new NativeMenuItem("Maximize");
			var hideCommand:NativeMenuItem = new NativeMenuItem("Minimize");
			var exitCommand:NativeMenuItem = new NativeMenuItem("Exit");
			
			var iconMenu:NativeMenu = new NativeMenu();
			iconMenu.addItem(showCommand);
			iconMenu.addItem(hideCommand);
			iconMenu.addItem(exitCommand);
			showCommand.addEventListener(Event.SELECT, function onShowCommandSelect(e:Event):void {
				undock();
			});
			hideCommand.addEventListener(Event.SELECT, function onHideCommandSelect(e:Event):void {
				dock();
			});
			exitCommand.addEventListener(Event.SELECT, function onExitCommandSelect(e:Event):void {
				activateApp();
				AlertDialog.showQuitAlert(appGroup);
			});
			
			return iconMenu;
		}
		
	}
	
}