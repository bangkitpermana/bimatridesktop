package core.data
{
	import mx.collections.ArrayCollection;

	public class ProfileMenu
	{
		
		public var id:String;
		[Bindable] public var name:Object;
		[Bindable] public var value:String;
		[Bindable] public var stoppableArray:ArrayCollection;
		[Bindable] public var percentage:Number;
		[Bindable] public var content:ArrayCollection;
		
		[Bindable] public var isNotification:Boolean;
		[Bindable] public var isContent:Boolean;
		[Bindable] public var isFreeAccess:Boolean;
		[Bindable] public var isBill:Boolean;
		[Bindable] public var isBtnPayNow:Boolean;
		[Bindable] public var isEmail:Boolean;
		[Bindable] public var isShareQuota:Boolean;
		
		[Bindable] public var isPromo:Boolean;
		[Bindable] public var isStoppable:Boolean;
		
		public var expendable:Boolean;
		[Bindable] public var isExtended:Boolean;
		
		public function ProfileMenu(id:String,name:Object,value:String,stoppableArray:ArrayCollection,percentage:Number=-1,isContent:Boolean=false,isNotif:Boolean=false,isFreeAccess:Boolean=false,isBill:Boolean=false, isBtnPayNow:Boolean = false,isEmail:Boolean=false,isShareQuota:Boolean=false):void {
			this.id = id;
			this.name = name;
			this.value = value;
			this.stoppableArray = stoppableArray;
			this.percentage = percentage;
			
			this.isNotification = isNotif;
			this.isContent = isContent;
			this.isFreeAccess = isFreeAccess;
			this.isBill = isBill;
			this.isBtnPayNow = isBtnPayNow;
			this.isEmail = isEmail;
			this.isShareQuota = isShareQuota;
			
			this.isStoppable = stoppableArray.length > 0;
		}
		
	}
}