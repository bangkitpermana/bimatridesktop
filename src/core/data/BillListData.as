package core.data
{
	import mx.collections.ArrayCollection;
	import mx.utils.ObjectProxy;

	public class BillListData
	{
		
		[Bindable] public var period:String;
		[Bindable] public var invoiceID:String;
		[Bindable] public var totalBill:String;
		[Bindable] public var summary:ArrayCollection = new ArrayCollection();
		[Bindable] public var topup:Object = new ObjectProxy();
		[Bindable] public var usage:Object = new ObjectProxy();
		public var selected:Boolean;
		
		public function BillListData(o:Object):void {
			period = o.periode;
			invoiceID = o.invoice_id;
			totalBill = o.total_bill;
			for(var i:int=0;i<o.summary.length;i++) {
				var s:Object = new ObjectProxy();
				s.label = o.summary[i].label;
				s.value = o.summary[i].value;
				summary.addItem(s);
			}
			
			if(o.topup) {
				if(o.topup.header) {
					topup.header = new ArrayCollection(o.topup.header);
					topup.rows = new ArrayCollection();
					for(i=0;i<o.topup.rows.length;i++) {
						var r1:ArrayCollection = new ArrayCollection(o.topup.rows[i]);
						topup.rows.addItem(r1);
					}
				}
			}
			
			usage.header = new ArrayCollection(o.usage.header);
			usage.rows = new ArrayCollection();
			for(i=0;i<o.usage.rows.length;i++) {
				var r2:ArrayCollection = new ArrayCollection(o.usage.rows[i]);
				usage.rows.addItem(r2);
			}
		}
		
	}
}