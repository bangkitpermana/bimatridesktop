package core
{
	import carousel.Carousel;
	
	import com.ab.display.special.MotionBlurAsset;
	
	import flash.display.MovieClip;
	import flash.display.NativeWindow;
	import flash.display.Sprite;
	import flash.display.Stage;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.filters.BitmapFilterQuality;
	import flash.filters.GlowFilter;
	import flash.geom.Point;
	import flash.text.TextField;
	import flash.utils.Timer;
	
	import mx.core.UIComponent;
	import mx.events.EffectEvent;
	import mx.states.AddChild;
	
	import org.casalib.util.StageReference;
	
	import spark.effects.Fade;
	import spark.effects.Move;
	import spark.effects.animation.RepeatBehavior;
	import spark.effects.easing.Bounce;
	import spark.effects.easing.Power;
	import spark.effects.easing.Sine;
	import spark.filters.BlurFilter;
	
	//[Embed(source="assets/bima.swf",symbol="Ball")]
	public class BimaAvatar extends Sprite
	{
		
		[Embed(source="assets/bima.swf",symbol="Bima")]
		public var AvatarClass:Class;
		public var asset:MovieClip;
		public var ca:CharacterAnimation;
		
		public var loadingCircle:MovieClip;
		
		[Embed(source="assets/bima.swf",symbol="Curtain")]
		public var CurtainClass:Class;
		public var curtain:Sprite;
		
		private var m:Manager = Manager.getInstance();
		
		private var bound:Point;
		private var app:NativeWindow;
		public var con:UIComponent;
		
		private var centerPos:Point;
		public var dockPos:Point;
		public var notifPos:Point;
		
		private var loadingLbl:TextField;
		public function BimaAvatar(app:NativeWindow):void {
			this.bound = new Point(m.appWindow.width,m.appWindow.height);
			this.app = app;
			this.con = m.appWindow.avatarCon;
			this.con.addChild(this);
			var t:Timer = new Timer(1000,1);
			t.addEventListener(TimerEvent.TIMER_COMPLETE, function onCompleted(e:TimerEvent):void {
				t.removeEventListener(TimerEvent.TIMER_COMPLETE, onCompleted);
				init();
			});
			t.start();
			
			dockPos = new Point(bound.x/7,bound.y*4/5);
			notifPos = new Point(m.screenRes.x-50-m.appWindow.x, m.screenRes.y-m.appWindow.y-10);
			
			setPos(dockPos);
			
			loadingCircle = new m.LoadingCircleClass();
			loadingCircle.y = -230;
			addChild(loadingCircle);
			fader = new Fade();
			fader.duration = 1000;
			fader.repeatCount = int.MAX_VALUE;
			fader.alphaFrom = 1;
			fader.alphaTo = 0;
			fader.repeatBehavior = RepeatBehavior.REVERSE;
			hideLoadingCircle();
			
			initAvatarListeners();
		}
		
		private function initAvatarListeners():void {
			addEventListener(Event.ADDED_TO_STAGE, function onAdded(e:Event):void {
				removeEventListener(Event.ADDED_TO_STAGE, onAdded);
				var t:Timer = new Timer(1000,1);
				t.addEventListener(TimerEvent.TIMER_COMPLETE, function onCompleted(e:TimerEvent):void {
					t.removeEventListener(TimerEvent.TIMER_COMPLETE, onCompleted);
					init();
				});
				t.start();
			});
			
			m.addEventListener(BimaEvent.AVATAR_ON_LOGGING_IN_MODE, function onUserLoggingIn(e:BimaEvent):void {
				m.removeEventListener(BimaEvent.AVATAR_ON_LOGGING_IN_MODE, onUserLoggingIn)
				//moveTo(new Point(bound.x*3/4,bound.y*3/4));
				if(isMoving) stopMove();
				setPos(dockPos);
			});
			m.addEventListener(BimaEvent.USER_LOGGED_IN, function onUserLogged(e:BimaEvent):void {
				hideLoadingCircle();
			});
		}
		
		public function setPos(pos:Point):void {
			con.x = pos.x;
			con.y = pos.y;
		}
		
		private var isMoving:Boolean;
		private var mover:Move = new Move();;
		private const s:uint = 2500; //move speed
		public function moveTo(pos:Point):void {
//			trace("con.toString()",con.toString());
//			trace("BimaAvatar.moveTo");
			if(isMoving) return;
//			trace("BimaAvatar.moveTo pass return");
			isMoving = true;
			var r:Number = Math.sqrt(Math.pow(pos.x-con.x,2)+Math.pow(pos.y-con.y,2));
			
			mover.xFrom = con.x; mover.yFrom = con.y;
			setPos(pos);
			mover.xTo = pos.x; mover.yTo = pos.y;
			mover.duration = r/s*3000;
			mover.addEventListener(EffectEvent.EFFECT_END, function onFxEnd(e:EffectEvent):void {
				mover.removeEventListener(EffectEvent.EFFECT_END, onFxEnd);
				isMoving = false;
				m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_MOVE_COMPLETE));
//				trace("BimaAvatar.conpos: ",con.x,con.y);
				//dispatchEvent(e);
			});
			mover.play([con]);
		}
		private function stopMove():void {
			if(isMoving) {
				mover.stop();
				isMoving = false;
			}
		}
		
		private function init():void {
			StageReference.setStage(stage);
			asset = new AvatarClass();
			asset.mouseChildren = asset.mouseEnabled = false;
			addChild(asset);
			ca = new CharacterAnimation(asset);
			asset.stop();
			
			//curtain
			curtain = new CurtainClass();
			addChild(curtain);
			curtain.blendMode = 'alpha';
			
			mouseEnabled = mouseChildren = false;
			asset.mouseEnabled = asset.mouseChildren = false;
			curtain.mouseEnabled = curtain.mouseChildren = false;
			
//			ca.addEventListener(BimaEvent.ANIMATION_END, function onEnd(e:BimaEvent):void {
//				ca.removeEventListener(BimaEvent.ANIMATION_END, onEnd);
//				
//				ca.stop("smile");
//				autoHover(con);
//				m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_READY));
//			});
//			ca.play('fly2');
			
			ca.stop("smile");
			fadeIn();
		}
		
		public function fadeIn():void {
			//ca.stop("smile");
			curtain.y = -220;
			curtain.scaleX = curtain.scaleY = 1;
			asset.cacheAsBitmap = curtain.cacheAsBitmap = true;
			asset.mask = curtain;
			var diff:int = 100;
			var d:int = Math.abs(440)/diff;
			var ft:Timer = new Timer(2000/diff,diff);
			ft.addEventListener(TimerEvent.TIMER, function onTimer(e:TimerEvent):void {
				curtain.y += d;
			});
			ft.addEventListener(TimerEvent.TIMER_COMPLETE, function onComplete(e:TimerEvent):void {
				ft.removeEventListener(TimerEvent.TIMER_COMPLETE, onComplete);
				asset.mask = null;
				//removeChild(curtain);
				//autoHover(con);
				//m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_READY));
				
				//salto
//				con.filters = [new BlurFilter(1.5, 1.5, BitmapFilterQuality.HIGH)]
				ca.addEventListener(BimaEvent.ANIMATION_END, function onEndSalto(e:BimaEvent):void {
					ca.removeEventListener(BimaEvent.ANIMATION_END, onEndSalto);
					
					//salto again
					ca.addEventListener(BimaEvent.ANIMATION_END, function onEndSalto2(e:BimaEvent):void {
						ca.removeEventListener(BimaEvent.ANIMATION_END, onEndSalto2);
						
//						con.filters = [];
						ca.stop("smile");
						autoHover(con);
						m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_READY));
					});
					ca.play("salto");
				});
				ca.play("salto");
			});
			ft.start();
		}
		
		private var d:int;
		public function fadeInNotif():void {
			var ifAlreadyInNotifMode:Boolean = con.x==notifPos.x;// ca.animLabel=='fly';
			if(ifAlreadyInNotifMode) {
				autoHover(con);
				m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_FADE_IN_NOTIF_COMPLETE));
				return;
			}
			pauseAutoHover();
			ca.stop("fly");
			notifPos = new Point(m.screenRes.x-50-m.appWindow.x, m.screenRes.y-m.appWindow.y-10);
			setPos(notifPos);
			//ca.stop("smile");
			curtain.y = -220;
			curtain.scaleX = curtain.scaleY = 0.5;
			asset.cacheAsBitmap = curtain.cacheAsBitmap = true;
			asset.mask = curtain;
			var diff:int = 100;
			d = Math.abs(220)/diff;
			var ft:Timer = new Timer(1000/diff,diff);
			ft.addEventListener(TimerEvent.TIMER, onTimer);
			ft.addEventListener(TimerEvent.TIMER_COMPLETE, function onComplete(e:TimerEvent):void {
				ft.removeEventListener(TimerEvent.TIMER_COMPLETE, onComplete);
				ft.removeEventListener(TimerEvent.TIMER, onTimer);
				ft = null;
				asset.mask = null;
				autoHover(con);
				m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_FADE_IN_NOTIF_COMPLETE));
			});
			
//			Hack : Using fadeIn can cause misposition. Skip fadeIn effect
			
//			ft.start();
			ft.dispatchEvent(new TimerEvent(TimerEvent.TIMER_COMPLETE));
		}
		private function onTimer(e:TimerEvent):void {
			curtain.y += d;
		}
		
		public function fadeOut():void {
			//ca.stop("smile");
			m.lastAnimLabelB4Notif = ca.animLabel;
			pauseAutoHover();
			curtain.y = -440;
			curtain.scaleX = 1; curtain.scaleY = -1;
			asset.cacheAsBitmap = curtain.cacheAsBitmap = true;
			asset.mask = curtain;
			var diff:int = 100;
			var d:int = Math.abs(440)/diff;
			var ft:Timer = new Timer(1000/diff,diff);
			ft.addEventListener(TimerEvent.TIMER, function onTimer(e:TimerEvent):void {
				curtain.y += d;
			});
			ft.addEventListener(TimerEvent.TIMER_COMPLETE, function onComplete(e:TimerEvent):void {
				ft.removeEventListener(TimerEvent.TIMER_COMPLETE, onComplete);
				
				m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_FADE_OUT_COMPLETE));
			});
			ft.start();
		}
		
		public function fadeOutNotif():void {
			curtain.y = -220;
			curtain.scaleX = 1; curtain.scaleY = -0.5;
			asset.cacheAsBitmap = curtain.cacheAsBitmap = true;
			asset.mask = curtain;
			var diff:int = 100;
			var d:int = Math.abs(220)/diff;
			var ft:Timer = new Timer(1000/diff,diff);
			ft.addEventListener(TimerEvent.TIMER, function onTimer(e:TimerEvent):void {
				curtain.y += d;
			});
			ft.addEventListener(TimerEvent.TIMER_COMPLETE, function onComplete(e:TimerEvent):void {
				ft.removeEventListener(TimerEvent.TIMER_COMPLETE, onComplete);
				asset.mask = null;
				//removeChild(curtain);
				autoHover(con);
				m.dispatchEvent(new BimaEvent(BimaEvent.AVATAR_FADE_OUT_NOTIF_COMPLETE));
			});
			ft.start();
		}
		
		private var hoverMover:Move = new Move();
		public function autoHover(con:UIComponent):void {
			hoverMover.yFrom = con.y;
			hoverMover.yTo = con.y - 10;
			hoverMover.duration = 2000;
			hoverMover.repeatBehavior = RepeatBehavior.REVERSE;
			hoverMover.repeatCount = int.MAX_VALUE;
			hoverMover.addEventListener(EffectEvent.EFFECT_END, function repeat(e:EffectEvent):void {
				hoverMover.play([con]);
			});
			hoverMover.play([con]);
		}
		
		public function pauseAutoHover():void {
			hoverMover.pause();
		}
		
		private function playAutoHover():void {
			hoverMover.resume();
		}
		
		private var fader:Fade;
		public function showLoadingCircle(text:String):void {
			var isNotInDock:Boolean = con.x!=dockPos.x;
			if(isNotInDock) return;
			loadingCircle.text.text = text;
			fader.play([loadingCircle.text]);
			loadingCircle.visible = true;
		}
		
		public function hideLoadingCircle():void {
			loadingCircle.visible = false;
			fader.end();
		}
		
	}
}