package core
{
	
	/**
	 * An implementation of a linked Stack Datastructure. From Java.
	 *
	 *@author Michael Avila
	 *@version 1.0.0
	 */
	public class Stack
	{
		private var head:Node;
		
		public function isEmpty ()
		{
			return head == null;
		}
		
		public function push(obj:String):void
		{
			var newNode:Node = new Node(obj);
			
			if(head == null)
				head = newNode;
			else
			{
				newNode.next = head;
				head = newNode;
			}
		}
		
		public function pop():String
		{
			
			if(head != null)
			{
				var result:String = head.value;
				head = head.next;
				
				return result;
			}
			else
				return null;
		}
		
		public function peek():String
		{
			if(head != null)
				return head.value;
			else
				return null;
		}
	}
}