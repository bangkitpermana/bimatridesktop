package core
{
	import flash.events.Event;

	public class BimaEvent extends Event
	{
		
		//services callback
		public static const JSON_PARSE_ERROR:String = "json parse error";
		public static const GET_PROFILE_CALLBACK:String = "get profile callback";
		public static const GET_NEW_TOKEN_CALLBACK:String = "get new token callback";
		public static const SIGNIN_CALLBACK:String = "signin callback";
		public static const SIGNOUT_CALLBACK:String = "signout callback";
		public static const IO_ERROR:String = "io error";
		public static const SIGNIN_TIMEOUT_ERROR:String = "signin timeout error";
		public static const AUTOSIGN_TIMEOUT_ERROR:String = " autosign timeout error";
		public static const GET_PACKAGE_CALLBACK:String = "get package callback";
		public static const GET_SPECIAL_PACKAGE_CALLBACK:String = "get special package callback";
		public static const GET_INBOX_CALLBACK:String = "get inbox callback";
		public static const GET_CONTACT_CALLBACK:String = "get contact callback";
		public static const GET_CALL_PLAN_VIEW_CALLBACK:String = "get call plan view callback";
		public static const GET_CALL_PLAN_VIEW_RESULT:String = "get call plan view result";
		public static const GET_UPSELL_CALLBACK:String = "get upsell callback";
		public static const GET_UPSELL_RESULT:String = "get upsell result";
		public static const GET_CREDIT_LIMIT_INFO_CALLBACK:String = "get credit limit info callback";
		public static const GET_CREDIT_LIMIT_INFO_RESULT:String = "get credit limit info result";
		public static const GET_BILLING_INFO_CALLBACK:String = "get billing info callback";
		public static const GET_BILLING_INFO_RESULT:String = "get billing info result";
		public static const GET_BILLING_SUMMARY_CALLBACK:String = "get billing summary callback";
		public static const GET_BILLING_SUMMARY_RESULT:String = "get billing summary result";
		public static const CALL_PLAN_CHANGE_CALLBACK:String = "get call plan change callback";
		public static const CALL_PLAN_CHANGE_RESULT:String = "get call plan change result";
		public static const CREDIT_LIMIT_CHANGE_CALLBACK:String = "get credit limit change callback";
		public static const CREDIT_LIMIT_CHANGE_RESULT:String = "get credit limit change result";
		public static const GET_PULL_NOTIFICATION_CALLBACK:String = "get pull notification callback";
		public static const SET_MESSAGE_MARK_AS_READ:String = "set message mark as read";
		public static const BILLING_PAYMENT_CALLBACK:String = "billing payment callback";
		public static const BILLING_PAYMENT_RESULT:String = "billing payment callback";
		public static const RELOAD_BY_VOUCHER_CALLBACK:String = "reload by voucher callback";
		public static const RELOAD_RESULT:String = "reload result";
		public static const GET_FREESITE_IMAGE_CALLBACK:String = "get freesite image callback";
		public static const BUY_PACKAGE_CALLBACK:String = "buy package callback";
		public static const DOKUCC_SUCCESS:String = "response success";
		public static const BUY_RESULT:String = "buy result";
		public static const BUY_PULSA_CALLBACK:String = "buy pulsa callback";
		public static const BUY_PULSA_RESULT:String = "buy pulsa result";
		public static const STOP_PACKAGE_CALLBACK:String = "stop package callback";
		public static const DEVICE_SETTING_CALLBACK:String = "device setting callback";
		public static const CHECK_TKC_CALLBACK:String = "check tkc callback";
		public static const CHECK_TKC_RESULT:String = "check tkc result";
		public static const VERIFY_TKC_CALLBACK:String = "verify tkc callback";
		public static const VERIFY_TKC_RESULT:String = "verify tkc result";
		public static const RIGHT_TKC_CALLBACK:String = "redeem tkc callback";
		public static const RIGHT_TKC_RESULT:String = "redeem tkc result";
		public static const LEFT_TKC_CALLBACK:String = "accumulate tkc callback";
		public static const LEFT_TKC_RESULT:String = "accumulate tkc result";
		public static const POSTPAID_PAYMENT_CALLBACK:String = "postpaid payment callback";
		public static const POSTPAID_PAYMENT_RESULT:String = "postpaid payment result";
		public static const UPDATE_EMAIL_RESULT:String = "update email result";
		public static const ADD_CHILD_RESULT:String = "add child result";
		public static const SEND_INVOICE_RESULT:String = "send invoice result";
		public static const RECEIVE_BILL_RESULT:String = "receive bill result";
		public static const HYBRID_BILL_RESULT:String = "hybrid bill result";
		// edit by bangkit  remove notification
		public static const GET_REMOVE_NOTIFICATION:String = "remove notification";
		public static const GET_PROMO_RESULT:String = "send invoice result";
		public static const ALERT_ERROR_CC:String = "error handle cc";
		
		//app event
		public static const USER_LOGGED_IN:String = "user logged in";
		public static const DOCK_APP:String = "dock app";
		public static const SHOW_LOGIN_WINDOW:String = "show login window";
		public static const SHOW_PROFILE_PAGE:String = "show profile page";
		public static const SHOW_STOP_PACKAGE_CONFIRMATION:String = "show stop package confirmation";
		public static const TIPS_DATA_READY:String = "tips data ready";
		public static const TIPS_TITLE_CLICKED:String = "tips title clicked";
		public static const BUILD_FULL_TASKBAR_MENU:String = "build full taskbar menu";
		public static const PACKAGE_TOP_MENU_CLICKED:String = "package top menu clicked";
		public static const PACKAGE_SUB_MENU_CLICKED:String = "package sub menu clicked";
		public static const PACKAGE_SUB_MENU_2_CLICKED:String = "package sub menu 2 clicked";
		public static const PAY_METHOD_MENU_CLICKED:String = "pay method menu clicked";
		public static const INBOX_MESSAGE_CLICKED:String = "inbox message clicked";
		public static const READ_MESSAGE:String = "read message";
		public static const REDRAW_UNREAD_NOTIFICATION_MENU_BALL:String = "redraw unread notification menu ball";
		public static const USER_PREFERENCES_LOADED:String = "user preferences loaded";
		public static const OPEN_PROFILE_MENU_PAGE:String = "open sub profile menu page";
		public static const PROMO_REDIRECT:String = "promo redirect";
		public static const PROMO_BACK_TO_PROFILE:String = "promo back to profile";
		public static const SHOW_THANKS_FROM_DOKU:String = "show thanks from doku";
		public static const POPUP_CLOSED:String = "popup closed";
		public static const BILL_RENDERER_CLICKED:String = "bill renderer clicked";
		public static const BILL_RENDERER_SELECTED_CHANGE:String = "bill renderer selected change";
		//for share quota
		public static const SHARE_QUOTA_RENDERER_SELECTED_CHANGE:String = "share quota renderer selected change";
		public static const OPEN_STOPPABLE_PAGE:String = "open stoppable page";
		public static const BILL_MAIN_RENDERER_CLICKED:String = "bill main renderer clicked";
		
		//avatar event
		public static const AVATAR_READY:String = "avatar ready";
		public static const AVATAR_SHOW_PROFILE_MENU:String = "avatar show profile menu";
		public static const AVATAR_HIDE_PROFILE_MENU:String = "avatar hide profile menu";
		public static const AVATAR_SHOW_CIRCLE_WINDOW:String = "avatar show circle window";
		public static const AVATAR_ON_LOGGING_IN_MODE:String = "avatar on logging in mode";
		public static const AVATAR_ON_LOGGED_IN_MODE:String = "avatar on logged in mode";
		public static const AVATAR_HIDE_INTRO:String = "avatar hide intro";
		public static const AVATAR_SHOW_PACKAGE_MENU:String = "avatar show package menu";
		public static const AVATAR_HIDE_PACKAGE_MENU:String = "avatar hide package menu";
		public static const ANIMATION_END:String = "animation end";
		public static const AVATAR_DOCK:String = "avatar dock";
		public static const AVATAR_DO_IDLE_VARIATION:String = "avatar do idle variation";
		public static const AVATAR_WANDER:String = "avatar wander";
		public static const AVATAR_FADE_OUT_COMPLETE:String = "avatar fade out complete";
		public static const AVATAR_FADE_IN_NOTIF_COMPLETE:String = "avatar fade in notif complete";
		public static const AVATAR_FADE_OUT_NOTIF_COMPLETE:String = "avatar fade out notif complete";
		public static const AVATAR_MOVE_COMPLETE:String = "avatar move complete";
		public static const AVATAR_IS_BACK_FROM_NOTIF_TO_APP:String = "avatar is back";
		public static const CLAIM_BONUS_RESULT:String = "claim bonus";
		
		
		
		//DOKU event
		public static const DOKU_PAYMENT_CALLBACK_BCA:String = "doku payment callback bca";
		public static const DOKU_PAYMENT_CALLBACK:String = "doku payment callback";
		public static const DOKU_LOCATION_CHANGE:String = "doku location change";
		
		public var params:Object;
		
		public function BimaEvent(type:String, params:Object=null):void {
			super(type);
			this.params = params;
		}
		
	}
}