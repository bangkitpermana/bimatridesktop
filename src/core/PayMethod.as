package core
{
	import flash.events.EventDispatcher;

	public class PayMethod extends EventDispatcher
	{
		
		public var paymentchannel:String;
		[Bindable] public var paymentname:String;
		public var paymenturl:String;
		
		public function PayMethod(paymentchannel:String,paymentname:String):void {
			this.paymentchannel = paymentchannel;
			this.paymentname = paymentname;
			//this.paymenturl = paymenturl;
		}
		
	}
}