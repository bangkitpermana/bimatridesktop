﻿package carousel {
	
	import components.BallMenu;
	
	import flash.display.Sprite;
	
	public class Item extends Sprite {
		
		public var data:BallMenu;
		public var angl:Number;
		
		public function Item(sp:BallMenu):void {
			this.data = sp;
			addChild(sp);
			this.buttonMode = true;
		}
		
	}
}