package carousel {

    import core.Manager;
    
    import flash.display.*;
    import flash.events.*;
    import flash.geom.PerspectiveProjection;
    import flash.geom.Point;
    import flash.net.*;
    import flash.text.*;
    import flash.ui.*;
    import flash.utils.*;
    
    import mx.collections.ArrayCollection;

    public class Carousel extends MovieClip {
		
		[Embed(source="../assets/images/buttons/left-arrow.png")]
		public var ArrowRightClass:Class;
		[Embed(source="../assets/images/buttons/right-arrow.png")]
		public var ArrowLeftClass:Class;

        private var items:ArrayCollection;
		private var numOfItems:uint; // number of Items to put on stage
        private var radiusX:uint = 250; // width of carousel
        private var radiusY:uint = 75; // height of carousel
        private var centerX:Number;// = stage.stageWidth / 2; // x position of center of carousel
        private var centerY:Number;// = stage.stageHeight / 2; // y position of center of carousel
        public var speed:Number = 0.05; // initial speed of rotation of carousel
        public var lastSpeed:Number;
		private var itemArray:Array = new Array(); // store the Items to sort them according to their 'depth' - see sortBySize() function.

		private var pathLayer:Sprite = new Sprite();
		private var itemLayer:Sprite = new Sprite();
		private var i:uint;
		
		//nav
		private var nextBtn:Sprite;
		private var prevBtn:Sprite;
		private var rc:Number;
		private var rt:Number;
		private var isRotation:Boolean;
		
		public function Carousel(items:ArrayCollection,rx:uint,ry:uint,center:Point,sp:Number=-0.01):void {
			addChild(pathLayer);
			addChild(itemLayer);
			
			this.items = items;
			numOfItems = items.length;
			radiusX = rx;
			radiusY = ry;
			centerX = center.x;
			centerY = center.y;
			this.speed = sp;
			itemArray = [];
			
			//draw elips path
			/*var path:Sprite = new Sprite();
			path.graphics.lineStyle(3,0xffff00,1);
			path.graphics.drawEllipse(0,0,radiusX*2,radiusX*2);
			path.x = -radiusX; path.y = -radiusX;
			//pathLayer.rotationX = 90*radiusY/radiusX;
			pathLayer.addChild(path);
			pathLayer.addEventListener(Event.ENTER_FRAME, function frameHandler(e:Event):void {
				pathLayer.rotationX += 5;
				pathLayer.rotationZ += 5;
			});//*/
			
            for(i=0; i<numOfItems; i++) {
                var item:Item = items[i] as Item;
                // public var angl:Number; is in Item class.
                // Item class extends ItemInner in FLA library.
                item.angl = i * ((Math.PI * 2) / numOfItems) + Math.PI/4;
//				item.transform.perspectiveProjection = new PerspectiveProjection();
//				item.transform.perspectiveProjection.projectionCenter = new Point();
//				trace(item.transform.perspectiveProjection.projectionCenter);
//				item.transform.perspectiveProjection = Manager.getInstance().proj;
                //item.alpha = 0.5;
                itemArray.push(item);
				itemLayer.addChild(item);
				item.x = Math.cos(item.angl) * radiusX + centerX;
				item.y = Math.sin(item.angl-Math.PI/numOfItems) * radiusY + centerY;
				item.z = -item.y;
//                item.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
                // listen for MouseEvents only on icons, not on reflections
                //item.addEventListener(MouseEvent.CLICK, clickHandler);
//                item.addEventListener(MouseEvent.ROLL_OVER, rollOverHandler);
//                item.addEventListener(MouseEvent.ROLL_OUT, rollOutHandler);
            }
			zSorting();
			rt = Math.abs(itemArray[1].angl - itemArray[0].angl);
			addEventListener(Event.ENTER_FRAME, carouselFrameHandler);
            //stage.addEventListener(MouseEvent.MOUSE_MOVE, mouseMoveHandler);
			
			//add navigation button
			addNavButtons();
        }
		
		private function addNavButtons():void {
			nextBtn = new Sprite();
			nextBtn.addChild(new ArrowRightClass());
			nextBtn.buttonMode = true;
			nextBtn.x = 100; nextBtn.y = 200;
			nextBtn.addEventListener(MouseEvent.CLICK, function onNextBtnClick(e:MouseEvent):void {
				speed = Math.abs(speed);
//				if(isRotation) return;
//				isRotation = true;
//				speed = Math.abs(speed);
//				rc = 0;
//				addEventListener(Event.ENTER_FRAME, carouselFrameHandler);
			});
			this.addChild(nextBtn);
			
			prevBtn = new Sprite();
			prevBtn.addChild(new ArrowLeftClass());
			prevBtn.buttonMode = true;
			prevBtn.x = -100 - prevBtn.width; prevBtn.y = 200;
			prevBtn.addEventListener(MouseEvent.CLICK, function onPrevBtnClick(e:MouseEvent):void {
				speed = -Math.abs(speed);
//				if(isRotation) return;
//				isRotation = true;
//				speed = -Math.abs(speed);
//				rc = 0;
//				addEventListener(Event.ENTER_FRAME, carouselFrameHandler);
			});
			this.addChild(prevBtn);
		}
		
		private function carouselFrameHandler(event:Event):void {
			for(i=0; i<numOfItems; i++) {
				var item:Item = itemArray[i] as Item;
				item.angl += speed;
				item.x = Math.cos(item.angl) * radiusX + centerX;
				item.y = Math.sin(item.angl-Math.PI/numOfItems) * radiusY + centerY;
				item.z = -item.y;
//				if(i==0) {
//					trace(item.x,item.y,item.z);
//				}
			}
			zSorting();
//			rc += Math.abs(speed);
//			if(rc>=rt) {
//				if(rc>rt) {
//					var d:Number = rc - rt;
//					for(i=0; i<numOfItems; i++) {
//						item = itemArray[i] as Item;
//						if(speed>0) {
//							item.angl -= speed;
//						} else {
//							item.angl += speed;
//						}
//						item.x = Math.cos(item.angl) * radiusX + centerX;
//						item.y = Math.sin(item.angl-Math.PI/numOfItems) * radiusY + centerY;
//						item.z = -item.y;
//					}
//					zSorting();
//				}
//				removeEventListener(Event.ENTER_FRAME, carouselFrameHandler);
//				isRotation = false;
//			}
		}
		
		private function zSorting():void {
			itemArray.sortOn("z", Array.DESCENDING | Array.NUMERIC);
//			while(numChildren>0) {
//				removeChildAt(0);
//			}
			for(i=0; i<numOfItems; i++) {
				var item:Item = itemArray[i] as Item;
				try {
					itemLayer.setChildIndex(item,i);
				} catch(e:Error) {
					continue;
				}
//				swapChildren(getChildAt(i),item);
			}
		}

        // position Items in elipse
        private function enterFrameHandler(event:Event):void {
            event.target.x = Math.cos(event.target.angl) * radiusX + centerX; // x position of Item
            event.target.y = Math.sin(event.target.angl) * radiusY + centerY; // y postion of Item
            // scale Item according to y position to give perspective
            var s:Number = event.target.y / (centerY + radiusY);
            event.target.scaleX = event.target.scaleY = s;
            event.target.angl += speed; // speed is updated by mouseMoveHandler
            sortBySize();
        }

        // set the display list index (depth) of the Items according to their
        // scaleX property so that the bigger the Item, the higher the index (depth)
        private function sortBySize():void {
            // There isn't an Array.ASCENDING property so use DESCENDING and reverse()
            itemArray.sortOn("scaleX", Array.DESCENDING | Array.NUMERIC);
            itemArray.reverse();
            for(i = 0; i < numOfItems; i++) {
                var item:Item = itemArray[i] as Item;
                //(item, i);
				//this.addChildAt(item,i);
            }
        }

        private function clickHandler(event:MouseEvent):void {
            // clean up your listeners before you do anything else to free up
            // user's CPU resources
//            for(var i:uint = 0; i < itemArray.length; i++) {
//                var item:Item = itemArray[i];
//                item.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
//                item.removeEventListener(MouseEvent.CLICK, clickHandler);
//                item.removeEventListener(MouseEvent.ROLL_OVER, rollOverHandler);
//                item.removeEventListener(MouseEvent.ROLL_OUT, rollOutHandler);
//                // optional:
//                removeChild(item);
//                // to replace the carousel again see reInit() function
//            }
            // put your own code here.
        }

        private function rollOverHandler(event:MouseEvent):void {
            event.target.parent.alpha = 1;
        }
        
        private function rollOutHandler(event:MouseEvent):void {
            event.target.parent.alpha = 0.5;
        }
        
        /*
        Update the speed at which the carousel rotates accoring to the distance of the mouse from the center of the stage. The speed variable only gets updated when the mouse moves over the Item Sprites.
        */
        private function mouseMoveHandler(event:MouseEvent):void {
            speed = (mouseX - centerX) / 6000;
        }
        
        // put items back on stage
//        private function reInit(event:MouseEvent):void {
//            for(var i:uint = 0; i < itemArray.length; i++) {
//                var item:Item = itemArray[i];
//                item.alpha = 0.5;
//                addChild(item);
//                item.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
//                item.icon.addEventListener(MouseEvent.CLICK, clickHandler);
//                item.icon.addEventListener(MouseEvent.ROLL_OVER, rollOverHandler);
//                item.icon.addEventListener(MouseEvent.ROLL_OUT, rollOutHandler);
//            }
//        }
        
    }
    
}