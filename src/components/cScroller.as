//Custom class since using TextInput inside a scroller will result in error#1009:null reference error cause by focusManager is null
package components
	
{
	import mx.core.mx_internal;
	use namespace mx_internal;	
	import flash.events.FocusEvent;
	import spark.components.Scroller;
	public class cScroller extends Scroller
	{
		public function cScroller()
		{
			super();
		}
		
		override protected function focusInHandler(event:FocusEvent):void
		{
			if(focusManager != null) {
				super.focusInHandler(event);
			}
		}
	}
}