!include "WordFunc.nsh"
!insertmacro VersionCompare
!define PRODUCT_NAME "Bima TRI"
!define PRODUCT_VERSION "1.2.2"
!define PRODUCT_PUBLISHER "H3I"
Name "${PRODUCT_NAME} ${PRODUCT_VERSION} by ${PRODUCT_PUBLISHER}"
BrandingText "Your digital assistant"
Icon "icon32.ico"
OutFile "bimaTRI-pro.exe"
Section Prerequisites
  SetAutoClose true
  SetOutPath ""
  SetOverwrite ifnewer
  ReadRegStr $0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Adobe AIR" "DisplayVersion"
        ;    $var=0  Versions are equal
        ;    $var=1  Version1 is newer
        ;    $var=2  Version2 is newer
    ${VersionCompare} $0 "3.4.0.2540" $R0
    IntCmp $R0 2 notinst
    Goto installProgram
  notinst:
    File /oname=$TEMP\airinstalltemp.exe "AdobeAIRInstaller_3.4.0.2540.exe"
    ExecWait "$TEMP\airinstalltemp.exe"
    Goto installProgram
  installProgram:
    File /oname=$TEMP\bimaTRI_pro.air "bimaTRI_pro.air"
    ExecShell "" "$TEMP\bimaTRI_pro.air"
SectionEnd