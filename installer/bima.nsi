!include "WordFunc.nsh"
!insertmacro VersionCompare
!define PRODUCT_NAME "Bima TRI"
!define PRODUCT_VERSION "1.2.1"
!define PRODUCT_PUBLISHER "HCPT"

;
;
;

Name "${PRODUCT_NAME} ${PRODUCT_VERSION} by ${PRODUCT_PUBLISHER}"
BrandingText "Your digital assistant"
Icon "res\icon32.ico"
OutFile "out\bimaTRI.exe"

;
;
;

VIProductVersion "${PRODUCT_VERSION}.0"
VIAddVersionKey  "ProductName"          "${PRODUCT_NAME}"
VIAddVersionKey  "CompanyName"          "${PRODUCT_PUBLISHER}"
VIAddVersionKey  "ProductVersion"       "${PRODUCT_VERSION}.0"
VIAddVersionKey  "FileVersion"          "${PRODUCT_VERSION}.0"
VIAddVersionKey  "LegalCopyright"       ""
VIAddVersionKey  "FileDescription"      ""

;
;
;

Section Prerequisites
  SetAutoClose true
  SetOutPath ""
  SetOverwrite ifnewer
  ReadRegStr $0 HKLM "SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\Adobe AIR" "DisplayVersion"
        ;    $var=0  Versions are equal
        ;    $var=1  Version1 is newer
        ;    $var=2  Version2 is newer
    ${VersionCompare} $0 "3.4.0.2540" $R0
    IntCmp $R0 2 notinst
    Goto installProgram
  notinst:
    File /oname=$TEMP\airinstalltemp.exe "bin\AdobeAIRInstaller_3.4.0.2540.exe"
    ExecWait "$TEMP\airinstalltemp.exe"
    Goto installProgram
  installProgram:
    File /oname=$TEMP\bima.air "air\bima.air"
    ExecShell "" "$TEMP\bima.air"
SectionEnd